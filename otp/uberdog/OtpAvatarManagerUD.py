from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD
from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.fsm.FSM import FSM

from otp.otpbase.OTPGlobals import AvatarNumSlots

from pirates.pirate.HumanDNA import HumanDNA
from pirates.ai.DatabaseObject import DatabaseObject

import time

class OperationFSM(FSM):

    def __init__(self, manager, target):
        self.manager = manager
        self.target = target

        FSM.__init__(self, self.__class__.__name__)

    def enterKill(self, reason = ''):
        self.manager.killAvatarOperationFSM(self.target, reason)
        self.demand('Off')

    def enterOff(self):
        del self.manager.avatar2fsm[self.target]

class AvatarOperationFSM(OperationFSM):
    POST_ACCOUNT_STATE = 'Off' # This should be overridden by inheritors.

    def enterRetrieveAccount(self):
        # Query the account:
        gotAccEvent = self.manager.air.uniqueName(f'gotAcc-{self.target}')
        self.acceptOnce(gotAccEvent, self.__handleRetrieve)

        db = DatabaseObject(self.manager.air, self.target)
        db.doneEvent = gotAccEvent
        db.dclass = self.manager.air.dclassesByName['AccountUD']
        db.getFields(['ACCOUNT_AV_SET'])

    def __handleRetrieve(self, db, retCode):
        if retCode != 0:
            self.demand('Kill', 'Your account object was not found in the database!')
            return

        self.account = db.values

        self.avList = self.account['ACCOUNT_AV_SET']

        # Sanitize:
        self.avList = self.avList[:AvatarNumSlots]
        self.avList += [0] * (AvatarNumSlots - len(self.avList))

        self.demand(self.POST_ACCOUNT_STATE)

class GetAvatarListFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('GetAvatarListFSM')
    POST_ACCOUNT_STATE = 'QueryAvatars'

    def enterStart(self):
        self.demand('RetrieveAccount')

    def enterQueryAvatars(self):
        self.pendingAvatars = set()
        self.avatarFields = {}

        for avId in self.avList:
            if avId:
                self.pendingAvatars.add(avId)

                def response(db, retCode, avId = avId):
                    if self.state != 'QueryAvatars':
                        return

                    if retCode != 0:
                        self.demand('Kill', f'The avatar associated with {avId} is invalid!')
                        return

                    self.avatarFields[avId] = db.values
                    self.pendingAvatars.remove(avId)

                    if not self.pendingAvatars:
                        self.demand('SendAvatarList')

                gotAvatarsEvent = self.manager.air.uniqueName(f'gotAvatars-{avId}')
                self.acceptOnce(gotAvatarsEvent, response)

                dbo = DatabaseObject(self.manager.air, avId)
                dbo.doneEvent = gotAvatarsEvent
                dbo.dclass = self.manager.air.dclassesByName['DistributedPlayerPirateUD']
                dbo.getFields(dbo.getDatabaseFields(dbo.dclass, True))

        if not self.pendingAvatars:
            self.demand('SendAvatarList')

    def retrieveAccountData(self, potentialAvs):
        accountData = []

        # TODO: Implement proper sub accounts
        subData = [
            [1] # sub account 1
        ]

        for i in range(len(subData)):
            # Used for sub accounts.
            subId = subData[i][0]

            # Disney method for avatars pending creation.
            numPending = 0

            # How many avatars can this user have?
            maxAvatars = AvatarNumSlots
            maxSlots = AvatarNumSlots

            accountData.append([subId, numPending, maxAvatars, maxSlots, potentialAvs])

        return accountData

    def enterSendAvatarList(self):
        potentialAvs = []
        pirateClass = self.manager.air.dclassesByName['DistributedPlayerPirateUD']

        for avId, fields in list(self.avatarFields.items()):
            index = self.avList.index(avId)
            name = fields.get('setName', [''])[0]
            wishNameState = fields.get('WishNameState', [''])[0]
            wishName = fields.get('WishName', [''])[0]

            nameState = '0'

            if wishNameState == 'OPEN':
                nameState = 'OPEN'

            elif wishNameState == 'PENDING':
                nameState = 'REQUESTED'

            elif wishNameState == 'APPROVED':
                nameState = 'APPROVED'

                # Set their name.
                fields = {
                    'WishNameState': ('',),
                    'WishName': ('',),
                    'setName': (fields['WishName'][0],)
                }

                simbase.air.dbInterface.updateObject(simbase.air.dbId, avId, pirateClass, fields)

            elif wishNameState == 'REJECTED':
                nameState = 'DENIED'

                # Set them to the OPEN state so they can try again.
                fields = {
                    'WishNameState': ('OPEN',),
                    'WishName': ('',)
                }

                simbase.air.dbInterface.updateObject(simbase.air.dbId, avId, pirateClass, fields)

            # TODO: Implement these fields
            creator = self.target # This sets who created that sub pirate. If its someone else, this pirate cant be deleted.
            shared = 1 # This sets if the sub pirate is shared or not. If not, this pirate is locked down.
            online = 0 # Is the sub pirate currently logged in ?
            defaultShard = fields.get('setDefaultShard', [201000000])[0]
            lastLogout = fields.get('setLastLogout', [0])[0] # This is used to check if player was last logged in. This also sets fav shard to 0 if none.

            humanDNA = HumanDNA()

            for fieldName, fieldValue in list(fields.items()):
                if hasattr(humanDNA, fieldName):
                    getattr(humanDNA, fieldName)(*fieldValue)

            potentialAvs.append([name, humanDNA, index, avId, creator, shared, online, wishName, nameState, defaultShard, lastLogout])

        numInventoryManagers = 1

        accountData = self.retrieveAccountData(potentialAvs)

        # Send it to DistributedAvatarManager, not OtpAvatarManager.
        self.manager.parent.sendUpdateToAccountId(self.target, 'avatarListResponse', [accountData, numInventoryManagers])

        self.demand('Off')

class RequestAvatarFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('RequestAvatarFSM')
    POST_ACCOUNT_STATE = 'RetrieveAvatar'

    def enterStart(self, subId, slot):
        self.subId = subId
        self.slot = slot
        self.demand('RetrieveAccount')

    def enterRetrieveAvatar(self):
        print('avId in slot', self.slot, ':', self.avList[self.slot])
        if self.avList[self.slot] == 0:
            self.manager.parent.sendUpdateToAccountId(self.target, 'avatarSlotResponse', [self.subId, self.slot])
        else:
            reasonId = 1 # TODO?
            self.manager.parent.sendUpdateToAccountId(self.target, 'rejectAvatarSlot', [reasonId, self.subId, self.slot])

        self.demand('Off')

class PlayAvatarFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('PlayAvatarFSM')
    POST_ACCOUNT_STATE = 'PlayAvatar'

    def enterStart(self, avId, subId):
        self.avId = avId
        self.subId = subId
        self.demand('RetrieveAccount')

    def enterPlayAvatar(self):
        if self.avId not in self.avList:
            self.manager.sendUpdateToAccountId(self.target, 'rejectPlayAvatar', [0, self.avId])
            self.demand('Kill', 'Database failed to check picked avatar!')
            return

        ## I assume this is another sub check, but since there is no subs, lets just skip this.
        self.manager.parent.sendUpdateToAccountId(self.target, 'playAvatarResponse', [self.avId, self.subId, 0, 0])

        self.demand('Off')

class DeleteAvatarFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('DeleteAvatarFSM')
    POST_ACCOUNT_STATE = 'ProcessDelete'

    def enterStart(self, avId, subId):
        self.avId = avId
        self.subId = subId
        self.demand('RetrieveAccount')

    def enterProcessDelete(self):
        if self.avId not in self.avList:
            self.demand('Kill', 'Tried to delete an avatar not in the account!')
            return

        index = self.avList.index(self.avId)
        self.avList[index] = 0

        avsDeleted = list(self.account.get('ACCOUNT_AV_SET_DEL', []))
        avsDeleted.append([self.avId, int(time.time())])

        db = DatabaseObject(self.manager.air, self.target)
        db.dclass = self.manager.air.dclassesByName['AccountUD']
        db.setFields({
            'ACCOUNT_AV_SET': self.avList,
            'ACCOUNT_AV_SET_DEL': avsDeleted
        })

        # TODO: Implement me
        # if retCode != 0:
            # self.manager.sendUpdateToAccountId(self.target, 'rejectRemoveAvatar', [0])
            # self.demand('Kill', 'Database failed to mark the avatar as deleted!')
            # return

        # Successfully deleted avatar!
        self.manager.air.writeServerEvent('avatarDeleted', self.avId, f'{self.subId}|{self.target}')
        self.manager.sendUpdateToAccountId(self.target, 'removeAvatarResponse', [self.avId, self.subId])

        self.demand('Off')

class OtpAvatarManagerUD(DistributedObjectGlobalUD):

    def __init__(self, parent):
        self.parent = parent
        self.air = parent.air

        DistributedObjectGlobalUD.__init__(self, self.air)

    def announceGenerate(self):
        DistributedObjectGlobalUD.announceGenerate(self)

        # This keeps track of all the avatar's currently requesting their avatars through the AvatarManager.
        # This exists in order to prevent hacked clients from firing up more than one operation
        # at a time, which could potentially lead to exploitations of race conditions.
        self.avatar2fsm = {}

    def killAvatarOperationFSM(self, accountId, message = 'An operation is already underway.'):
        fsm = self.avatar2fsm.get(accountId)

        if not fsm:
            self.notify.warning(f'Tried to kill account {accountId} for duplicate FSM, but none exists!')
            return

        self.air.kickChannel(accountId, message)

    def runAvatarFSM(self, fsmOperation, *args):
        sender = self.air.getAccountIdFromSender()

        if not sender:
            return

        if sender in self.avatar2fsm:
            self.killAvatarOperationFSM(sender)
            del self.avatar2fsm[sender]
            return

        self.avatar2fsm[sender] = fsmOperation(self, sender)
        self.avatar2fsm[sender].request('Start', *args)

    def requestAvatarList(self, _):
        self.runAvatarFSM(GetAvatarListFSM)

    def requestAvatarSlot(self, _, subId, slot):
        self.runAvatarFSM(RequestAvatarFSM, subId, slot)

    def requestRemoveAvatar(self, _, avatarId, subId, confirmPassword):
        self.runAvatarFSM(DeleteAvatarFSM, avatarId, subId)

    def requestPlayAvatar(self, _, avId, subId):
        self.runAvatarFSM(PlayAvatarFSM, avId, subId)
