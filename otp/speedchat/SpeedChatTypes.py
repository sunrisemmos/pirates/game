# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: otp.speedchat.SpeedChatTypes
from .SCObject import SCObject
from .SCMenu import SCMenu
from .SCElement import SCElement
from .SCMenuHolder import SCMenuHolder
from .SCTerminal import SCTerminal
from .SCCustomMenu import SCCustomMenu
from .SCEmoteMenu import SCEmoteMenu
from .SCStaticTextTerminal import SCStaticTextTerminal
from .SCGMTextTerminal import SCGMTextTerminal
from .SCCustomTerminal import SCCustomTerminal
from .SCEmoteTerminal import SCEmoteTerminal
from .SCColorScheme import SCColorScheme