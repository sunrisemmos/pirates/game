# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: otp.speedchat.SpeedChatGlobals
from .SCTerminal import SCTerminalSelectedEvent
from .SCTerminal import SCTerminalLinkedEmoteEvent
from .SCStaticTextTerminal import SCStaticTextMsgEvent
from .SCGMTextTerminal import SCGMTextMsgEvent
from .SCCustomTerminal import SCCustomMsgEvent
from .SCEmoteTerminal import SCEmoteMsgEvent, SCEmoteNoAccessEvent