# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: otp.web.Setting
from direct.fsm.StatePush import StateVar

class Setting:

    def __init__(self, name, value):
        self._name = name
        self.setValue(value)

    def getName(self):
        return self._name

    def setValue(self, value):
        self._value = value

    def getValue(self):
        return self._value

class StateVarSetting(Setting, StateVar):

    def __init__(self, name, value):
        StateVar.__init__(self, value)
        Setting.__init__(self, name, value)

    def setValue(self, value):
        StateVar.set(self, value)

    def getValue(self):
        return StateVar.get(self)