import sys, os, time, string, builtins
from panda3d.core import *
from direct.showbase.MessengerGlobal import *
from direct.showbase.DirectObject import DirectObject
from direct.showbase.EventManagerGlobal import *
from direct.task.MiniTask import MiniTask, MiniTaskManager
from direct.directnotify.DirectNotifyGlobal import *
from direct.showbase import DConfig

class LogAndOutput:

    def __init__(self, orig, log):
        self.orig = orig
        self.log = log
        self.console = False

    def write(self, str):
        self.log.write(str)
        self.log.flush()
        if self.console:
            self.orig.write(str)
            self.orig.flush()

    def flush(self):
        self.log.flush()
        self.orig.flush()

class LauncherBase(DirectObject):
    GameName = 'game'
    win32con_FILE_PERSISTENT_ACLS = 8
    InstallDirKey = 'INSTALL_DIR'
    GameLogFilenameKey = 'GAMELOG_FILENAME'
    PandaWindowOpenKey = 'PANDA_WINDOW_OPEN'
    PandaErrorCodeKey = 'PANDA_ERROR_CODE'
    NewInstallationKey = 'IS_NEW_INSTALLATION'
    LastLoginKey = 'LAST_LOGIN'
    UserLoggedInKey = 'USER_LOGGED_IN'
    PaidUserLoggedInKey = 'PAID_USER_LOGGED_IN'
    ReferrerKey = 'REFERRER_CODE'
    PeriodTimeRemainingKey = 'PERIOD_TIME_REMAINING'
    PeriodNameKey = 'PERIOD_NAME'
    SwidKey = 'SWID'
    DISLTokenKey = 'DISLTOKEN'
    ProxyServerKey = 'PROXY_SERVER'
    ProxyDirectHostsKey = 'PROXY_DIRECT_HOSTS'
    launcherFileDbFilename = 'launcherFileDb'
    webLauncherFlag = False

    def __init__(self):
        self.started = False
        self.taskMgrStarted = False
        self.pandaErrorCode = 0
        self.WIN32 = os.name == 'nt'
        if self.WIN32:
            self.VISTA = sys.getwindowsversion()[3] == 2 and sys.getwindowsversion()[0] == 6
        else:
            self.VISTA = 0
        ltime = time.localtime()
        logSuffix = '%02d%02d%02d_%02d%02d%02d' % (ltime[0] - 2000, ltime[1], ltime[2], ltime[3], ltime[4], ltime[5])
        logPrefix = ''
        if not self.WIN32:
            logPrefix = os.environ.get('LOGFILE_PREFIX', '')
        logFolder = self.getLogFolder()
        if not os.path.exists(logFolder):
            os.makedirs(logFolder)
        logfile = logFolder + logPrefix + self.getLogFileName() + '-' + logSuffix + '.log'
        self.errorfile = 'errorCode'
        log = open(logfile, 'a')
        logOut = LogAndOutput(sys.__stdout__, log)
        logErr = LogAndOutput(sys.__stderr__, log)
        sys.stdout = logOut
        sys.stderr = logErr
        print('\n\nStarting %s...' % self.GameName)
        print('Current time: ' + time.asctime(time.localtime(time.time())) + ' ' + time.tzname[0])
        print('sys.path = ', sys.path)
        print('sys.argv = ', sys.argv)
        print('os.environ = ', os.environ)
        launcherConfig = DConfig
        builtins.config = launcherConfig
        self.miniTaskMgr = MiniTaskManager()
        self.setServerVersion(launcherConfig.GetString('server-version', 'no_version_set'))
        self.ServerVersionSuffix = launcherConfig.GetString('server-version-suffix', '')
        self.nout = MultiplexStream()
        Notify.ptr().setOstreamPtr(self.nout, 0)
        self.nout.addFile(Filename(logfile))
        if launcherConfig.GetBool('console-output', False):
            self.nout.addStandardOutput()
            sys.stdout.console = True
            sys.stderr.console = True
        self.notify = directNotify.newCategory('Launcher')
        self.clock = TrueClock.getGlobalPtr()
        self.logPrefix = logPrefix
        self.testServerFlag = self.getTestServerFlag()
        self.notify.info('isTestServer: %s' % self.testServerFlag)
        self.gameServer = self.getGameServer()
        self.notify.info('Game Server %s' % self.gameServer)
        self.goUserName = ''
        self.lastLauncherMsg = None
        self.setRegistry(self.GameLogFilenameKey, logfile)
        self.showPhase = 3.5
        self.currentPhase = 4
        if self.getServerVersion() == 'no_version_set':
            self.setPandaErrorCode(10)
            self.notify.info('Aborting, Configrc did not run!')
            sys.exit()
        self.launcherMessage(self.Localizer.LauncherStartingMessage)
        self.http = HTTPClient()
        self.foreground()

    def getTime(self):
        return self.clock.getShortTime()

    def isDummy(self):
        return 0

    def getProductName(self):
        config = DConfig
        productName = config.GetString('product-name', '')
        if productName and productName != 'DisneyOnline-US':
            productName = '_%s' % productName
        else:
            productName = ''
        return productName

    def background(self):
        self.notify.info('background: Launcher now operating in background')
        self.backgrounded = 1

    def foreground(self):
        self.notify.info('foreground: Launcher now operating in foreground')
        self.backgrounded = 0

    def setRegistry(self, key, value):
        self.notify.info('DEPRECATED setRegistry: %s = %s' % (key, value))

    def getRegistry(self, key):
        self.notify.info('DEPRECATED getRegistry: %s' % key)
        return

    def foregroundSleep(self):
        if not self.backgrounded:
            time.sleep(self.ForegroundSleepTime)

    def forceSleep(self):
        if not self.backgrounded:
            time.sleep(3.0)

    def maybeStartGame(self):
        if not self.started and self.currentPhase >= self.showPhase:
            self.started = True
            self.notify.info('maybeStartGame: starting game')
            self.launcherMessage(self.Localizer.LauncherStartingGame)
            self.background()
            builtins.launcher = self
            self.startGame()

    def _runTaskManager(self):
        if not self.taskMgrStarted:
            self.miniTaskMgr.run()
            self.notify.info('Switching task managers.')
        taskMgr.run()

    def _stepMiniTaskManager(self, task):
        self.miniTaskMgr.step()
        if self.miniTaskMgr.taskList:
            return task.cont
        self.notify.info('Stopping mini task manager.')
        self.miniTaskMgr = None
        return task.done

    def _addMiniTask(self, task, name):
        if not self.miniTaskMgr:
            self.notify.info('Restarting mini task manager.')
            self.miniTaskMgr = MiniTaskManager()
            from direct.task.TaskManagerGlobal import taskMgr
            taskMgr.remove('miniTaskManager')
            taskMgr.add(self._stepMiniTaskManager, 'miniTaskManager')
        self.miniTaskMgr.add(task, name)

    def newTaskManager(self):
        self.taskMgrStarted = True
        if self.miniTaskMgr.running:
            self.miniTaskMgr.stop()
        from direct.task.TaskManagerGlobal import taskMgr
        taskMgr.remove('miniTaskManager')
        taskMgr.add(self._stepMiniTaskManager, 'miniTaskManager')

    def mainLoop(self):
        try:
            self._runTaskManager()
        except SystemExit:
            if hasattr(builtins, 'base'):
                base.destroy()
            self.notify.info('Normal exit.')
            raise
        except:
            self.setPandaErrorCode(12)
            self.notify.warning('Handling Python exception.')
            if hasattr(builtins, 'base') and getattr(base, 'cr', None):
                if base.cr.timeManager:
                    from otp.otpbase import OTPGlobals
                    base.cr.timeManager.setDisconnectReason(OTPGlobals.DisconnectPythonError)
                    base.cr.timeManager.setExceptionInfo()
                base.cr.sendDisconnect()
            if hasattr(builtins, 'base'):
                base.destroy()
            self.notify.info('Exception exit.\n')
            import traceback
            traceback.print_exc()
            sys.exit()

    def updateNextMultifile(self):
        if len(self.phaseMultifileNames) > 0:
            self.currentMfname = self.phaseMultifileNames.pop()
            self.curMultifileRetry = 0
            self.getMultifile(self.currentMfname)
        else:
            if self.currentMfname is None:
                self.notify.warning('no multifile found! See below for debug info:')
                for i in range(self.dldb.getServerNumMultifiles()):
                    mfname = self.dldb.getServerMultifileName(i)
                    phase = self.dldb.getServerMultifilePhase(mfname)
                    print(i, mfname, phase)

                self.handleGenericMultifileError()
            decompressedMfname = os.path.splitext(self.currentMfname)[0]
            localFilename = Filename(self.mfDir, Filename(decompressedMfname))
            nextIndex = self.LauncherPhases.index(self.currentPhase) + 1
            if nextIndex < len(self.LauncherPhases):
                self.MakeNTFSFilesGlobalWriteable(localFilename)
            else:
                self.MakeNTFSFilesGlobalWriteable()
            vfs = VirtualFileSystem.getGlobalPtr()
            vfs.mount(localFilename, '.', VirtualFileSystem.MFReadOnly)
            self.setPercentPhaseComplete(self.currentPhase, 100)
            self.notify.info('Done updating multifiles in phase: ' + repr((self.currentPhase)))
            self.progressSoFar += int(round(self.phaseOverallMap[self.currentPhase] * 100))
            self.notify.info('progress so far ' + repr((self.progressSoFar)))
            messenger.send('phaseComplete-' + repr((self.currentPhase)))
            if nextIndex < len(self.LauncherPhases):
                self.currentPhase = self.LauncherPhases[nextIndex]
                self.currentPhaseIndex = nextIndex + 1
                self.currentPhaseName = self.Localizer.LauncherPhaseNames[self.currentPhase]
                self.updatePhase(self.currentPhase)
            else:
                self.notify.info('ALL PHASES COMPLETE')
                self.maybeStartGame()
                messenger.send('launcherAllPhasesComplete')
                self.cleanup()
        return

    def isDownloadComplete(self): # TODO
        return True

    def launcherMessage(self, msg):
        if msg != self.lastLauncherMsg:
            self.lastLauncherMsg = msg
            self.notify.info(msg)

    def isTestServer(self):
        return self.testServerFlag

    def recordPeriodTimeRemaining(self, secondsRemaining):
        self.setValue(self.PeriodTimeRemainingKey, int(secondsRemaining))

    def recordPeriodName(self, periodName):
        self.setValue(self.PeriodNameKey, periodName)

    def recordSwid(self, swid):
        self.setValue(self.SwidKey, swid)

    def getGoUserName(self):
        return self.goUserName

    def setGoUserName(self, userName):
        self.goUserName = userName

    def setPandaWindowOpen(self):
        self.setValue(self.PandaWindowOpenKey, 1)

    def setPandaErrorCode(self, code):
        self.notify.info('setting panda error code to %s' % code)
        self.pandaErrorCode = code
        errorLog = open(self.errorfile, 'w')
        errorLog.write(str(code) + '\n')
        errorLog.flush()
        errorLog.close()

    def getPandaErrorCode(self):
        return self.pandaErrorCode

    def setDisconnectDetailsNormal(self):
        self.notify.info('Setting Disconnect Details normal')
        self.disconnectCode = 0
        self.disconnectMsg = 'normal'

    def setDisconnectDetails(self, newCode, newMsg):
        self.notify.info('New Disconnect Details: %s - %s ' % (newCode, newMsg))
        self.disconnectCode = newCode
        self.disconnectMsg = newMsg

    def setServerVersion(self, version):
        self.ServerVersion = version

    def getServerVersion(self):
        return self.ServerVersion

    def getIsNewInstallation(self):
        result = self.getValue(self.NewInstallationKey, 1)
        result = base.config.GetBool('new-installation', result)
        return result

    def setIsNotNewInstallation(self):
        self.setValue(self.NewInstallationKey, 0)

    def getLastLogin(self):
        return self.getValue(self.LastLoginKey, '')

    def setLastLogin(self, login):
        self.setValue(self.LastLoginKey, login)

    def setUserLoggedIn(self):
        self.setValue(self.UserLoggedInKey, '1')

    def setPaidUserLoggedIn(self):
        self.setValue(self.PaidUserLoggedInKey, '1')

    def getReferrerCode(self):
        return self.getValue(self.ReferrerKey, None)

    def getPhaseComplete(self, phase): # TODO
        return True

    def setPercentPhaseComplete(self, phase, percent):
        self.notify.info('phase updating %s, %s' % (phase, percent))
        oldPercent = self.phaseComplete[phase]
        if oldPercent != percent:
            self.phaseComplete[phase] = percent
            messenger.send('launcherPercentPhaseComplete', [
             phase, percent, self.getBandwidth(), self.byteRate])
            percentPhaseCompleteKey = 'PERCENT_PHASE_COMPLETE_' + repr(phase)
            self.setRegistry(percentPhaseCompleteKey, percent)
            self.overallComplete = int(round(percent * self.phaseOverallMap[phase])) + self.progressSoFar
            self.setRegistry('PERCENT_OVERALL_COMPLETE', self.overallComplete)

    def getPercentPhaseComplete(self, phase):
        return 100

    def cleanup(self):
        self.notify.info('cleanup: cleaning up Launcher')
        self.ignoreAll()
        del self.clock
        del self.http

    def getBlue(self):
        return

    def getPlayToken(self):
        return

    def getDISLToken(self):
        DISLToken = self.getValue(self.DISLTokenKey)
        self.setValue(self.DISLTokenKey, '')
        if DISLToken == 'NO DISLTOKEN':
            DISLToken = None
        return DISLToken