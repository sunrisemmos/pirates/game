from otp.avatar.DistributedAvatarUD import DistributedAvatarUD
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedPlayerUD(DistributedAvatarUD):
    notify = directNotify.newCategory('DistributedPlayerUD')

    def arrivedOnDistrict(self, districtId):
        pass

    def setAccountName(self, accountName):
        pass

    def setChat(self, chatString, chatFlags, DISLid):
        pass

    def setWLChat(self, chatString, chatFlags, DISLid):
        pass

    def setWhisperFrom(self, fromId, chatString, senderDISLid):
        pass

    def setWhisperWLFrom(self, fromId, chatString, senderDISLid):
        pass

    def setWhisperSCFrom(self, fromId, msgIndex):
        pass

    def setWhisperSCCustomFrom(self, fromId, msgIndex):
        pass

    def setWhisperSCEmoteFrom(self, fromId, emoteId):
        pass

    def setSystemMessage(self, aboutId, chatString):
        pass

    def setCommonChatFlags(self, flags):
        pass

    def setSC(self, msgIndex):
        pass

    def setSCCustom(self, msgIndex):
        pass

    def setFriendsList(self, friendsList):
        pass

    def setDISLname(self, name):
        pass

    def setDISLid(self, id_):
        pass

    def setAccessLevel(self, access):
        pass

    def OwningAccount(self, doId):
        pass

    def WishName(self, name):
        pass

    def WishNameState(self, state):
        pass

    def setPreviousAccess(self, access):
        pass

    def setAccess(self, access):
        pass