from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD

class StatusDatabaseUD(DistributedObjectGlobalUD):
    notify = directNotify.newCategory("StatusDatabaseUD")

    def requestOfflineAvatarStatus(self, avIds):
        # We've received a list of avatar IDs.
        # We'll need to request each avatar's field data
        # to get their last logout.

        sender = self.air.getAvatarIdFromSender()

        index = 0

        def handleRetrieve(dclass, fields):
            if 'setLastLogout' not in fields:
                # The dclass doesn't have the field we need.
                return

            lastLogout = fields['setLastLogout'][0]

            self.sendUpdateToAvatarId(sender, 'recvOfflineAvatarStatus', [avIds[index], lastLogout])

        for avId in avIds:
            self.air.dbInterface.queryObject(self.air.dbId, avId, handleRetrieve)

            index += 1