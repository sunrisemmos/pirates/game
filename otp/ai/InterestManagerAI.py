from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.PyDatagram import *
from direct.showbase.DirectObject import DirectObject

class InterestManagerAI(DirectObject):
    notify = directNotify.newCategory('InterestManagerAI')
    StartingRange = range(0x8000, 0x8FFF)
    HandleAmount = 0xFFF
    LastHandle = 0xFFFF
    FirstMask = 0x8000
    ManagementHandle = FirstMask
    ShardHandle = ManagementHandle + 1
    UberZoneHandle = ShardHandle + 1

    def __init__(self, air):
        DirectObject.__init__(self)

        self.air = air

        self.registeredRanges = {}
        self.openedInterests = {}

    def registerHandleSpace(self, objId, handler):
        """
        Returns interest handle space (up to 4095 IDs) for a particular object.
        New handle space is registered for new handlers.
        """

        # Check if we already have registered ranges for this object.
        if objId in self.registeredRanges:
            # Check if we already have a range for this handler.
            if handler in self.registeredRanges[objId]:
                # We already have registered space, so return it:
                return self.registeredRanges[objId][handler]

            # Get the last mask which was registered.
            lastMask = self.registeredRanges[objId]['LastMask']
            if lastMask >= self.LastHandle:
                # No space left to be registered.
                return

            # Get the last mask, increment it, and add 4095 potential handles to it.
            newLastMask = lastMask + 1 + self.HandleAmount

            # Make the range from our first mask to our last mask.
            self.registeredRanges[objId][handler] = list(range(lastMask + 1, newLastMask))

            # Update the last mask.
            self.registeredRanges[objId]['LastMask'] = newLastMask

            # Return the new registered handle space.
            return self.registeredRanges[objId][handler]

        # Get the last mask we will have, which is the first mask
        # plus the amount of potential handles we will have.
        lastMask = self.FirstMask + self.HandleAmount

        # Set up the object in our registered ranges.
        self.registeredRanges[objId] = {handler: list(range(self.FirstMask, lastMask)),
                                        'LastMask': lastMask}

        # Store the object ID and handler in an opened interest dictionary
        # for later use.
        self.openedInterests[objId] = {}

        # Return the newly registered handle space.
        return self.registeredRanges[objId][handler]

    def useNewHandle(self, objId, handler):
        """
        Fetches a new, unused handle out of registered handle space.
        """

        if not self.registeredRanges.get(objId, {}).get(handler):
            # We don't have registered handle space.
            return

        # Take out a new handle based off of the first available handle
        # in our registered space.
        newHandle = self.registeredRanges[objId][handler].pop(0)

        # Put the new handle in our opened interests dictionary.
        self.openedInterests[objId][handler] = newHandle

        # Return the handle.
        return newHandle

    def putBackHandle(self, objId, handler, handle):
        """
        Puts back a used handle into an object's handle space.
        """

        if not self.registeredRanges.get(objId, {}).get(handler):
            # We don't have registered handle space.
            return

        # Put the handle back.
        self.registeredRanges[objId][handler].insert(0, handle)

    def openInterest(self, clientChannel, handle, parentId, zones):
        """
        This is a utility function to add interest from the AI on any amount of zones.
        """

        datagram = PyDatagram()
        datagram.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_ADD_INTEREST_MULTIPLE)
        datagram.addUint16(handle)
        datagram.addUint32(parentId)
        datagram.addUint16(len(zones))
        for zoneId in zones:
            datagram.addUint32(zoneId)
        self.air.send(datagram)

    def closeInterest(self, clientChannel, handle):
        """
        This is a utility function to remove an interest by its handle.
        """

        datagram = PyDatagram()
        datagram.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_REMOVE_INTEREST)
        datagram.addUint16(handle)
        self.air.send(datagram)