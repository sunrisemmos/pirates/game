from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD
from direct.directnotify.DirectNotifyGlobal import directNotify

class PlayerFriendsManagerUD(DistributedObjectGlobalUD):
    notify = directNotify.newCategory('PlayerFriendsManagerUD')

    def __init__(self, air):
        DistributedObjectGlobalUD.__init__(self, air)