# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: otp.friends.FriendResponseCodes
INVITATION_RESP_OK = 0
INVITATION_RESP_DECLINE = 1
INVITATION_RESP_RETRACT = 2
INVITATION_RESP_CANCEL = 3
INVITATION_RESP_ALREADY_FRIENDS = 4
INVITATION_RESP_NEW_FRIENDS = 5