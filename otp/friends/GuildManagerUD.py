from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD
from direct.distributed.PyDatagram import *
from direct.distributed.MsgTypes import *
from direct.fsm.FSM import FSM

from otp.uberdog.RejectCode import RejectCode

from pickle import dumps, loads
from string import ascii_letters

import random, os

def retrieveFields(guildManager, target, callback):
    """
    Utility function to retrieve avatar fields.
    """

    def handleRetrieve(dclass, fields):
        if 'setName' not in fields:
            # The dclass is not an avatar with the essential fields.
            guildManager.air.writeServerEvent('suspicious', target, 'Target is not an avatar with necessary fields!')
            return

        guildId = 0
        guildName = 'Null'

        guildData = guildManager.getGuildDataByAvId(target)
        if guildData:
            guildId = guildData['Id']
            guildName = guildData['Name']

        fields['setGuildId'] = [guildId]
        fields['setGuildName'] = [guildName]

        callback(dclass.getName() + 'UD', fields)
    guildManager.air.dbInterface.queryObject(guildManager.air.dbId, target, handleRetrieve)

def avSubscribe(guildManager, avId, guildId):
    """
    Utility function to subscribe avatars to guild channels.
    """

    datagram = PyDatagram()
    datagram.addServerHeader(guildManager.GetPuppetConnectionChannel(avId), guildManager.air.ourChannel, CLIENTAGENT_OPEN_CHANNEL)
    datagram.addChannel(guildId)
    guildManager.air.send(datagram)

class GuildCreationOperation(FSM):
    notify = directNotify.newCategory("GuildCreationOperation")

    def __init__(self, guildManager, target):
        FSM.__init__(self, self.__class__.__name__)
        self.guildManager = guildManager
        self.target = target

    def enterStart(self):
        retrieveFields(self.guildManager, self.target, self.__handleRetrieve)

    def __handleRetrieve(self, dclass, fields):
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        if guildId or (guildName and guildName != 'Null'):
            # Suspicious! Players cannot create a guild
            # when they're already in one.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player tried creating a guild when already in one!')
            return

        # Allocate a new guild ID and the default guild name.
        guildId = self.guildManager.allocateChannel()
        guildName = 'Pirate Guild'

        # Put all of the initial guild data into a dict.
        initialGuildData = {'Id': guildId, 'Name': guildName, 'Members': {self.target: {'Name': fields['setName'][0], 'Rank': 3}}, 'Tokens': {}}

        # Subscribe the player to the guild channel.
        avSubscribe(self.guildManager, self.target, guildId)

        # Update the player's guild ID and name.
        self.guildManager.updateGuildInfo(self.target, guildId, guildName)

        # Update the player's guild status.
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildStatusUpdate', [guildId, guildName, 3])

        # Save the guild to a file.
        self.guildManager.saveGuild(initialGuildData)

class GuildNameOperation(FSM):
    notify = directNotify.newCategory('GuildNameOperation')

    def __init__(self, guildManager, target, newName):
        FSM.__init__(self, self.__class__.__name__)
        self.guildManager = guildManager
        self.target = target
        self.newName = newName

    def enterStart(self):
        retrieveFields(self.guildManager, self.target, self.__handleRetrieve)

    def __handleRetrieve(self, dclass, fields):
        self.dclass = dclass

        self.guildId = fields['setGuildId'][0]
        self.guildName = fields['setGuildName'][0]
        self.guildData = self.guildManager.loadGuild(guildId, guildName)

        # Now, check the new name.
        self.demand('JudgeName')

    def enterJudgeName(self):
        # TODO: Send names to an online API for moderators to
        # reject/approve and have the player wait for
        # the name to be approved.

        # Make sure this name is unique.
        guilds = self.guildManager.getGuilds()
        for guildData in guilds:
            if self.newName == guildData['Name']:
                # This name is not unique.
                self.guildManager.sendUpdateToAvatarId(self.target, 'guildNameReject', [self.guildId])
                return

        # For now, just set it.
        self.demand('SetName')

    def enterSetName(self):
        for avId, memberData in list(self.guildData['Members'].items()):
            # Update the guild name and status for every guild member
            # to reflect the guild name change.

            self.guildManager.updateGuildInfo(avId, self.guildId, self.newName)
            self.guildManager.sendUpdateToAvatarId(avId, 'guildStatusUpdate', [self.guildId, self.newName, memberData['Rank']])

        # Reflect the guild name change in the guild data.
        self.guildData['Name'] = self.newName

        # Save the new guild data to the file.
        self.saveGuild(self.guildData)

        # Finally, send the response to the client.
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildNameChange', [self.newName, 0])

class GuildInviteOperation(FSM):
    notify = directNotify.newCategory("GuildInviteOperation")

    def __init__(self, guildManager, sender, target):
        FSM.__init__(self, self.__class__.__name__)

        self.guildManager = guildManager
        self.sender = sender
        self.target = target

    def enterStart(self):
        retrieveFields(self.guildManager, self.sender, self.__handleInviterRetrieve)

    def __handleInviterRetrieve(self, dclass, fields):
        self.senderName = fields['setName'][0]

        self.guildId = fields.get('setGuildId', [0])[0]
        self.guildName = fields.get('setGuildName', [0])[0]
        self.guildData = self.guildManager.loadGuild(self.guildId, self.guildName)

        if not self.guildId or not self.guildName or self.guildName == 'Null':
            # Suspicious! A player can't invite someone to a guild
            # when they aren't part of one to begin with...
            self.guildManager.air.writeServerEvent('suspicious', self.sender, 'Player tried inviting to guild when not in one!')
            return

        # Now, let's retrieve the invitee's fields.
        retrieveFields(self.guildManager, self.target, self.__handleInviteeRetrieve)

    def __handleInviteeRetrieve(self, dclass, fields):
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        # There may be more rejection types that we aren't covering...

        if len(self.guildData['Members']) > 500:
            # Reject the invitation since the guild is full.
            self.guildManager.sendUpdateToAvatarId(self.sender, 'guildRejectInvite', [self.target, RejectCode.GUILD_FULL])
            return

        if guildId or (guildName and guildName != 'Null'):
            # Reject the invitation since the invitee is
            # already part of a guild.
            self.guildManager.sendUpdateToAvatarId(self.sender, 'guildRejectInvite', [self.target, RejectCode.LEAVE_CURRENT_GUILD_FIRST])
            return

        # We need to store a map of the invitee with the guild data.
        # As well as the inviter's data...
        self.guildManager.invitee2guild[self.target] = (self.sender, self.senderName, self.self.guildData)

        # Alright, let's process the invite.
        self.guildManager.sendUpdateToAvatarId(self.target, 'invitationFrom', [self.sender, self.senderName, self.guildId, self.guildName])

class GuildNewMemberOperation(FSM):
    notify = directNotify.newCategory("GuildNewMemberOperation")

    def __init__(self, guildManager, target, inviterId, inviterName, guildData):
        FSM.__init__(self, self.__class__.__name__)

        self.guildManager = guildManager
        self.target = target
        self.inviterId = inviterId
        self.inviterName = inviterName
        self.guildData = guildData

    def enterStart(self):
        retrieveFields(self.guildManager, self.target, self.__handleRetrieve)

    def __handleRetrieve(self, dclass, fields):
        # First, let's get the avatar's name.
        name = fields['setName'][0]

        # To avoid race conditions, let's make sure
        # our guild data is up to date.
        self.guildData = self.guildManager.loadGuild(self.guildData['Id'], self.guildData['Name'])

        # Subscribe the avatar to the guild channel.
        avSubscribe(self.guildManager, self.target, self.guildData['Id'])

        # We need to update the guild fields for the avatar.
        self.guildManager.updateGuildInfo(self.target, self.guildData['Id'], self.guildData['Name'])

        # Now, we need to update the guild data.
        self.guildData['Members'][self.target] = {'Name': name, 'Rank': 1}
        self.guildManager.saveGuild(self.guildData)

        # Then, we need to perform a status update.
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildStatusUpdate', [self.guildData['Id'], self.guildData['Name'], 1])

        # Finally, we need to inform all other guild members that
        # a new member has been added to the guild.
        # N.B. This sends an update to the guild channel, so
        # every guild member (who should have interest) should
        # receive this update.
        memberData = self.guildData['Members'][self.target]
        memberInfo = [self.target, memberData['Name'], memberData['Rank'], 1, 0, 0]
        self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberAdded', [memberInfo, self.inviterId, self.inviterName])

class GuildMemberListOperation(FSM):
    notify = directNotify.newCategory("GuildMemberListOperation")

    def __init__(self, guildManager, target):
        FSM.__init__(self, self.__class__.__name__)

        self.guildManager = guildManager
        self.target = target

    def enterStart(self):
        retrieveFields(self.guildManager, self.target, self.__handleRetrieve)

    def __handleRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        if not guildId or not guildName or guildName == 'Null':
            # Almost certainly a hacker if they get here without
            # being a member of a guild already.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player tried getting member list when not part of guild!')
            return

        guildData = self.guildManager.loadGuild(guildId, guildName)

        for avId, memberData in list(guildData['Members'].items()):
            memberInfo = [avId, memberData['Name'], memberData['Rank'], 1, 0, 0]

            # Receive the individual member.
            self.guildManager.sendUpdateToAvatarId(self.target, 'receiveMember', [memberInfo])

        # We're done receiving members, now display the list.
        self.guildManager.sendUpdateToAvatarId(self.target, 'receiveMembersDone', [])

class GuildChangeRankOperation(FSM):
    notify = directNotify.newCategory("GuildChangeRankOperation")

    def __init__(self, guildManager, sender, target, rank):
        FSM.__init__(self, self.__class__.__name__)

        self.guildManager = guildManager
        self.sender = sender
        self.target = target
        self.rank = rank

    def enterStart(self):
        retrieveFields(self.guildManager, self.sender, self.__handleSenderRetrieve)

    def __handleSenderRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        self.senderName = fields['setName'][0]
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        if not guildId or not guildName or guildName == 'Null':
            # Almost certainly a hacker if they get here without
            # being a member of a guild already.
            self.guildManager.air.writeServerEvent('suspicious', self.sender, 'Player tried changing rank when not part of guild!')
            return

        self.guildData = self.guildManager.loadGuild(guildId, guildName)
        memberData = self.guildData['Members'][self.sender]

        rank = memberData['Rank']
        if rank != 3:
            # Only the Guildmaster may promote/demote guild members.
            self.guildManager.air.writeServerEvent('suspicious', self.sender, 'Player tried changing player\'s guild rank when not Guildmaster!')
            return

        retrieveFields(self.guildManager, self.target, self.__handleTargetRetrieve)

    def __handleTargetRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        name = fields['setName'][0]
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        if not guildId or not guildName or guildName == 'Null':
            # This should be impossible.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player tried changing rank of player not part of guild!')
            return

        memberData = self.guildData['Members'][self.target]

        rank = memberData['Rank']
        if self.rank > rank:
            # This is a promotion.
            promote = True
        elif self.rank < rank:
            # This is a demotion.
            promote = False
        else:
            # This should be impossible.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player tried changing rank of player to the same rank!')
            return

        if rank == 3:
            # Hmm... the Guildmaster is swapping
            # his rank with the target.
            self.guildData['Members'][self.sender]['Rank'] = 2
            self.guildManager.sendUpdateToAvatarId(self.sender, 'guildStatusUpdate', [self.guildData['Id'], self.guildData['Name'], 2])
            self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberUpdateRank', [self.sender, self.sender, self.senderName, self.senderName, 2, False])

        # Save all of the changes.
        self.guildData['Members'][self.target]['Rank'] = self.rank
        self.guildManager.saveGuild(self.guildData)
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildStatusUpdate', [self.guildData['Id'], self.guildData['Name'], self.rank])

        # Broadcast the changes to all guild members.
        self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberUpdateRank', [self.target, self.sender, name, self.senderName, self.rank, promote])

class GuildRemoveMemberOperation(FSM):
    notify = directNotify.newCategory("GuildRemoveMemberOperation")

    def __init__(self, guildManager, sender, target):
        FSM.__init__(self, self.__class__.__name__)

        self.guildManager = guildManager
        self.sender = sender
        self.target = target

    def enterStart(self):
        retrieveFields(self.guildManager, self.sender, self.__handleSenderRetrieve)

    def __handleSenderRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        self.senderName = fields['setName'][0]
        guildId = fields.get('setGuildId', [0])[0]
        guildName = fields.get('setGuildName', [0])[0]

        if not guildId or not guildName or guildName == 'Null':
            # Almost certainly a hacker if they get here without
            # being a member of a guild already.
            self.guildManager.air.writeServerEvent('suspicious', self.sender, 'Player tried removing member when not part of guild!')
            return

        self.guildData = self.guildManager.loadGuild(guildId, guildName)
        memberData = self.guildData['Members'][self.sender]
        rank = memberData['Rank']

        if self.sender == self.target:
            # Ah, a member is leaving.
            # We won't have to proceed with the target retrieval.
            if rank == 3:
                # Hmm, this is a problem.
                # If the Guildmaster wants to leave, we have to choose
                # a random person of the highest rank possible
                # to be the new Guildmaster.

                self.target = 0
                curRank = 4
                while curRank:
                    for memberId, memberDict in list(self.guildData['Members'].items()):
                        if memberDict['Rank'] == curRank:
                            # Works on my machine.
                            self.target = memberId
                            break

                    curRank -= 1
                    if curRank == 3:
                        curRank -= 1

                if self.target:
                    retrieveFields(self.guildManager, self.target, self.__handleGMTargetRetrieve)
                else:
                    # The guild must be empty.
                    pass

            # At this point, the member leaving handling
            # is the same regardless of rank.

            # Remove the member from the guild in the database.
            self.guildManager.updateGuildInfo(self.sender)

            # Save all of the changes.
            del self.guildData['Members'][self.sender]
            self.guildManager.saveGuild(self.guildData)
            self.guildManager.sendUpdateToAvatarId(self.sender, 'guildStatusUpdate', [0, 'Null', -1])

            # Broadcast the changes to all guild members.
            self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberRemoved', [self.sender, self.sender, self.senderName, self.senderName])
            return

        if rank < 2:
            # Only Guildmasters and Officers may remove guild members.
            self.guildManager.air.writeServerEvent('suspicious', self.sender, 'Player tried removing player from guild when not Officer/Guildmaster!')
            return

        retrieveFields(self.guildManager, self.target, self.__handleTargetRetrieve)

    def __handleTargetRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        name = fields['setName'][0]
        guildId = fields['setGuildId'][0]
        guildName = fields['setGuildName'][0]

        if not guildId or not guildName or guildName == 'Null':
            # This should be impossible.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player tried removing player not part of guild!')
            return

        memberData = self.guildData['Members'][self.target]

        rank = memberData['Rank']
        if self.rank == 2 and rank == 2 or rank == 3:
            # Officers can't remove other officers or Guildmasters.
            return

        # Remove the target from the guild in the database.
        self.guildManager.updateGuildInfo(self.target)

        # Save all of the changes.
        del self.guildData['Members'][self.target]
        self.guildManager.saveGuild(self.guildData)
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildStatusUpdate', [0, 'Null', -1])

        # Broadcast the changes to all guild members.
        self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberRemoved', [self.target, self.sender, name, self.senderName])

    def __handleGMTargetRetrieve(self, dclass, fields):
        # Collect all of the information we need.
        name = fields['setName'][0]
        guildId = fields['setGuildId'][0]
        guildName = fields['setGuildName'][0]

        if not guildId or not guildName or guildName == 'Null':
            # This should be impossible.
            self.guildManager.air.writeServerEvent('suspicious', self.target, 'Player becoming Guildmaster not part of guild!')
            return

        # Save all of the changes.
        self.guildData['Members'][self.target]['Rank'] = 3
        self.guildManager.saveGuild(self.guildData)
        self.guildManager.sendUpdateToAvatarId(self.target, 'guildStatusUpdate', [guildId, guildName, 3])

        # Broadcast the changes to all guild members.
        self.guildManager.sendUpdateToChannel(self.guildData['Id'], 'recvMemberUpdateRank', [self.target, '', name, 'System', 3, True])

class GuildManagerUD(DistributedObjectGlobalUD):
    notify = directNotify.newCategory("GuildManagerUD")

    def __init__(self, air):
        DistributedObjectGlobalUD.__init__(self, air)

        # Make sure the `guilds` directory exists.
        if not os.path.isdir('guilds'):
            os.mkdir('guilds')

        # Disney didn't feel like supplying the guildId
        # for guild invite rejecting/accepting, so
        # we have to keep a fucking map of invitee->guild.
        # Oh, and not only that, but we also have to store
        # the inviter's fucking data.
        self.invitee2guild = {}

    def online(self):
        pass

    def getFileName(self, guildId, guildName):
        return '%s-%s.guild' % (str(guildId), guildName)

    def saveGuild(self, guildData):
        fileName = self.getFileName(guildData['Id'], guildData['Name'])

        with open(os.path.join('guilds', fileName), 'wb+') as f:
            f.write(dumps(guildData))

    def loadGuild(self, guildId, guildName):
        fileName = self.getFileName(guildId, guildName)

        with open(os.path.join('guilds', fileName), 'rb') as f:
            guildData = loads(f.read())

        return guildData

    def getGuilds(self):
        guilds = []

        for root, dirs, files in os.walk(os.path.join(os.getcwd(), 'guilds')):
            for fileName in [n for n in files if n.endswith('.guild')]:
                path = root + '/' + fileName

                with open(path, 'rb') as f:
                    guilds.append(loads(f.read()))

        return guilds

    def getGuildDataByAvId(self, avId):
        guildData = {}
        guilds = self.getGuilds()

        for data in guilds:
            for member in data['Members']:
                if member == avId:
                    guildData = data

        return guildData

    def getGuildIdByAvId(self, avId):
        guildId = 0
        guilds = self.getGuilds()

        for guildData in guilds:
            for member in guildData['Members']:
                if member == avId:
                    guildId = guildData['Id']

        return guildId

    def allocateChannel(self):
        channel = self.air.allocateChannel()

        guilds = self.getGuilds()
        for guild in guilds:
            if guild['Id'] == channel:
                channel += 1
                continue

        return channel

    def allocateToken(self):
        # First, generate four random letters.
        letterToken = ''
        for i in range(4):
            letterToken += random.choice(ascii_letters)

        # Next, generate a random integer between 1000 and 9999.
        numberToken = random.randint(1000, 9999)

        # Finally, return the combination of the two tokens.
        return letterToken + str(numberToken)

    def requestInvite(self, avatarId):
        sender = self.air.getAvatarIdFromSender()

        GuildInviteOperation(self, sender, avatarId).demand('Start')

    def memberList(self):
        sender = self.air.getAvatarIdFromSender()

        GuildMemberListOperation(self, sender).demand('Start')

    def createGuild(self):
        sender = self.air.getAvatarIdFromSender()

        GuildCreationOperation(self, sender).demand('Start')

    def acceptInvite(self):
        sender = self.air.getAvatarIdFromSender()

        if sender not in self.invitee2guild:
            # This is suspicious. All invitees are properly stored.
            self.air.writeServerEvent('suspicious', sender, 'Invitee not in guild map!')
            return

        avatarId, avatarName, guildData = self.invitee2guild[sender]

        del self.invitee2guild[sender]

        # Run a GuildNewMemberOperation.
        GuildNewMemberOperation(self, sender, avatarId, avatarName, guildData).demand('Start')

        # Inform the inviter that the invitee accepted the invitation.
        self.sendUpdateToAvatarId(avatarId, 'guildAcceptInvite', [sender])

    def declineInvite(self):
        sender = self.air.getAvatarIdFromSender()

        if sender not in self.invitee2guild:
            # This is suspicious. All invitees are properly stored.
            self.air.writeServerEvent('suspicious', sender, 'Invitee not in guild map!')
            return

        avatarId, avatarName, guildData = self.invitee2guild[sender]

        del self.invitee2guild[sender]

        self.sendUpdateToAvatarId(avatarId, 'guildRejectInvite', [sender, RejectCode.NO_GUILD])

    def setWantName(self, newName):
        sender = self.air.getAvatarIdFromSender()

        GuildNameOperation(self, sender, newName).demand('Start')

    def removeMember(self, avatarId):
        sender = self.air.getAvatarIdFromSender()

        GuildRemoveMemberOperation(self, sender, avatarId).demand('Start')

    def changeRank(self, avatarId, rank):
        sender = self.air.getAvatarIdFromSender()

        GuildChangeRankOperation(self, sender, avatarId, rank).demand('Start')

    def statusRequest(self):
        pass

    def requestLeaderboardTopTen(self):
        pass

    def updateRep(self, todo0, todo1):
        pass

    def setTalkGroup(self, fromAv, fromAC, avatarName, chat, mods, flags):
        sender = self.air.getAvatarIdFromSender()

        guildId = self.getGuildIdByAvId(sender)
        if not guildId:
            # This is suspicious. You can't talk if you aren't in the guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to guild talk when not in guild!')
            return

        # TODO: Parse through whitelist
        # (this should be implemented at the same time as the ChatRouter)

        # Now this is very simple in comparison to the ChatRouter.
        # Since every guild member has subscribed to the guild
        # channel, we can simply use sendUpdateToChannel
        # instead of an aiFormatUpdate to receive messages.
        self.sendUpdateToChannel(guildId, 'setTalkGroup', [0, 0, '', chat, [], 0])

    def sendChat(self, msgText, chatFlags, unused, field='recvChat'):
        sender = self.air.getAvatarIdFromSender()

        guildId = self.getGuildIdByAvId(sender)
        if not guildId:
            # This is suspicious. You can't chat if you aren't in the guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to guild chat when not in guild!')
            return

        # TODO: Parse through whitelist
        # (this should be implemented at the same time as the ChatRouter)

        self.sendUpdateToChannel(guildId, field, [sender, msgText, chatFlags, sender])

    def sendWLChat(self, msgText, chatFlags, unused):
        """
        This is literally the same thing as sendChat but
        a different field to update, so yeah...
        """

        self.sendChat(msgText, chatFlags, unused, 'recvWLChat')

    def sendSC(self, msgIndex):
        sender = self.air.getAvatarIdFromSender()

        guildId = self.getGuildIdByAvId(sender)
        if not guildId:
            # This is suspicious. You can't chat if you aren't in the guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to guild chat when not in guild!')
            return

        self.sendUpdateToChannel(guildId, 'recvSC', [sender, msgIndex])

    def sendSCQuest(self, questInt, msgType, taskNum):
        sender = self.air.getAvatarIdFromSender()

        guildId = self.getGuildIdByAvId(sender)
        if not guildId:
            # This is suspicious. You can't chat if you aren't in the guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to guild chat when not in guild!')
            return

        self.sendUpdateToChannel(guildId, 'recvSCQuest', [sender, questInt, msgType, taskNum])

    def sendTokenRequest(self):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if not guildData:
            # This is suspicious. You can't request a token when not in a guild!
            self.air.writeServerEvent('suspicious', sender, 'Player tried to request token when not in guild!')
            return

        memberData = guildData['Members'].get(sender)
        if not memberData:
            # This is suspicious. Player isn't part of this guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to request token when not part of guild!')
            return

        rank = memberData['Rank']
        if rank != 3:
            # This is suspicious. You can't request a token when not the Guildmaster.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to request token when not Guildmaster!')
            return

        token = self.allocateToken()

        if len(guildData['Members']) > 500:
            # No token, the guild is full.
            return

        # This is the preExistPerm, which is just a variable that
        # decides whether the invite is unlimited use or not.
        # There also seems to be multiple use and single use.
        # For now, all invites may be unlimited use.
        preExistPerm = 0

        # Set up the token data for the guild data.
        tokenData = {'Perm': preExistPerm}
        guildData['Tokens'][token] = tokenData
        self.saveGuild(guildData)

        self.sendUpdateToAvatarId(sender, 'recvTokenInviteValue', [token, preExistPerm])

    def sendTokenForJoinRequest(self, token, name):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if guildData:
            # This is suspicious. Player is already part of a guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to redeem token when part of guild!')
            self.sendUpdateToAvatarId(sender, 'recvTokenRedeemMessage', ['***ERROR - GUILD CODE INVALID***'])
            return

        # First, we need to check if this token is valid.
        guildData = {}
        tokenData = {}
        guilds = self.getGuilds()
        for guildD in guilds:
            for tokenStr, tokenD in list(guildD['Tokens'].items()):
                if token == tokenStr:
                    guildData = guildD
                    tokenData = tokenD

        if not tokenData or not guildData:
            # This token is not valid.
            self.sendUpdateToAvatarId(sender, 'recvTokenRedeemMessage', ['***ERROR - GUILD CODE INVALID***'])
            return

        # TODO: Check if guild is full

        if len(guildData['Members']) > 500:
            # This guild is full.
            self.sendUpdateToAvatarId(sender, 'recvTokenRedeemMessage', ['***ERROR - GUILD FULL***'])
            return

        # Alright, the token is valid. Let's eliminate the token
        # if it's single use.
        if tokenData['Perm'] == 2:
            del guildData['Tokens'][token]
            self.saveGuild(guildData)

        # Now, let's run a GuildNewMemberOperation.
        GuildNewMemberOperation(self, sender, 0, '', guildData).demand('Start')

        # Send any final updates we have to send.
        self.sendUpdateToAvatarId(sender, 'recvTokenRedeemMessage', [guildData['Name']])
        self.sendUpdateToChannel(guildData['Id'], 'recvTokenRedeemedByPlayerMessage', [name])

    def sendTokenRValue(self, tokenString, rValue):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if not guildData:
            # This is suspicious. Player isn't part of a guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to set token perm when not in guild!')
            return

        memberData = guildData['Members'].get(sender)
        if not memberData:
            # This is suspicious. Player isn't part of this guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to set token perm when not part of guild!')
            return

        rank = memberData['Rank']
        if rank != 3:
            # This is suspicious. You can't set the token perm when not the Guildmaster.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to set token perm when not Guildmaster!')
            return

        if tokenString not in guildData['Tokens']:
            # This token doesn't exist.
            return

        # Save the changes.
        guildData['Tokens'][tokenString]['Perm'] = rValue
        self.saveGuild(guildData)

    def sendPermToken(self):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if not guildData:
            # This is suspicious. Player isn't part of a guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to receive perm tokens when not in guild!')
            return

        memberData = guildData['Members'].get(sender)
        if not memberData:
            # This is suspicious. Player isn't part of this guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to receive perm tokens when not part of guild!')
            return

        for token, tokenData in list(guildData['Tokens'].items()):
            if tokenData['Perm'] == 0:
                self.sendUpdateToAvatarId(sender, 'recvPermToken', [token])

    def sendNonPermTokenCount(self):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if not guildData:
            # This is suspicious. Player isn't part of a guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to receive non-perm token count when not in guild!')
            return

        memberData = guildData['Members'].get(sender)
        if not memberData:
            # This is suspicious. Player isn't part of this guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to receive non-perm token count when not part of guild!')
            return

        count = 0

        for token, tokenData in list(guildData['Tokens'].items()):
            if tokenData['Perm'] > 0:
                count += 1

        self.sendUpdateToAvatarId(sender, 'recvNonPermTokenCount', [count])

    def sendClearTokens(self, perm):
        sender = self.air.getAvatarIdFromSender()

        guildData = self.getGuildDataByAvId(sender)
        if not guildData:
            # This is suspicious. Player isn't part of a guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to clear tokens when not in guild!')
            return

        memberData = guildData['Members'].get(sender)
        if not memberData:
            # This is suspicious. Player isn't part of this guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to clear tokens when not part of guild!')
            return

        rank = memberData['Rank']
        if rank != 3:
            # This is suspicious. You can't clear the tokens when not the Guildmaster.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to clear tokens when not Guildmaster!')
            return

        # Delete any tokens under the given perm.
        for tokenStr, tokenData in list(guildData['Tokens'].items()):
            if tokenData['Perm'] == perm:
                del guildData['Tokens'][tokenStr]

        # Save the data.
        self.saveGuild(guildData)

    def sendAvatarBandId(self, todo0, todo1, todo2):
        pass

    def notifyGuildKicksMaxed(self):
        pass

    def updateGuildInfo(self, avId, guildId=0, guildName='Null'):
        do = self.air.dclassesByName['DistributedPlayerPirateUD']

        dg1 = do.aiFormatUpdate('setGuildId', avId, avId,
                                self.air.ourChannel,
                                [guildId])
        dg2 = do.aiFormatUpdate('setGuildName', avId, avId,
                                self.air.ourChannel,
                                [guildName])

        self.air.send(dg1)
        self.air.send(dg2)

    def avatarOnline(self, avId, unused):
        guildData = self.getGuildDataByAvId(avId)
        if not guildData:
            # It's okay. Not everyone is part of a guild.
            return

        guildId = guildData['Id']
        guildName = guildData['Name']
        memberData = guildData['Members'][avId]

        # Subscribe the avatar to the guild channel.
        avSubscribe(self, avId, guildId)

        # Update the avatar's status and guild information.
        self.sendUpdateToAvatarId(avId, 'guildStatusUpdate', [guildId, guildData['Name'], memberData['Rank']])

        # Tell everyone else in the guild the avatar came online.
        self.sendUpdateToChannel(guildId, 'recvAvatarOnline', [avId, memberData['Name'], 0, 0])

    def avatarOffline(self, avId):
        guildData = self.getGuildDataByAvId(avId)
        if not guildData:
            # It's okay. Not everyone is part of a guild.
            return

        guildId = guildData['Id']
        avName = guildData['Members'][avId]['Name']

        self.sendUpdateToChannel(guildId, 'recvAvatarOffline', [avId, avName])

    def reflectTeleportQuery(self, todo0, todo1, todo2, todo3, todo4):
        pass

    def reflectTeleportResponse(self, todo0, todo1, todo2, todo3, todo4):
        pass

    def requestGuildMatesList(self, todo0, todo1, todo2):
        pass

    def updateAvatarName(self, avId, name):
        # An avatar's name was changed. Let's check if this
        # avatar was part of a guild.
        guildData = self.getGuildDataByAvId(avId)
        if not guildData:
            # Alright, we don't have to do anything.
            return

        memberInfo = guildData['Members'][avId]

        # Handle the name change.
        # There's not really an efficient way to simply inform
        # every member of a name change, so why not
        # just remove/readd the member?
        self.sendUpdateToChannel(guildData['Id'], 'recvMemberRemoved', [avId, 0, memberInfo['Name'], ''])
        self.sendUpdateToChannel(guildData['Id'], 'recvMemberAdded', [memberInfo, 0, ''])
        guildData['Members'][avId]['Name'] = name
        self.saveGuild(guildData)

    def avatarDeleted(self, avId):
        # An avatar was deleted. Let's check if this avatar
        # was part of a guild.
        guildData = self.getGuildDataByAvId(avId)
        if not guildData:
            # Alright, we don't have to do anything.
            return

        # Handle the deletion.
        self.sendUpdateToChannel(guildData['Id'], 'recvMemberRemoved', [avId, 0, guildData['Members'][avId]['Name'], ''])
        del guildData['Members'][avId]
        self.saveGuild(guildData)