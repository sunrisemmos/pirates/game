-- Message types:
CLIENT_LOGIN                                   = 1
CLIENT_LOGIN_RESP                              = 2
CLIENT_GET_AVATARS                             = 3
-- Sent by the server when it is dropping the connection deliberately.
CLIENT_GO_GET_LOST                             = 4
CLIENT_GET_AVATARS_RESP                        = 5
CLIENT_CREATE_AVATAR                           = 6
CLIENT_CREATE_AVATAR_RESP                      = 7
CLIENT_GET_FRIEND_LIST                         = 10
CLIENT_GET_FRIEND_LIST_RESP                    = 11
CLIENT_GET_AVATAR_DETAILS                      = 14
CLIENT_GET_AVATAR_DETAILS_RESP                 = 15
CLIENT_LOGIN_2                                 = 16
CLIENT_LOGIN_2_RESP                            = 17

CLIENT_OBJECT_UPDATE_FIELD                     = 24
CLIENT_OBJECT_UPDATE_FIELD_RESP                = 24
CLIENT_OBJECT_DISABLE                          = 25
CLIENT_OBJECT_DISABLE_RESP                     = 25
CLIENT_OBJECT_DISABLE_OWNER                    = 26
CLIENT_OBJECT_DISABLE_OWNER_RESP               = 26
CLIENT_OBJECT_DELETE                           = 27
CLIENT_OBJECT_DELETE_RESP                      = 27
CLIENT_SET_ZONE_CMU                            = 29
CLIENT_REMOVE_ZONE                             = 30
CLIENT_SET_AVATAR                              = 32
CLIENT_CREATE_OBJECT_REQUIRED                  = 34
CLIENT_CREATE_OBJECT_REQUIRED_RESP             = 34
CLIENT_CREATE_OBJECT_REQUIRED_OTHER            = 35
CLIENT_CREATE_OBJECT_REQUIRED_OTHER_RESP       = 35
CLIENT_CREATE_OBJECT_REQUIRED_OTHER_OWNER      = 36
CLIENT_CREATE_OBJECT_REQUIRED_OTHER_OWNER_RESP = 36

CLIENT_REQUEST_GENERATES                       = 36

CLIENT_DISCONNECT                              = 37

CLIENT_GET_STATE_RESP                          = 47
CLIENT_DONE_INTEREST_RESP                      = 48

CLIENT_DELETE_AVATAR                           = 49

CLIENT_DELETE_AVATAR_RESP                      = 5

CLIENT_HEARTBEAT                               = 52
CLIENT_FRIEND_ONLINE                           = 53
CLIENT_FRIEND_OFFLINE                          = 54
CLIENT_REMOVE_FRIEND                           = 56

CLIENT_CHANGE_PASSWORD                         = 65

CLIENT_SET_NAME_PATTERN                        = 67
CLIENT_SET_NAME_PATTERN_ANSWER                 = 68

CLIENT_SET_WISHNAME                            = 70
CLIENT_SET_WISHNAME_RESP                       = 71
CLIENT_SET_WISHNAME_CLEAR                      = 72
CLIENT_SET_SECURITY                            = 73

CLIENT_SET_DOID_RANGE                          = 74

CLIENT_GET_AVATARS_RESP2                       = 75
CLIENT_CREATE_AVATAR2                          = 76
CLIENT_SYSTEM_MESSAGE                          = 78
CLIENT_SET_AVTYPE                              = 80

CLIENT_GET_PET_DETAILS                         = 81
CLIENT_GET_PET_DETAILS_RESP                    = 82

CLIENT_ADD_INTEREST                            = 97
CLIENT_REMOVE_INTEREST                         = 99
CLIENT_OBJECT_LOCATION                         = 102

CLIENT_LOGIN_3                                 = 111
CLIENT_LOGIN_3_RESP                            = 110

CLIENT_GET_FRIEND_LIST_EXTENDED                = 115
CLIENT_GET_FRIEND_LIST_EXTENDED_RESP           = 116

CLIENT_SET_FIELD_SENDABLE                      = 120

CLIENT_SYSTEMMESSAGE_AKNOWLEDGE                = 123
CLIENT_CHANGE_GENERATE_ORDER                   = 124

-- new toontown specific login message, adds last logged in, and if child account has parent acount
CLIENT_LOGIN_TOONTOWN                          = 125
CLIENT_LOGIN_TOONTOWN_RESP                     = 126

CLIENT_DISCONNECT_GENERIC                = 1
CLIENT_DISCONNECT_RELOGIN                = 100
CLIENT_DISCONNECT_OVERSIZED_DATAGRAM     = 106
CLIENT_DISCONNECT_NO_HELLO               = 107
CLIENT_DISCONNECT_CHAT_AUTH_ERROR        = 120
CLIENT_DISCONNECT_ACCOUNT_ERROR          = 122
CLIENT_DISCONNECT_NO_HEARTBEAT           = 345
CLIENT_DISCONNECT_INVALID_MSGTYPE        = 108
CLIENT_DISCONNECT_TRUNCATED_DATAGRAM     = 109
CLIENT_DISCONNECT_ANONYMOUS_VIOLATION    = 113
CLIENT_DISCONNECT_FORBIDDEN_INTEREST     = 115
CLIENT_DISCONNECT_MISSING_OBJECT         = 117
CLIENT_DISCONNECT_FORBIDDEN_FIELD        = 118
CLIENT_DISCONNECT_FORBIDDEN_RELOCATE     = 119
CLIENT_DISCONNECT_BAD_VERSION            = 125
CLIENT_DISCONNECT_FIELD_CONSTRAINT       = 127
CLIENT_DISCONNECT_SESSION_OBJECT_DELETED = 153

DATABASE_OBJECT_TYPE_ACCOUNT = 1
DATABASE_OBJECT_TYPE_PIRATE = 2

-- Internal message types
STATESERVER_OBJECT_UPDATE_FIELD = 2004
STATESERVER_OBJECT_DELETE_RAM = 2007

CLIENTAGENT_EJECT = 3004

-- Friend related messages
TOONTOWNCLIENT_FRIEND_ONLINE  = 3501
TOONTOWNCLIENT_FRIEND_OFFLINE = 3502
TOONTOWNCLIENT_CHECK_ONLINE_FRIENDS = 3503
TOONTOWNCLIENT_MAKE_FRIEND = 3504
TOONTOWNCLIENT_REMOVE_FRIEND = 3505

BCHAN_CLIENTS = 4014

STATESERVER_OBJECT_SET_ZONE = 2008

OTP_DO_ID_PIRATES_INVENTORY_MANAGER_BASE = 5001

CHANNEL_PUPPET_ACTION = 4004

ACCOUNT_AVATAR_USAGE = 3005
ACCOUNT_ACCOUNT_USAGE = 3006

OTP_ZONE_ID_MANAGEMENT = 2

local inspect = require('inspect')

-- From https://stackoverflow.com/a/22831842
function string.starts(String,Start)
    return string.sub(String,1,string.len(Start))==Start
end

-- From https://stackoverflow.com/a/2421746
function string.upperFirst(str)
    return (string.gsub(str, "^%l", string.upper))
end

function readAccountBridge()
    local json = require("json")
    local io = require("io")

    -- TODO: Custom path.
    f, err = io.open("../otp/databases/accounts.json", "r")
    if err then
        print("PiratesClient: Returning empty table for account bridge")
        return {}
    end

    decoder = json.new_decoder(f)
    result, err = decoder:decode()
    f:close()
    assert(not err, err)
    print("PiratesClient: Account bridge successfully loaded.")
    return result
end

ACCOUNT_BRIDGE = readAccountBridge()

function saveAccountBridge()
    local json = require("json")
    local io = require("io")

    -- TODO: Custom path.
    f, err = io.open("../otp/databases/accounts.json", "w")
    assert(not err, err)
    encoder = json.new_encoder(f)
    err = encoder:encode(ACCOUNT_BRIDGE)
    assert(not err, err)
end

WHITELIST = {}
function readWhitelist()
    local io = require("io")
    local f, err = io.open("../../resources/phase_3/etc/pwhitelist.txt")
    assert(not err, err)
    for line in f:lines() do
        WHITELIST[line] = true
    end
end
readWhitelist()
print("PiratesClient: Successfully loaded whitelist.")

-- Converts a hexadecimal string to a string of bytes
-- From: https://smherwig.blogspot.com/2013/05/a-simple-binascii-module-in-ruby-and-lua.html
function unhexlify(s)
    if #s % 2 ~= 0 then
        error('unhexlify: hexstring must contain even number of digits')
    end
    local a = {}
    for i=1,#s,2 do
        local hs = string.sub(s, i, i+1)
        local code = tonumber(hs, 16)
        if not code then
            error(string.format("unhexlify: '%s' is not avalid hex number", hs))
        end
        table.insert(a, string.char(code))
    end
    return table.concat(a)
end

-- Load the configuration varables (see config.example.lua)
dofile("config.lua")

function handleDatagram(client, msgType, dgi)
    -- Internal datagrams
    if msgType == TOONTOWNCLIENT_CHECK_ONLINE_FRIENDS then
        handleCheckFriendOnline(client, dgi)
    elseif msgType == TOONTOWNCLIENT_FRIEND_ONLINE then
        handleFriendOnline(client, dgi)
    elseif msgType == TOONTOWNCLIENT_FRIEND_OFFLINE then
        handleFriendOffline(client, dgi)
    elseif msgType == TOONTOWNCLIENT_MAKE_FRIEND then
        handleMakeFriend(client, dgi)
    elseif msgType == TOONTOWNCLIENT_REMOVE_FRIEND then
        handleClientRemoveFriend(client, dgi:readUint32())
    else
        client:warn(string.format("Received unknown server msgtype %d", msgType))
    end
end

function receiveDatagram(client, dgi)
    -- Client received datagrams
    msgType = dgi:readUint16()

    -- print(string.format("PiratesClient: Received client msgtype %d", msgType))

    if msgType == CLIENT_HEARTBEAT then
        client:handleHeartbeat()
    elseif msgType == CLIENT_DISCONNECT then
        client:handleDisconnect()
    elseif msgType == CLIENT_OBJECT_UPDATE_FIELD then
        client:handleUpdateField(dgi)
    elseif msgType == CLIENT_LOGIN_3 then
        handleLogin(client, dgi)
    -- We have reached the only message types unauthenticated clients can use.
    elseif not client:authenticated() then
        client:sendDisconnect(CLIENT_DISCONNECT_GENERIC, string.format("%d message type is not allowed while unauthenticated.", msgType), true)
    elseif msgType == CLIENT_ADD_INTEREST then
        handleAddInterest(client, dgi)
    elseif msgType == CLIENT_REMOVE_INTEREST then
        client:handleRemoveInterest(dgi)
    elseif msgType == CLIENT_GET_AVATARS then
        handleGetAvatars(client)
    elseif msgType == CLIENT_CREATE_AVATAR then
        handleCreateAvatar(client, dgi)
    elseif msgType == CLIENT_SET_WISHNAME then
        handleSetWishname(client, dgi)
    elseif msgType == CLIENT_SET_AVATAR then
        beginSetAvatar(client, dgi)
    elseif msgType == CLIENT_GET_FRIEND_LIST then
        handleGetFriendList(client)
    elseif msgType == CLIENT_REMOVE_FRIEND then
        handleClientRemoveFriend(client, dgi:readUint32())
    elseif msgType == CLIENT_OBJECT_LOCATION then
        client:setLocation(dgi)
    elseif msgType == CLIENT_GET_AVATAR_DETAILS then
        handleGetAvatarDetails(client, dgi, "DistributedPlayerPirate", CLIENT_GET_AVATAR_DETAILS_RESP)
    elseif msgType == CLIENT_GET_PET_DETAILS then
        handleGetAvatarDetails(client, dgi, "DistributedPet", CLIENT_GET_PET_DETAILS_RESP)
    else
        client:sendDisconnect(CLIENT_DISCONNECT_GENERIC, string.format("Unknown message type: %d", msgType), true)
    end

    if dgi:getRemainingSize() ~= 0 then
        client:sendDisconnect(CLIENT_DISCONNECT_OVERSIZED_DATAGRAM, string.format("Datagram contains excess data.\n%s", dgi:dumpHex()), true)
    end
end

function handleLogin(client, dgi)
    local playToken = dgi:readString()
    local version = dgi:readString()
    local hash = dgi:readUint32()
    local tokenType = dgi:readInt32()
    local validateDownload = dgi:readString()
    local wantMagicWords = dgi:readString()

    if client:authenticated() then
        client:sendDisconnect(CLIENT_DISCONNECT_RELOGIN, "Authenticated client tried to login twice!", true)
        return
    end

    -- Check if version and hash matches
    if version ~= SERVER_VERSION then
        client:sendDisconnect(CLIENT_DISCONNECT_BAD_VERSION, string.format("Client version mismatch: client=%s, server=%s", version, SERVER_VERSION), true)
        return
    end
    if hash ~= DC_HASH then
        client:sendDisconnect(CLIENT_DISCONNECT_BAD_VERSION, string.format("Client DC hash mismatch: client=%d, server=%d", hash, DC_HASH), true)
        return
    end

    local openChat
    local isPaid
    local dislId
    local linkedToParent
    if PRODUCTION_ENABLED then
        local json = require("json")
        local crypto = require("crypto")
        local ok, err = pcall(function()
            local decodedToken, err = crypto.base64_decode(playToken)
            if err then
                error(err)
                return
            end
            local encrypted, err = json.decode(decodedToken)
            if err then
                error(err)
                return
            end
            local encryptedData, err = crypto.base64_decode(encrypted.data)
            if err then
                error(err)
                return
            end
            local iv, err = crypto.base64_decode(encrypted.iv)
            if err then
                error(err)
                return
            end

            local data, err = crypto.decrypt(encryptedData, 'aes-cbc', unhexlify(PLAY_TOKEN_KEY), crypto.RAW_DATA, iv)
            if err then
                error(err)
                return
            end
            local jsonData, err = json.decode(data)
            if err then
                error(err)
                return
            end

            -- Retrieve data from the API response.
            playToken = jsonData.playToken
            if tonumber(jsonData.OpenChat) == 1 then
                openChat = true
            else
                openChat = false
            end

            if tonumber(jsonData.Member) == 1 then
                isPaid = true
            else
                isPaid = false
            end

            local timestamp = jsonData.Timestamp
            dislId = tonumber(jsonData.dislId)
            local accountType = jsonData.accountType
            linkedToParent = jsonData.LinkedToParent

            if WANT_TOKEN_EXPIRATIONS and timestamp < os.time() then
                client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "Token has expired.", true)
                return
            end
        end)

        if not ok then
            -- Bad play token
            client:warn(string.format("PiratesClient: Error when decrypting play token: %s", err))
            client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "Invalid play token", true)
            return
        end
        -- TODO: Send discord webhook.
    else
        -- Production is not enabled
        -- We need these dummy values
        openChat = false
        if WANT_MEMBERSHIP then
            isPaid = true
        else
            isPaid = false
        end
        dislId = 1
        linkedToParent = false
    end

    local accountId = ACCOUNT_BRIDGE[playToken]
    if accountId ~= nil then
        -- Query the account object
        function queryLoginResponse(doId, success, fields)
            if not success then
                client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "The Account object was unable to be queried.", true)
                return
            end

            client:setDatabaseValues(accountId, "Account", {
                LAST_LOGIN = os.date("%a %b %d %H:%M:%S %Y"),
            })

            loginAccount(client, fields, accountId, playToken, openChat, isPaid, dislId, linkedToParent)
        end
        client:getDatabaseValues(accountId, "Account", {"ACCOUNT_AV_SET"}, queryLoginResponse)
    else
        -- Create a new account object
        local account = {
            -- The rest of the values are defined in the dc file.
            CREATED = os.date("%a %b %d %H:%M:%S %Y"),
            LAST_LOGIN = os.date("%a %b %d %H:%M:%S %Y"),
        }

        function createAccountResponse(accountId)
            if accountId == 0 then
                client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "The Account object was unable to be created.", false)
                return
            end

            -- Store the account into the bridge
            ACCOUNT_BRIDGE[playToken] = accountId
            saveAccountBridge()

            account.ACCOUNT_AV_SET = {0, 0, 0, 0}

            client:writeServerEvent("account-created", "PiratesClient", string.format("%d", accountId))

            loginAccount(client, account, accountId, playToken, openChat, isPaid, dislId, linkedToParent)
        end
        client:createDatabaseObject("Account", account, DATABASE_OBJECT_TYPE_ACCOUNT, createAccountResponse)
    end
end

function loginAccount(client, account, accountId, playToken, openChat, isPaid, dislId, linkedToParent)
    local ejectDg = datagram:new()
    client:addServerHeaderWithAccountId(ejectDg, accountId, CLIENTAGENT_EJECT)
    ejectDg:addUint16(100)
    ejectDg:addString("You have been disconnected because someone else just logged in using your account on another computer.")
    client:routeDatagram(ejectDg)

    -- Subscribe to our puppet channel.
    client:subscribePuppetChannel(accountId, 3)

    -- Set our channel containing our account id
    client:setChannel(accountId, 0)

    client:authenticated(true)

    -- Store the account id and avatar list into our client's user table:
    local userTable = client:userTable()
    userTable.accountId = accountId
    userTable.avatars = account.ACCOUNT_AV_SET
    userTable.playToken = playToken
    userTable.isPaid = isPaid
    userTable.openChat = openChat
    client:userTable(userTable)

    -- Log the event
    client:writeServerEvent("account-login", "PiratesClient", string.format("%d", accountId))

    -- Prepare the login response.
    local resp = datagram:new()
    resp:addUint16(CLIENT_LOGIN_3_RESP)

    resp:addInt8(0) -- Return code
    resp:addString("All Ok") -- Error string

    resp:addString('YES') -- openChatEnabled
    resp:addString('YES') -- createFriendsWithChat
    resp:addString('YES') -- chatCodeCreationRule

    if isPaid then
        resp:addString("FULL") -- access
    else
        resp:addString("VELVET") -- access
    end

    resp:addInt32(0) -- familyAccountId
    resp:addInt32(accountId) -- playerAccountId

    resp:addString(playToken) -- playerName
    resp:addInt8(1) -- playerNameApproved

    -- maxAvatars
    local avCount

    if isPaid then
        avCount = 4
    else
        avCount = 2
    end

    resp:addInt32(avCount) -- maxAvatars

    resp:addUint16(1) -- numSubs

    -- TODO: We need atleast one sub account otherwise the client crashes ??
    resp:addUint32(1) -- subId
    resp:addUint32(accountId) -- subOwnerId

    resp:addString(playToken) -- subName
    resp:addString("YES") -- subActive

    -- subAccess
    if isPaid then
        resp:addString("FULL")
    else
        resp:addString("VELVET")
    end

    resp:addUint8(1) -- subLevel

    -- subNumAvatars
    if isPaid then
        subNumAvatars = 4
    else
        subNumAvatars = 2
    end

    resp:addUint8(subNumAvatars)

    resp:addUint8(1) -- subNumConcur

    resp:addString("YES") -- subFounder

    if openChat then
        resp:addString("YES") -- WLChatEnabled
    else
        resp:addString("NO") -- WLChatEnabled
    end

    -- Dispatch the response to the client.
    client:sendDatagram(resp)
end

function handleAddInterest(client, dgi)
    local handle = dgi:readUint16()
    local context = dgi:readUint32()
    local parent = dgi:readUint32()
    local zones = {}
    while dgi:getRemainingSize() > 0 do
        table.insert(zones, dgi:readUint32())
    end

    client:handleAddInterest(handle, context, parent, zones)
end

function handleGetAvatars(client)
    local userTable = client:userTable()
    local avatarList = userTable.avatars

    local retreivedFields = {}
    local expectingAvatars = {}
    local gotAvatars = {}

    function gotAllAvatars()
        dg = datagram:new()
        dg:addUint16(CLIENT_GET_AVATARS_RESP)
        dg:addUint8(0) -- returnCode
        dg:addUint16(#gotAvatars) -- avatarTotal
        for index, fields in pairs(retreivedFields) do
            dg:addUint32(fields.avatarId)
            dg:addString(fields.setName[1]) -- name
            -- TODO: WishName (Typed name moderation)
            dg:addString("") -- wantName
            dg:addString("") -- approvedName
            dg:addString("") -- rejectedName

            dg:addString(fields.setDNAString[1]) -- avDNA
            dg:addUint8(index - 1) -- avPosition
            dg:addUint8(0) -- aName
        end
        client:sendDatagram(dg)
    end

    for index, avatarId in ipairs(avatarList) do
        if avatarId ~= 0 then
            -- Query the avatar object
            function queryAvatarResponse(doId, success, fields)
                if not success then
                    client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, string.format("The DistributedPlayerPirate object %d was unable to be queried.", avatarId))
                    return
                end

                fields.avatarId = avatarId
                retreivedFields[index] = fields
                table.insert(gotAvatars, {})
                if #gotAvatars == #expectingAvatars then
                    gotAllAvatars()
                end
            end

            -- Query the avatar object
            client:getDatabaseValues(avatarId, "DistributedPlayerPirate", {"setName", "setDNAString"}, queryAvatarResponse)
            table.insert(expectingAvatars, avatarId)
        end
        if index == 6 and #expectingAvatars == 0 then
            -- We got nothing to do.
            gotAllAvatars()
        end
    end
end

function handleCreateAvatar(client, dgi)
    local userTable = client:userTable()
    local accountId = userTable.accountId

    local contextId = dgi:readUint16()
    local dnaString = dgi:readString()
    local avPosition = dgi:readUint8()

    -- TODO: Validate DNA string
    -- TODO: Check if a avatar already exists in the slot.

    if avPosition > 6 then
        -- Client sent an invalid av position
        client:sendDisconnect(CLIENT_DISCONNECT_GENERIC, "Invalid Avatar index chosen.", true)
        return
    end

    -- Create a new DistributedPlayerPirate object
    local avatar = {
        -- The rest of the values are defined in the dc file.
        setDISLid = {accountId},
        setDNAString = {dnaString},
        setPosIndex = {avPosition},
        setAccountName = {userTable.playToken},
    }

    function createAvatarResponse(avatarId)
        if avatarId == 0 then
            client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "The DistributedPlayerPirate object was unable to be created.", false)
            return
        end

        userTable.avatars[avPosition + 1] = avatarId

        client:setDatabaseValues(accountId, "Account", {
            ACCOUNT_AV_SET = userTable.avatars,
        })

        client:writeServerEvent("avatar-created", "PiratesClient", string.format("%d|%d", accountId, avatarId))

        -- Prepare the create avatar response.
        local resp = datagram:new()
        resp:addUint16(CLIENT_CREATE_AVATAR_RESP)
        resp:addUint16(contextId)
        resp:addUint8(0) -- returnCode
        resp:addUint32(avatarId)

        -- Dispatch the response to the client.
        client:sendDatagram(resp)
    end

    client:createDatabaseObject("DistributedPlayerPirate", avatar, DATABASE_OBJECT_TYPE_TOON, createAvatarResponse)
end

function handleSetWishname(client, dgi)
    local avId = dgi:readUint32()
    local name = dgi:readString()

    if avId ~= 0 then
        -- Client wants to set the name and we're just gonna allow them to.
        client:setDatabaseValues(avId, "DistributedPlayerPirate", {
            setName = {name},
        })
    end

    -- Prepare the wish name response.
    local resp = datagram:new()
    resp:addUint16(CLIENT_SET_WISHNAME_RESP)
    resp:addUint32(avId)
    resp:addUint16(0) -- returnCode
    resp:addString("") -- pendingName
    resp:addString(name) -- approvedName
    resp:addString("") -- rejectedName

     -- Dispatch the response to the client.
    client:sendDatagram(resp)
end

function beginSetAvatar(client, dgi)
    -- Workaround to make sure our avatar list is in sync after creating a pirate
    -- We need this because avatar creation is not handled in Lua
    local userTable = client:userTable()

    local avatarId = dgi:readUint32()

    if avatarId == 0 then
        -- No need to query the database
        -- We are logging out
        clearAvatar(client)
        return
    end

    -- Query the account object
    function queryLoginResponse(doId, success, fields)
        if not success then
            client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "The Account object was unable to be queried.", true)
            return
        end

        userTable.avatars = fields.ACCOUNT_AV_SET
        client:userTable(userTable)

        handleSetAvatar(client, avatarId)
    end

    client:getDatabaseValues(userTable.accountId, "Account", {"ACCOUNT_AV_SET"}, queryLoginResponse)
end

function handleSetAvatar(client, avatarId)
    local userTable = client:userTable()
    local accountId = userTable.accountId

    local isAvInList = false
    for _, avId in ipairs(userTable.avatars) do
        if avatarId == avId then
            isAvInList = true
            break
        end
    end
    if not isAvInList then
        client:sendDisconnect(CLIENT_DISCONNECT_GENERIC, string.format("Avatar %d not in list.", avatarId), true)
        return
    end

    userTable.avatarId = avatarId
    client:userTable(userTable)

    client:setChannel(accountId, avatarId)

    local setAccess = 1
    if userTable.isPaid then
        setAccess = 2
    end

    client:sendActivateObject(avatarId, "DistributedPlayerPirate", {
        setAccess = {setAccess},
    })

    client:objectSetOwner(avatarId, true)

    -- Let the UberDog know about our avatar usage.
    sendUsage(client, userTable.playToken, userTable.openChat, 0, avatarId, accountId, userTable.isPaid, false)

    -- Broadcast to our friends that we are coming online
    dg = datagram:new()
    client:addServerHeader(dg, BCHAN_CLIENTS, TOONTOWNCLIENT_FRIEND_ONLINE)
    dg:addUint32(avatarId)
    dg:addBool(userTable.openChat)
    client:routeDatagram(dg)

    -- Set post removes in case we go offline.
    dg = datagram:new()
    client:addServerHeader(dg, BCHAN_CLIENTS, TOONTOWNCLIENT_FRIEND_OFFLINE)
    dg:addUint32(avatarId)
    client:addPostRemove(dg)

    -- Let the UberDog know about our avatar usage (going offline).
    sendUsage(client, userTable.playToken, userTable.openChat, avatarId, 0, accountId, userTable.isPaid, true)
end

function sendUsage(client, playToken, openChat, priorAvatar, newAvatar, accountId, isPaid, postRemove)
    -- Log avatar usage
    local playerName = playToken
    local playerNameApproved = 1
    local openChatEnabled = "NO"

    if openChat then
        openChatEnabled = "YES"
    end

    local createFriendsWithChat = "YES"
    local chatCodeCreation = "YES"

    local avatarId

    if priorAvatar ~= 0 then
        avatarId = priorAvatar
    else
        avatarId = newAvatar
    end

    local dg = datagram:new()
    dg:addServerHeader(CHANNEL_PUPPET_ACTION, avatarId, ACCOUNT_AVATAR_USAGE)

    dg:addUint32(priorAvatar) -- priorAvatar
    dg:addUint32(newAvatar) -- newAvatar
    dg:addUint16(0) -- newAvatarType
    dg:addUint32(accountId) -- accountId
    dg:addString(openChatEnabled) -- openChatEnabled
    dg:addString(createFriendsWithChat) -- createFriendsWithChat
    dg:addString(chatCodeCreation) -- chatCodeCreation

    if isPaid then
        dg:addString("FULL") -- piratesAccess
    else
        dg:addString("VELVET") -- piratesAccess
    end

    dg:addInt32(0) -- familyAccountId
    dg:addInt32(accountId) -- playerAccountId

    dg:addString(playerName) -- playerName
    dg:addInt8(playerNameApproved) -- playerNameApproved

    -- maxAvatars
    local maxAvatars

    if isPaid then
        maxAvatars = 4
    else
        maxAvatars = 2
    end

    dg:addInt32(maxAvatars) -- maxAvatars

    dg:addInt16(0) -- numFamilyMembers

    if postRemove then
        client:addPostRemove(dg)
    else
        client:routeDatagram(dg)
    end
end

function clearAvatar(client)
    local userTable = client:userTable()

    if userTable.avatarId == nil then
        return
    end

    client:removeSessionObject(userTable.avatarId)
    client:unsubscribePuppetChannel(userTable.avatarId, 1)

    dg = datagram:new()
    client:addServerHeader(dg, userTable.avatarId, STATESERVER_OBJECT_DELETE_RAM)
    dg:addUint32(userTable.avatarId)
    client:routeDatagram(dg)

    -- Tell our friends that we are going offline
    -- and clear the post remove set for it.
    dg = datagram:new()
    client:addServerHeader(dg, BCHAN_CLIENTS, TOONTOWNCLIENT_FRIEND_OFFLINE)
    dg:addUint32(userTable.avatarId)
    client:routeDatagram(dg)

    client:clearPostRemoves()

    -- Undeclare all friends.
    client:undeclareAllObjects()

    userTable.avatarId = nil
    userTable.friendsList = nil
    client:userTable(userTable)

    client:setChannel(userTable.accountId, 0)

end

function handleAddOwnership(client, doId, parent, zone, dc, dgi)
    local userTable = client:userTable()
    local accountId = userTable.accountId
    local avatarId = userTable.avatarId
    if doId ~= avatarId then
        -- We own other objects other than the avatar like the teleport actor
        local dg = datagram:new()
        dg:addUint16(CLIENT_CREATE_OBJECT_REQUIRED_OTHER_OWNER)
        dg:addUint16(dc)
        dg:addUint32(doId)
        dg:addUint32(parent)
        dg:addUint32(zone)
        dg:addData(dgi:readRemainder())
        client:sendDatagram(dg)
        return
    end

    client:addSessionObject(doId)
    client:subscribePuppetChannel(avatarId, 1)

    -- Store name for SpeedChat+
    local name = dgi:readString()
    userTable.avatarName = name
    client:userTable(userTable)

    local remainder = dgi:readRemainder()

    client:writeServerEvent("selected-avatar", "PiratesClient", string.format("%d|%d", accountId, avatarId))

    local resp = datagram:new()
    resp:addUint16(CLIENT_GET_AVATAR_DETAILS_RESP)
    resp:addUint32(doId) -- avatarId
    resp:addUint8(0) -- returnCode
    resp:addString(name) -- setName
    resp:addData(remainder)
    client:sendDatagram(resp)

    -- Update common chat flags:
    local dg = datagram:new()
    dg:addServerHeader(avatarId, avatarId, STATESERVER_OBJECT_UPDATE_FIELD)
    dg:addUint32(avatarId)
    client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setCommonChatFlags", {0}, true)
    client:routeDatagram(dg)

    -- update whitelist chat flags:
    local dg = datagram:new()
    dg:addServerHeader(avatarId, avatarId, STATESERVER_OBJECT_UPDATE_FIELD)
    dg:addUint32(avatarId)
    if userTable.openChat then
        client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setWhitelistChatFlags", {1}, true)
    else
        client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setWhitelistChatFlags", {0}, true)
    end
    client:routeDatagram(dg)

    -- Inventory begin
    -- Query the player object
    function queryAvatarResponse(doId, success, fields)
        if not success then
            client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, "The DistributedPlayerPirate object was unable to be queried.", true)
            return
        end

        -- Setup inventory:
        local dg = datagram:new()
        dg:addServerHeader(OTP_DO_ID_PIRATES_INVENTORY_MANAGER_BASE, avatarId, STATESERVER_OBJECT_UPDATE_FIELD)
        dg:addUint32(OTP_DO_ID_PIRATES_INVENTORY_MANAGER_BASE)
        client:packFieldToDatagram(dg, "DistributedInventoryManager", "avatarOnline", {fields.setInventoryId[1], 0}, true)
        client:routeDatagram(dg)
    end

    client:getDatabaseValues(avatarId, "DistributedPlayerPirate", {"setInventoryId"}, queryAvatarResponse)
end

function handleGetFriendList(client)
    local userTable = client:userTable()

    function queryFriendsListResponse(doId, success, fields)
        if not success then
            client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, string.format("An error has occurred when attempting to fetch your friends list. (doId: %d)", avatarId))
            return
        end

        cleanupFriendsList(client, fields.setFriendsList[1])
        userTable.friendsList = fields.setFriendsList[1]
        client:userTable(userTable)

        if #userTable.friendsList == 0 then
            -- Nothing to do.
            finishGetFriendsList(client, {})
            return
        end

        local avatarFields = {}
        local gotNumAvatars = 0
        function queryAvatarResponse(doId, success, fields)
            if not success then
                client:sendDisconnect(CLIENT_DISCONNECT_ACCOUNT_ERROR, string.format("An error has occurred when attempting to fetch your friends list. (doId: %d)", doId))
                return
            end
            avatarFields[doId] = fields
            gotNumAvatars = gotNumAvatars + 1
            if gotNumAvatars == #userTable.friendsList then
                finishGetFriendsList(client, avatarFields)
            end
        end

        for _, v in ipairs(userTable.friendsList) do
            client:getDatabaseValues(v[1], "DistributedPlayerPirate", {"setName", "setDNAString", "setPetId"}, queryAvatarResponse)
        end
    end
    client:queryObjectFields(userTable.avatarId, "DistributedPlayerPirate", {"setFriendsList"}, queryFriendsListResponse)
end

function cleanupFriendsList(client, friendsList)
    local userTable = client:userTable()

    -- This cleans up the friends list by removing any
    -- non-db ids (this is caused by logging off/crashing while in the
    -- middle of using a teleport bot).
    local modifiedFriendsList = false
    for i = #friendsList, 1, -1 do
        if friendsList[i][1] > 199999999 then
            table.remove(friendsList, i)
            modifiedFriendsList = true
        end
    end

    if modifiedFriendsList then
        -- Update the object itself
        local dg = datagram:new()
        dg:addServerHeader(userTable.avatarId, 0, STATESERVER_OBJECT_UPDATE_FIELD)
        dg:addUint32(userTable.avatarId)
        client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setFriendsList", {friendsList}, true)
        client:routeDatagram(dg)
    end
end

function finishGetFriendsList(client, avatarFields)
    local userTable = client:userTable()

    local resp = datagram:new()
    resp:addUint16(CLIENT_GET_FRIEND_LIST_RESP)
    resp:addUint8(0) -- errorCode
    resp:addUint16(#userTable.friendsList) -- count
    for _, v in ipairs(userTable.friendsList) do

        -- Declare the object's existance so they can teleport.
        client:declareObject(v[1], "DistributedPlayerPirate")

        -- Check if this avatar is online
        local dg = datagram:new()
        client:addServerHeaderWithAvatarId(dg, v[1], TOONTOWNCLIENT_CHECK_ONLINE_FRIENDS)
        dg:addUint32(userTable.avatarId)
        client:routeDatagram(dg)

        local fields = avatarFields[v[1]]
        resp:addUint32(v[1]) -- doId
        resp:addString(fields.setName[1]) -- name
        resp:addString(fields.setDNAString[1]) -- dna
        resp:addUint32(fields.setPetId[1]) -- petId
    end
    client:sendDatagram(resp)
end

function handleFriendOnline(client, dgi)
    local userTable = client:userTable()
    local friendId = dgi:readUint32()
    local openChat = dgi:readBool()

    if userTable.avatarId == nil then
        return
    end

    if friendId == userTable.avatarId then
        return
    end

    for i, v in ipairs(userTable.friendsList) do
        if v[1] == friendId then
            -- Send a CLIENT_FRIEND_ONLINE message.
            local dg = datagram:new()
            dg:addUint16(CLIENT_FRIEND_ONLINE)
            dg:addUint32(friendId) -- doId
            dg:addUint8(v[2]) -- commonChatFlags
            if openChat then
                dg:addBool(openChat) -- whitelistChatFlags
            end

            client:sendDatagram(dg)
            break
        end
    end
end

function handleFriendOffline(client, dgi)
    local userTable = client:userTable()
    local friendId = dgi:readUint32()

    if userTable.avatarId == nil then
        return
    end

    if friendId == userTable.avatarId then
        return
    end

    for i, v in ipairs(userTable.friendsList) do
        if v[1] == friendId then
            -- Send a CLIENT_FRIEND_OFFLINE message.
            local dg = datagram:new()
            dg:addUint16(CLIENT_FRIEND_OFFLINE)
            dg:addUint32(friendId) -- doId
            client:sendDatagram(dg)
            break
        end
    end
end

function handleCheckFriendOnline(client, dgi)
    local userTable = client:userTable()
    local friendId = dgi:readUint32()

    if userTable.avatarId == nil then
        -- Not logged in yet.
        return
    end

    if userTable.friendsList == nil then
        client:warn("handleCheckFriendOnline: TODO: No friends list yet.")
        -- Have not gotten our friends list yet.
        -- TODO: Put the friendId in a queue table and
        -- wait until we have gotten the friends list
        -- before doing the check.
        return
    end

    for i, v in ipairs(userTable.friendsList) do
        if v[1] == friendId then
            local dg = datagram:new()
            client:addServerHeaderWithAvatarId(dg, friendId, TOONTOWNCLIENT_FRIEND_ONLINE)
            dg:addUint32(userTable.avatarId)
            dg:addBool(userTable.openChat)
            client:routeDatagram(dg)
            break
        end
    end
end

function handleMakeFriend(client, dgi)
    local userTable = client:userTable()
    local friendId = dgi:readUint32()
    local flag = dgi:readUint8()

    if userTable.friendsList == nil then
        client:warn("handleMakeFriend: No friends list")
        return
    end

    local addNewFriend = true
    for i, v in ipairs(userTable.friendsList) do
        if v[1] == friendId then
            userTable.friendsList[i][2] = flag
            addNewFriend = false
            break
        end
    end

    if addNewFriend then
        table.insert(userTable.friendsList, {friendId, flag})
    end

    client:userTable(userTable)

    -- Declare the object's existance so they can teleport.
    client:declareObject(friendId, "DistributedPlayerPirate")

    client:writeServerEvent("made-friends", "PiratesClient", string.format("%d|%d", userTable.avatarId, friendId))

    -- Check if the friend is online (in case someone redeems a
    -- code while they're offline)
    local dg = datagram:new()
    client:addServerHeaderWithAvatarId(dg, friendId, TOONTOWNCLIENT_CHECK_ONLINE_FRIENDS)
    dg:addUint32(userTable.avatarId)
    client:routeDatagram(dg)
end

function handleClientRemoveFriend(client, friendId)
    local userTable = client:userTable()
    if userTable.friendsList == nil then
        client:warn("handleClientRemoveFriend: No friends list")
        return
    end

    for i, v in ipairs(userTable.friendsList) do
        if v[1] == friendId then
            -- Remove from our friends list.
            userTable.friendsList[i] = nil

            -- Undeclare the object
            client:undeclareObject(friendId)

            -- Update the object itself
            local dg = datagram:new()
            dg:addServerHeader(userTable.avatarId, 0, STATESERVER_OBJECT_UPDATE_FIELD)
            dg:addUint32(userTable.avatarId)
            client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setFriendsList", {userTable.friendsList}, true)
            client:routeDatagram(dg)

            client:writeServerEvent("removed-friend", "PiratesClient", string.format("%d|%d", userTable.avatarId, friendId))

            -- Update the other friend's list as well.
            function queryFriendsListResponse(doId, success, fields)
                if not success then
                    client:error(string.format("PiratesClient: handleClientRemoveFriend: Unable to get friends list for friendId: %d", friendId))
                    return
                end
                if fields.setFriendsList ~= nil then
                    for i, v in ipairs(fields.setFriendsList[1]) do
                        if v[1] == userTable.avatarId then
                            fields.setFriendsList[1][i] = nil

                            -- Update the object itself
                            local dg = datagram:new()
                            dg:addServerHeader(friendId, 0, STATESERVER_OBJECT_UPDATE_FIELD)
                            dg:addUint32(friendId)
                            client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setFriendsList", fields.setFriendsList, true)
                            client:routeDatagram(dg)
                            return
                        end
                    end
                end
            end
            if friendId < 199999999 then
                client:getDatabaseValues(friendId, "DistributedPlayerPirate", {"setFriendsList"}, queryFriendsListResponse)
            else
                client:queryObjectFields(friendId, "DistributedPlayerPirate", {"setFriendsList"}, queryFriendsListResponse)
            end
            return
        end
    end
    client:error(string.format("handleClientRemoveFriend: friend %d not found!", friendId))
end

function handleGetAvatarDetails(client, dgi, className, responseMsg)
    local doId = dgi:readUint32()

    function queryAvatarResponse(doId, success, fields)
        resp = datagram:new()
        resp:addUint16(responseMsg)
        resp:addUint32(doId)

        if not success then
            client:error(string.format("PiratesClient: queryAvatarResponse: Unable to get avatar: %d", doId))
            resp:addUint8(1) -- error
            client:sendDatagram(resp)
            return
        end

        resp:addUint8(0) -- error
        for name, value in pairs(fields) do
            client:packFieldToDatagram(resp, className, name, value, false)
        end
        client:sendDatagram(resp)
    end
    if doId > 199999999 then
        client:queryAllRequiredFields(doId, className, queryAvatarResponse)
    else
        client:getAllRequiredFromDatabase(doId, className, queryAvatarResponse)
    end
end

function filterWhitelist(message)
    local modifications = {}
    local wordsToSub = {}
    local offset = 0
    -- Match any character except spaces.
    for word in string.gmatch(message, "[^%s]*") do
        -- Strip out punctuations just for checking with the whitelist.
        local strippedWord = string.gsub(word, "[.,?!]", "")
        if word ~= "" and WHITELIST[string.lower(strippedWord)] ~= true then
            table.insert(modifications, {offset, offset + string.len(word) - 1})
            table.insert(wordsToSub, word)
        end
        if word ~= "" then
            offset = offset + string.len(word) + 1
        end
    end
    local cleanMessage = message

    for _, word in ipairs(wordsToSub) do
        cleanMessage = string.gsub(cleanMessage, word, string.rep('*', string.len(word)), 1)
    end

    return cleanMessage, modifications
end

-- setTalk from client
function handleClientDistributedPlayerPirate_setTalk(client, doId, fieldId, data)
    -- The data is safe to use, as the ranges has already been
    -- verified by the server prior to calling this function.

    local userTable = client:userTable()
    local accountId = userTable.accountId
    local avatarId = userTable.avatarId
    local avatarName = userTable.avatarName

    -- All other data are blank values, except for chat.
    local message = data[4] --chat

    local dg = datagram:new()
    -- We set the sender field to the doId instead of our channel to make sure
    -- we can receive the broadcast.
    dg:addServerHeader(doId, doId, STATESERVER_OBJECT_UPDATE_FIELD)
    dg:addUint32(doId)
    client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setTalk", {avatarId, accountId, avatarName, message, {}, 0}, true)
    client:routeDatagram(dg)
end

-- setTalk from server
function handleDistributedPlayerPirate_setTalk(client, doId, fieldId, data)
    -- The data is safe to use, as the ranges has already been
    -- verified by the server prior to calling this function.

    local userTable = client:userTable()

    local avatarId = data[1]
    local accountId = data[2]
    local avatarName = data[3]
    local message = data[4]
    -- The rest are intentionally left blank.
    local modifications = {}

    local shouldFilterMessage = true
    if userTable.friendsList ~= nil then
        if avatarId == userTable.avatarId then
            -- That's us.  Don't filter from whitelist
            -- if one of our friends is a true friend
            for i, v in ipairs(userTable.friendsList) do
                if userTable.friendsList[i][2] == 1 then
                    shouldFilterMessage = false
                    break
                end
            end
        else
            -- That's a different person.  Check if that person
            -- is a true friend:
            for i, v in ipairs(userTable.friendsList) do
                if userTable.friendsList[i][1] == avatarId and userTable.friendsList[i][2] == 1 then
                    shouldFilterMessage = false
                    break
                end
            end
        end
    end

    if shouldFilterMessage then
        message, modifications = filterWhitelist(message)
    end

    local dg = datagram:new()
    dg:addUint16(CLIENT_OBJECT_UPDATE_FIELD)
    dg:addUint32(doId)
    client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setTalk", {avatarId, accountId, avatarName, message, modifications, 0}, true)
    client:sendDatagram(dg)
end

function handleClientDistributedPlayerPirate_setTalkWhisper(client, doId, fieldId, data)
    -- The data is safe to use, as the ranges has already been
    -- verified by the server prior to calling this function.

    local userTable = client:userTable()
    local accountId = userTable.accountId
    local avatarId = userTable.avatarId
    local avatarName = userTable.avatarName

    -- All other data are blank values, except for chat.
    local message = data[4] --chat

    if message == "" then
        return
    end

    local cleanMessage, modifications = filterWhitelist(message)
    -- Check friends list if what we're sending this too is a true friend:
    if userTable.friendsList ~= nil then
        for i, v in ipairs(userTable.friendsList) do
            if v[1] == doId and v[2] == 1 then
                -- Send unfiltered message
                cleanMessage, modifications = message, {}
            end
        end
    end

    local dg = datagram:new()
    -- We set the sender field to the doId instead of our channel to make sure
    -- we can receive the broadcast.
    dg:addServerHeader(doId, doId, STATESERVER_OBJECT_UPDATE_FIELD)
    dg:addUint32(doId)
    client:packFieldToDatagram(dg, "DistributedPlayerPirate", "setTalkWhisper", {avatarId, accountId, avatarName, cleanMessage, modifications, 0}, true)
    client:routeDatagram(dg)
end
