# Server
dc-file config/dclass/pirates.dc
dc-file config/dclass/otp.dc

# Features
want-membership true
want-ships false
want-friends true
want-quests true
want-bands false
want-production false
want-enemies false
want-tutorial true

# Models:
model-path resources
model-path resources/phase_2
model-path resources/phase_3
model-path resources/phase_4
model-path resources/phase_5

# Worlds
world-file piratesWorld
