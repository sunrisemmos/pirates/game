window-title Pirates
icon-filename phase_3/etc/Pirates_Adds.ico

model-path resources
model-path resources/phase_2
model-path resources/phase_3
model-path resources/phase_4
model-path resources/phase_5
default-model-extension .bam

audio-library-name p3fmod_audio

dc-file config/dclass/pirates.dc
dc-file config/dclass/otp.dc

server-version pcsv1.0.34.31
want-ssl-scheme false
want-magic-words true
signature SunriseGames
allow-unclean-exit true
enable-pipe-selector true
skip-tutorial false
gm-nametag-enabled 1
allow-linked-accounts 1
want-land-infamy true
want-sea-infamy true
want-privateering true
console-output true
required-login DISLToken

# Discord:
want-discord-presence false
discord-client-id 592845025331642389
