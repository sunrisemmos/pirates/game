from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD

import traceback


class InjectionManagerUD(DistributedObjectGlobalUD):

    def inject_ud(self, code):
        sender = self.air.getMsgSender()

        if self.air.wantProduction:
            self.air.writeServerEvent('suspicious', sender, 'Tried to inject UD in production!')
            return

        try:
            exec(code, globals())
        except:
            traceback.print_exc()
