from direct.distributed.DistributedObjectGlobalAI import DistributedObjectGlobalAI

import traceback


class InjectionManagerAI(DistributedObjectGlobalAI):

    def inject_ai(self, code):
        avId = self.air.getAvatarIdFromSender()

        if self.air.wantProduction:
            self.air.writeServerEvent('suspicious', avId, 'Tried to inject AI in production!')
            return

        try:
            exec(code, globals())
        except:
            traceback.print_exc()
