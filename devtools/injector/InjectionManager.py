from direct.distributed.DistributedObject import DistributedObject


class InjectionManager(DistributedObject):
    neverDisable = 1

    def d_inject_ai(self, code):
        self.sendUpdate('inject_ai', [code])

    def d_inject_ud(self, code):
        self.sendUpdate('inject_ud', [code])
