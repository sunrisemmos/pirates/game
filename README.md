# Pirates of the Caribbean
This is the development repository for Pirates of the Caribbean, a project working to recreate the game as it was in 2013.

## Dependencies
* Panda3D [Windows (x86_64)] - https://download.alt.sunrise.games/binaries/Panda3D-1.11.0-py3.11-x64-Pirates.exe

### Getting Setup
This assumes you have the Panda3D already in your path.

* `git clone https://gitlab.com/sunrisemmos/Pirates/game`
* `cd game`
* `git clone https://gitlab.com/sunrisemmos/Pirates/resources resources`
* `python -m pip install -r requirements.txt`
