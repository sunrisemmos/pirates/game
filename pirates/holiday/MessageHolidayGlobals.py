# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.holiday.MessageHolidayGlobals
from pirates.ai.HolidayDates import *

class ConfigIds:
    CrewDays = 1

MessageHolidayConfigs = {ConfigIds.CrewDays: {'id': ConfigIds.CrewDays, 'name': 'CrewDays (Msg)', 'dates': HolidayDates(HolidayDates.TYPE_WEEKLY, [
                                (
                                 Day.FRIDAY, 15, 0, 0), (Day.FRIDAY, 20, 0, 0), (Day.SATURDAY, 15, 0, 0), (Day.SATURDAY, 20, 0, 0)])}}