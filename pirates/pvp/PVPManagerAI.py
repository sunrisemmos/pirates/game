from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class PVPManagerAI(DistributedObjectAI):
    notify = directNotify.newCategory('PVPManagerAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

    def requestChallenge(self, challengeeId):
        avatarId = self.air.getAvatarIdFromSender()
        self.d_challengeFrom(avatarId, challengeeId)

    def acceptChallenge(self, challengerId):
        avatarId = self.air.getAvatarIdFromSender()

    def d_challengeAccepted(self, avatarId, challengeeId):
        self.sendUpdateToAvatarId(avatarId, 'challengeAccepted', [challengeeId])

    def d_challengeFrom(self, avatarId, challengeeId):
        self.sendUpdateToAvatarId(avatarId, 'challengeFrom', [challengeeId])