# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.pvp.Beacon
from pandac.PandaModules import CardMaker, NodePath

def getBeaconModel():
    return loader.loadModel('models/textureCards/pvp_arrow').find('**/pvp_arrow')

__beacon = None

def initBeacon():
    global __beacon
    geom = getBeaconModel()
    geom.setBillboardAxis(1)
    geom.setZ(1)
    geom.setScale(2)
    __beacon = geom

initBeacon()

def getBeacon(parent):
    return __beacon.copyTo(parent)