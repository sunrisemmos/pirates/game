from pandac.PandaModules import *
from panda3d.direct import DCPacker

from direct.directnotify.DirectNotifyGlobal import *
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

from otp.ai.AIMsgTypes import DBSERVER_ID, DBSERVER_GET_STORED_VALUES, DBSERVER_CREATE_STORED_OBJECT, DBSERVER_SET_STORED_VALUES

from pirates.pirate import DistributedPlayerPirateAI

class DatabaseObject:
    """DatabaseObject

    This class stores an object as retrieved directly from the
    database via some special direct-to-database queries.  It is used
    for in-game operations as well as offline database repair utilities.
    """

    notify = directNotify.newCategory("DatabaseObject")
    notify.setDebug(1)

    def __init__(self, air, doId=None, doneEvent="DatabaseObject"):
        self.air = air
        self.doId = doId
        self.values = {}
        self.gotDataHandler = None
        self.doneEvent = doneEvent

    def readPirate(self, fields = None):
        # Reads and returns a DistributedPlayerPirateAI object.  Note that an
        # empty DistributedPlayerPirateAI will be returned by this call; the
        # values will be filled in on the pirate at some later point in
        # time, after the database has responded to the query.
        pirate = DistributedPlayerPirateAI.DistributedPlayerPirateAI(self.air)
        self.readObject(pirate, fields)
        return pirate

    def readObject(self, do, fields = None):
        # Reads a DistributedObject from the database and fills in its
        # data.  The data is not available immediately, but rather
        # when the self.doneEvent is thrown.
        self.do = do
        className = do.__class__.__name__
        self.dclass = self.air.dclassesByName[className]
        self.gotDataHandler = self.fillin

        # If fields is supplied, it is a subset of fields to read.
        if fields != None:
            self.getFields(fields)
        else:
            self.getFields(self.getDatabaseFields(self.dclass))

    def storeObject(self, do, fields = None):
        # Copies the data from the indicated DistributedObject and
        # writes it to the database.
        self.do = do
        className = do.__class__.__name__
        self.dclass = self.air.dclassesByName[className]

        if fields != None:
            self.reload(self.do, self.dclass, fields)
        else:
            dbFields = self.getDatabaseFields(self.dclass)
            self.reload(self.do, self.dclass, dbFields)

        values = self.values
        if fields != None:
            # If fields is supplied, it is a subset of fields to update.
            values = {}
            for field in fields:
                if field in self.values:
                    values[field] = self.values[field]
                else:
                    self.notify.warning("Field %s not defined." % (field))

        self.setFields(values)

    def getFields(self, fields):
        # Get a unique context for this query and associate ourselves
        # in the map.
        context = self.air.dbObjContext
        self.air.dbObjContext += 1
        self.air.dbObjMap[context] = self

        dg = PyDatagram()
        dg.addServerHeader(DBSERVER_ID, self.air.ourChannel, DBSERVER_GET_STORED_VALUES)
        dg.addUint32(context)
        dg.addUint32(self.doId)
        dg.addUint16(len(fields))
        for f in fields:
            dg.addString(f)

        self.air.send(dg)

    def unpackFromBlob(self, data, field):
        unpacker = DCPacker()

        unpacker.setUnpackData(data)
        unpacker.beginUnpack(field)

        args = field.unpackArgs(unpacker)

        unpacker.endUnpack()

        return args

    def getFieldsResponse(self, di):
        objId = di.getUint32()
        if objId != self.doId:
            self.notify.warning("Unexpected doId %d" % (objId))
            return

        count = di.getUint16()
        fields = []
        for i in range(count):
            name = di.getString()
            fields.append(name)

        retCode = di.getUint8()
        if retCode != 0:
            self.notify.warning("Failed to retrieve data for object %d" % (self.doId))
        else:
            for i in range(count):
                packedValue, found = di.getBlob(), di.getBool()

                if not found:
                    # this occurs for all DB fields on a pet when it's first created
                    # this should be a warning, but until we can create pirates with
                    # their required fields initialized in the DB, keep this as 'info'
                    self.notify.warning("field %s is not found" % fields[i])
                    continue

                self.values[fields[i]] = self.unpackFromBlob(packedValue, self.dclass.getFieldByName(fields[i]))
                self.notify.debug(f'{fields[i]} = {self.values[fields[i]]}')

            self.notify.info("got data for %d" % (self.doId))

            if self.gotDataHandler != None:
                self.gotDataHandler(self.do, self.dclass)
                self.gotDataHandler = None

        if self.doneEvent != None:
            messenger.send(self.doneEvent, [self, retCode])

    def setFields(self, values):
        dg = PyDatagram()
        dg.addServerHeader(DBSERVER_ID, self.air.ourChannel, DBSERVER_SET_STORED_VALUES)

        dg.addUint32(self.doId)
        dg.addUint16(len(values))

        for name, value in list(values.items()):
            dg.addString(name)
            dg.addBlob(self.packArgs(name, value))

        self.air.send(dg)

    def getDatabaseFields(self, dclass, inherited: bool = False):
        """getDatabaseFields(self, DCClass dclass, bool inherited)

        Returns the list of fields associated with the indicated
        DCClass that should be stored on the database.
        """
        fields = []
        for i in range(dclass.getNumFields() if not inherited else dclass.getNumInheritedFields()):
            dcf = dclass.getField(i) if not inherited else dclass.getInheritedField(i)
            af = dcf.asAtomicField()
            if af or not inherited:
                if af.isDb():
                    fields.append(af.getName())

        return fields

    def fillin(self, do, dclass):
        """fillin(self, DistributedObjectAI do, DCClass dclass)

        Fills in all the data from the DatabaseObject into the
        indicated distributed object by calling the appropriate set*()
        functions, where defined.

        """
        do.doId = self.doId
        for field, value in list(self.values.items()):
            dclass.directUpdate(do, field, value)

    def reload(self, do, dclass, fields):
        """reload(self, DistributedObjectAI do, DCClass dclass)

        Re-reads all of the data from the DistributedObject and stores
        it in the values table.
        """
        self.doId = do.doId

        self.values = {}
        for fieldName in fields:
            field = dclass.getFieldByName(fieldName)
            if field == None:
                self.notify.warning("No definition for %s" % (fieldName))
            else:
                dg = PyDatagram()
                packOk = dclass.packRequiredField(dg, do, field)
                assert(packOk)
                self.values[fieldName] = dg

    def packArgs(self, key, value):
        field = self.dclass.getFieldByName(key)

        packer = DCPacker()
        packer.beginPack(field)

        field.packArgs(packer, value)
        packer.endPack()

        return packer.getBytes()

    def createObject(self, objectType, values):
        # If we just want the default values for the new object's fields,
        # there's no need to specify any field values here. (Upon generation,
        # fields that are not stored in the database are assigned their
        # default values).
        # In the future, when creating an object in the DB, we may want
        # to provide values that differ from the default; this dict is
        # the place to put those values.
        # Note that the values in this dict must be specially formatted in
        # a datagram; I haven't sussed that out. See self.reload() and
        # AIDistUpdate.insertArg().

        # objectType is an integer that the DB uses to distinguish object
        # types, i.e. PiratesAIMsgTypes.DBSERVER_PET_OBJECT_TYPE
        assert type(objectType) is int

        # Get a unique context for this query and associate ourselves
        # in the map.
        context = self.air.dbObjContext
        self.air.dbObjContext += 1
        self.air.dbObjMap[context] = self

        self.createObjType = objectType

        dg = PyDatagram()
        dg.addServerHeader(DBSERVER_ID, self.air.ourChannel, DBSERVER_CREATE_STORED_OBJECT)

        dg.addUint32(context)
        dg.addString('')
        dg.addUint16(objectType)
        dg.addUint16(len(values))

        for name, value in list(values.items()):
            dg.addString(name)
            dg.addBlob(self.packArgs(name, value))

        self.air.send(dg)

    def handleCreateObjectResponse(self, di):
        retCode = di.getUint8()
        if retCode != 0:
            self.notify.warning("Database object %s create failed" %
                                (self.createObjType))
        else:
            del self.createObjType
            # The object has just been created in the database. We do not
            # have an instance of it, but we know its doId.
            self.doId = di.getUint32()

        if self.doneEvent != None:
            messenger.send(self.doneEvent, [self, retCode])

    def deleteObject(self):
        self.notify.warning('deleting object %s' % self.doId)

        dg = PyDatagram()
        dg.addServerHeader(DBSERVER_ID, self.air.ourChannel, DBSERVER_DELETE_STORED_OBJECT)

        dg.addUint32(self.doId)
        dg.addUint32(0xdeadbeef)

        # bye bye
        self.air.send(dg)
