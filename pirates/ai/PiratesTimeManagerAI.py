from otp.ai.TimeManagerAI import TimeManagerAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class PiratesTimeManagerAI(TimeManagerAI):
    notify = directNotify.newCategory('PiratesTimeManagerAI')

    def setFrameRate(self, fps, deviation, numAvs, numShips, locationCode, timeInLocation, timeInGame, gameOptionsCode, vendorId, deviceId, processMemory, pageFileUsage, physicalMemory, pageFaultCount, osInfo, cpuSpeed, numCpuCores, numLogicalCpus, apiName):
        pass