from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

from otp.ai.MagicWordManagerAI import MagicWordManagerAI
from pirates.ship import ShipGlobals

from typing import Union
import time, limeade

class PiratesMagicWordManagerAI(MagicWordManagerAI):
    notify = directNotify.newCategory('PiratesMagicWordManagerAI')

    def __init__(self, air):
        MagicWordManagerAI.__init__(self, air)

        self.sentFromExt = False

        self.staffMembers = []

    def announceGenerate(self):
        MagicWordManagerAI.announceGenerate(self)

        # Register our events.
        self.air.netMessenger.accept('magicWord', self, self.setMagicWordExt)
        self.air.netMessenger.accept('magicWordApproved', self, self.setMagicWordApproved)

    def setMagicWordApproved(self, accountId):
        self.staffMembers.append(accountId)

    def requestServerTime(self):
        avatarId = self.air.getAvatarIdFromSender()
        avatar = self.air.doId2do.get(avatarId)

        if not avatar:
            return

        sinceEpoch = time.time()
        self.d_recvServerTime(avatar.doId, sinceEpoch)

    def d_recvServerTime(self, avatarId, sinceEpoch):
        self.sendUpdateToAvatarId(avatarId, 'recvServerTime', [sinceEpoch])

    def sendSystemMessage(self, av, message: str) -> str:
        if message == '':
            return

        self.air.sendSystemMessage(message)

        return 'Sent system message!'

    def setGold(self, av, amount: int) -> str:
        inventory = av.getInventory()
        inventory.setGoldInPocket(inventory.getGoldInPocket() + amount)

        return f'Current gold is now: {inventory.getGoldInPocket()}.'

    def setZombie(self, av) -> str:
        state, cursed = av.getZombie()

        if state:
            av.b_setZombie(0, 0)
            return 'The curse has worn off...'
        else:
            av.b_setZombie(1, 1)
            return f'{av.getName()} has been cursed!'

    def setName(self, av, name: str) -> str:
        av.b_setName(name)

        return 'Changed name!'

    def skipActiveQuest(self, av) -> str:
        questId = av.getActiveQuest()

        if not questId:
            return 'You do not have a active quest!'

        quest = simbase.air.questMgr.getQuest(av, questId = questId)

        for taskState in quest.getTaskStates():
            taskState.forceComplete()

        self.air.questMgr.completeQuest(av, quest)

        return f'Forced quest: {questId} completion status.'

    def createShip(self, av) -> str:
        if not self.air.wantShips:
            return 'Ships are not enabled!'

        self.air.shipLoader.createShip(av, ShipGlobals.INTERCEPTORL1)

        return 'Gave ship!'

    def setFounder(self, av, state: int) -> str:
        av.d_setFounder(state)

        return f'Founder set to: {state}.'

    def setHp(self, av, value: int) -> str:
        value = min(value, av.getMaxHp())
        av.b_setHp(value)

        return f'Set health to: {value}'

    def setGMTag(self, av, gmNameTagState: int, gmNameTagColor: int, gmNameTagString: str):
        validColors = [
            'gold',
            'red',
            'green',
            'blue',
            'white'
        ]

        if gmNameTagColor not in validColors:
            response = 'Invalid color specified!'
            self.sendResponseMessage(av.doId, response)
            return

        if gmNameTagState < 0 or gmNameTagState > 1:
            response = 'Invalid state!'
            self.sendResponseMessage(av.doId, response)
            return

        av.b_setAsGM(1)
        av.b_setFounder(True)
        av.b_updateGMNameTag(gmNameTagState, gmNameTagColor, gmNameTagString)

        return 'Nametag set.'

    def setHat(self, av, hatId: int, hatColor: int):
        self.d_setHat(av, hatId, hatColor)

        return 'Set hat and color!'

    def d_setHat(self, av, hatId: int, hatColor: int):
        av.sendUpdate('setHatIdx', [hatId])
        av.sendUpdate('setHatColor', [hatColor])

    def giveQuest(self, av, questId) -> str:
        if simbase.air.questMgr.hasQuest(av, questId = questId):
            return f'Avatar already has active quest: {questId}!'

        simbase.air.questMgr.createQuest(av, questId)
        return f'Given new active quest: {questId}.'

    def refreshModules(self, av) -> str:
        # Refresh our AI modules.
        limeade.refresh()

        # Refresh our UberDOG modules.
        self.netMessenger.send('refreshModules')

        return 'AI/UD modules have been refreshed.'

    def checkArguments(self, magicWord: str, function, args) -> tuple[bool, str]:
        fArgs = function.__code__.co_argcount - 1
        argCount = len(args)

        if argCount > fArgs:
            return False, 'Invalid argument count!'

        minArgs = fArgs - (len(function.__defaults__) if function.__defaults__ else 0) - 1

        if argCount < minArgs:
            return False, f'{magicWord} requires at least {str(minArgs)} arguments but received {str(argCount)}!'

        return True, 'Success!'

    def sendResponseMessage(self, avId: int, message: str):
        self.sendUpdateToAvatarId(avId, 'setMagicWordResponse', [message])

    def setMagicWordExt(self, magicWord: str, avId: int):
        av = self.air.doId2do.get(avId)

        if not av:
            return

        self.sentFromExt = True

        self.setMagicWord(magicWord, avId, av.zoneId, '')

    def setMagicWord(self, magicWord: str, avId: int, zoneId: int, signature: str):
        if not self.sentFromExt:
            # We must be using a custom client.
            avId = self.air.getAvatarIdFromSender()

        # Grab our avatar.
        av = self.air.doId2do.get(avId)

        if self.air.wantProduction and av.getDISLid() not in self.staffMembers:
            # Log this attempt.
            self.air.writeServerEvent('suspicious', avId, 'Tried to invoke magic word with insufficient access.')
            return

        # Chop off the prefix at the start as its not needed
        magicWord = magicWord[1:]
        # Split the Magic Word.
        splitWord = magicWord.split(' ')
        # Grab the arguments.
        args = splitWord[1:]
        # Make the Magic Word case insensitive.
        magicWord = splitWord[0].lower()
        del splitWord

        # Log this attempt.
        self.notify.info(f'{av.getName()} ({avId}) executed Magic Word: {magicWord}!')

        # Grab all of our string arguments.
        string = ' '.join(str(x) for x in args)
        stringVal = ' '.join(str(x) for x in args[2:])

        clientWords = [
            'run',
            'fps',
            'mario',
            'gmNameTag',
            'rocketman',
            'shiphat'
        ]

        if magicWord in clientWords or magicWord == '':
            # We can ignore this.
            return

        if magicWord in ('system', 'smsg'):
            response = self.sendSystemMessage(av, message=string)
        elif magicWord == 'setgold':
            wordValid = self.checkArguments(magicWord, self.setGold, args)
            if wordValid[0]:
                response = self.setGold(av, int(args[0]))
            else:
                response = wordValid[1]
        elif magicWord == 'setzombie':
            response = self.setZombie(av)
        elif magicWord in ('name', 'setname'):
            response = self.setName(av, string)
        elif magicWord == 'skipactivequest':
            response = self.skipActiveQuest(av)
        elif magicWord in ('giveship', 'ship'):
            response = self.createShip(av)
        elif magicWord == 'refresh':
            response = self.refreshModules(av)
        elif magicWord == 'givequest':
            wordValid = self.checkArguments(magicWord, self.giveQuest, args)
            if wordValid[0]:
                response = self.giveQuest(av, int(args[0]))
            else:
                response = wordValid[1]
        elif magicWord == 'setfounder':
            wordValid = self.checkArguments(magicWord, self.setFounder, args)
            if wordValid[0]:
                response = self.setFounder(av, int(args[0]))
            else:
                response = wordValid[1]
        elif magicWord == 'givedefaultquest':
            av.giveDefaultQuest()
            response = 'Gave default quest!'
        elif magicWord == 'refreshmodules':
            limeade.refresh()
            self.air.netMessenger.send('refreshModules')
            response = 'Refreshed server modules!'
        elif magicWord in ('setgmtag', 'gamemaster'):
            wordValid = self.checkArguments(magicWord, self.setGMTag, args)
            if wordValid[0]:
                response = self.setGMTag(av, int(args[0]), int(args[1]), stringVal)
            else:
                response = wordValid[1]
        elif magicWord == 'sethat':
            wordValid = self.checkArguments(magicWord, self.setHat, args)
            if wordValid[0]:
                response = self.setHat(av, int(args[0]), int(args[1]))
            else:
                response = wordValid[1]
        elif magicWord == 'sethp':
            wordValid = self.checkArguments(magicWord, self.setHp, args)
            if wordValid[0]:
                response = self.setHp(av, int(args[0]))
            else:
                response = wordValid[1]
        else:
            response = f'{magicWord} is not a valid Magic Word.'
            self.notify.info(f'Unknown Magic Word: {magicWord} from avId: {avId}!')

        # Send our response to the client.
        self.sendResponseMessage(avId, response)

        # Call our main class.
        MagicWordManagerAI.setMagicWord(self, magicWord, avId, zoneId)
