from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from direct.http.webAIInspector import aiWebServer

from otp.otpbase import OTPGlobals
from direct.directnotify import DirectNotifyGlobal
from otp.ai.AIDistrict import AIDistrict
#from otp.friends import FriendManagerAI
from otp.distributed.OtpDoGlobals import *
from otp.otpbase.OTPGlobals import *

from pirates.distributed.PiratesDistrictAI import PiratesDistrictAI
from pirates.distributed.DistributedPopulationTrackerAI import DistributedPopulationTrackerAI
from pirates.distributed.TargetManagerAI import TargetManagerAI

from pirates.ai.PiratesTimeManagerAI import PiratesTimeManagerAI
from pirates.ai.PiratesMagicWordManagerAI import PiratesMagicWordManagerAI
from pirates.ai.NewsManagerAI import NewsManagerAI

from pirates.piratesbase.UniqueIdManager import UniqueIdManager
from pirates.piratesbase.PiratesGlobals import *
from pirates.piratesbase.DistributedTimeOfDayManagerAI import DistributedTimeOfDayManagerAI
from pirates.piratesbase.DistributedGameStatManagerAI import DistributedGameStatManagerAI

from pirates.world.WorldGlobals import *
from pirates.world.WorldGridManagerAI import WorldGridManagerAI
from pirates.world.WorldCreatorAI import WorldCreatorAI

from pirates.instance.DistributedTeleportMgrAI import DistributedTeleportMgrAI

from pirates.battle.DistributedEnemySpawnerAI import DistributedEnemySpawnerAI

from pirates.trades.TradeManagerAI import TradeManagerAI

from pirates.quest.QuestManagerAI import QuestManagerAI

from pirates.piratesgui.DistributedPirateProfileMgrAI import DistributedPirateProfileMgrAI

from pirates.band.DistributedPirateBandManagerAI import DistributedPirateBandManagerAI

from pirates.pvp.PVPManagerAI import PVPManagerAI

if __debug__:
    import pdb

class PiratesAIRepository(AIDistrict):
    notify = DirectNotifyGlobal.directNotify.newCategory(
            "PiratesAIRepository")

    def __init__(self, *args, **kw):
        AIDistrict.__init__(self, *args, **kw)
        self.setTimeWarning(ConfigVariableDouble('aimsg-time-warning', 4).getValue())

        self.uidMgr = UniqueIdManager(self)

        self.wantShips = ConfigVariableBool('want-ships', False).getValue()
        self.wantQuests = ConfigVariableBool('want-quests', False).getValue()
        self.wantBands = ConfigVariableBool('want-bands', False).getValue()

        # turn on garbage-collection debugging to see if it's related
        # to the chugs we're seeing
        # eventually we will probably put in our own gc pump
        if ConfigVariableBool('gc-debug', False).getValue():
            import gc
            gc.set_debug(gc.DEBUG_STATS)

    def getGameDoId(self):
        return OTP_DO_ID_PIRATES

    def getDatabaseIdForClassName(self, className):
        return DatabaseIdFromClassName.get(
            className, DefaultDatabaseChannelId)

    def _isValidPlayerLocation(self, parentId, zoneId):
        return True

        # keep players out of parents that are smaller than N, where N is smaller than all district IDs
        # and N is random enough to be confusing for hackers
        if parentId < 900000:
            return False
        # keep players out of uberzones and zones up to 900 which are not used anyway, to confuse hackers
        if (OTPGlobals.UberZone <= zoneId <= 900):
            return False
        """
        # this doesn't work in the current TT LIVE publish, when teleporting, old district gets
        # setLocation on new district
        if parentId != self.districtId:
            return False
        if zoneId == 2:
            return False
            """
        return True

    def createObjects(self):
        # Create a new district (aka shard) for this AI:
        self.district = PiratesDistrictAI(self, self.districtName, PiratesWorldSceneFile)
        self.district.generateOtpObject(
                self.getGameDoId(), OTP_ZONE_ID_DISTRICTS,
                doId=self.districtId
        )

        self.populationTracker = DistributedPopulationTrackerAI(self)
        self.populationTracker.setShardId(self.districtId)
        self.populationTracker.setPopLimits(
            ConfigVariableInt('shard-pop-limit-low', 100).getValue(),
            ConfigVariableInt('shard-pop-limit-high', 300).getValue()
        )
        self.populationTracker.generateOtpObject(self.getGameDoId(), OTP_ZONE_ID_DISTRICTS_STATS)

        # The Time manager.  This negotiates a timestamp exchange for
        # the purposes of synchronizing clocks between client and
        # server with a more accurate handshaking protocol than we
        # would otherwise get.
        #
        # We must create this object first, so clients who happen to
        # come into the world while the AI is still coming up
        # (particularly likely if the AI crashed while players were
        # in) will get a chance to synchronize.
        self.timeManager = PiratesTimeManagerAI(self)
        self.timeManager.generateOtpObject(
            self.district.getDoId(), OTPGlobals.UberZone)

        self.teleportMgr = DistributedTeleportMgrAI(self)
        self.teleportMgr.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.timeOfDayMgr = DistributedTimeOfDayManagerAI(self)
        self.timeOfDayMgr.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.newsManager = NewsManagerAI(self)
        self.newsManager.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        if not self.wantProduction:
            self.injectionManager = self.generateGlobalObject(OTP_DO_ID_INJECTION_MGR, 'InjectionManager')

        self.holidayMgr = self.generateGlobalObject(OTP_DO_ID_PIRATES_HOLIDAY_MANAGER, 'HolidayManager')

        self.gameStatManager = DistributedGameStatManagerAI(self)
        self.gameStatManager.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.targetMgr = TargetManagerAI(self)
        self.targetMgr.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.inventoryManager = self.generateGlobalObject(OTP_DO_ID_PIRATES_INVENTORY_MANAGER_BASE, 'DistributedInventoryManager')

        self.spawner = DistributedEnemySpawnerAI(self)
        self.spawner.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        # The Magic Word Manager
        if ConfigVariableBool('want-magic-words', True).getValue():
            self.magicWordManager = PiratesMagicWordManagerAI(self)
            self.magicWordManager.generateWithRequired(OTPGlobals.UberZone)

        self.tradeMgr = TradeManagerAI(self)
        self.tradeMgr.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.crewMatchManager = self.generateGlobalObject(OTP_DO_ID_PIRATES_CREW_MATCH_MANAGER, 'DistributedCrewMatchManager')

        self.guildManager = self.generateGlobalObject(OTP_DO_ID_PIRATES_GUILD_MANAGER, 'PCGuildManager')

        self.questMgr = QuestManagerAI(self)

        self.shipLoader = self.generateGlobalObject(OTP_DO_ID_PIRATES_SHIP_MANAGER, 'DistributedShipLoader')

        self.pirateProfileMgr = DistributedPirateProfileMgrAI(self)
        self.pirateProfileMgr.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.worldGridManager = WorldGridManagerAI(self)

        self.bandManager = DistributedPirateBandManagerAI(self)
        self.bandManager.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        self.pvpManager = PVPManagerAI(self)
        self.pvpManager.generateWithRequired(OTP_ZONE_ID_MANAGEMENT)

        # The Friend Manager
        # self.friendManager = FriendManagerAI.FriendManagerAI(self)
        # self.friendManager.generateWithRequired(OTPGlobals.UberZone)

        # mark district as avaliable
        self.district.b_setAvailable(1)

        # Create our zones
        self.worldCreator = WorldCreatorAI(self)
        self.worldCreator.loadObjectsFromFile(PiratesWorldSceneFile)

        self.httpInspector = aiWebServer(self)

    def getHandleClassNames(self):
        # This function should return a tuple or list of string names
        # that represent distributed object classes for which we want
        # to make a special 'handle' class available.
        return ('DistributedPlayerPirate',)

    def getMinDynamicZone(self):
        # Override this to return the minimum allowable value for a
        # dynamically-allocated zone id.
        return DynamicZonesBegin

    def getMaxDynamicZone(self):
        # Override this to return the maximum allowable value for a
        # dynamically-allocated zone id.

        # Note that each zone requires the use of the channel derived
        # by self.districtId + zoneId.  Thus, we cannot have any zones
        # greater than or equal to self.minChannel - self.districtId,
        # which is our first allocated doId.
        return min(self.minChannel - self.districtId, DynamicZonesEnd) - 1

    def handlePlayGame(self, msgType, di):
        AIDistrict.handlePlayGame(self, msgType, di)
