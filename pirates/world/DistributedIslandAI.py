from pirates.world.DistributedGameAreaAI import DistributedGameAreaAI
from direct.distributed.DistributedCartesianGridAI import DistributedCartesianGridAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.world import WorldGlobals
from pirates.world.LocationConstants import LocationIds
from pirates.battle.Teamable import Teamable
from direct.task import Task
from pirates.world.DistributedShipDeployerAI import DistributedShipDeployerAI
from pirates.piratesbase import PiratesGlobals
import random


class DistributedIslandAI(DistributedGameAreaAI, DistributedCartesianGridAI, Teamable):
    notify = directNotify.newCategory('DistributedIslandAI')
    notify.setInfo(True)

    def __init__(self, air):
        startingZone = WorldGlobals.ISLAND_GRID_STARTING_ZONE
        cellWidth = WorldGlobals.ISLAND_CELL_SIZE
        gridSize = WorldGlobals.ISLAND_GRID_SIZE
        gridRadius = WorldGlobals.ISLAND_GRID_RADIUS

        DistributedCartesianGridAI.__init__(self, air, startingZone, gridSize, gridRadius, cellWidth, style='CartesianStated')
        DistributedGameAreaAI.__init__(self, air)
        Teamable.__init__(self)

        self.sphereRadii = [1000, 2000, 4000]
        self.sphereCenter = [0, 0]
        self.islandModel = ''
        self.undockable = False
        self.collisionSpheres = []
        self.feastFireEnabled = False
        self.fireworkShowEnabled = [False, 0]

        self.nextEvent = 0

        self.shipDeployer = None

    def handleChildArrive(self, childObj, zoneId):
        """
        This is called when a child object enters our island.
        The only functionality we want to add here is that we want to set the
        return location of players that enter our island to ourselves.
        """

        # Process the normal handleChildArrive function, and find out if this child is a player.
        isPlayer = DistributedGameAreaAI.handleChildArrive(self, childObj, zoneId)

        # If this child is a player, and we aren't already their return location,
        # set ourselves as their return location.
        if isPlayer and childObj.returnLocation != self.uniqueId:
            childObj.b_setReturnLocation(self.uniqueId)

    def generate(self):
        DistributedCartesianGridAI.generate(self)
        DistributedGameAreaAI.generate(self)

        # We want to generate our ship deployer, which creates
        # collision spheres surrounding us based on our radius.
        self.shipDeployer = DistributedShipDeployerAI(self.air)
        self.shipDeployer.setMinRadius(self.getZoneSphereSize()[0])
        self.shipDeployer.setMaxRadius(self.getZoneSphereSize()[2])
        self.shipDeployer.setSpacing((self.gridSize - self.gridRadius) * self.cellWidth)
        self.shipDeployer.setHeading(self.getH())
        self.shipDeployer.generateWithRequiredAndId(self.air.allocateChannel(),
                                                    self.doId, PiratesGlobals.IslandShipDeployerZone)

    def announceGenerate(self):
        DistributedCartesianGridAI.announceGenerate(self)
        DistributedGameAreaAI.announceGenerate(self)
        if config.GetBool('want-island-events', True):
            self.__runIslandEvents()
            self.runEvents = taskMgr.doMethodLater(15, self.__runIslandEvents, 'runEvents')

    def delete(self):
        DistributedCartesianGridAI.delete(self)
        DistributedGameAreaAI.delete(self)

    def __runIslandEvents(self, task=None):
        self.nextEvent -= 15
        if self.nextEvent <= 0:
            islandId = self.getUniqueId()
            if islandId == LocationIds.DEL_FUEGO_ISLAND:
                self.d_makeLavaErupt()
                self.nextEvent = max(config.GetInt('eruption-delay', 60), 15) if config.GetBool('want-dev', False) else random.randint(5, 10) * 60
                self.notify.debug('%s has erupted! Next eruption is in %d seconds' % (self.getLocalizerName(), self.nextEvent))

        return Task.again

    def setGridSize(self):
        """
        This sets the real grid size of the island based off of the
        LOD Sphere's radius and center.
        """

        self.gridSize = self.getGridSizeFromSphere(self.getZoneSphereSize()[2] / 2, self.getZoneSphereCenter(), self.cellWidth, self.gridRadius)

    def setIslandTransform(self, x, y, z, h):
        self.setXYZH(x, y, z, h)

    def d_setIslandTransform(self, x, y, z, h):
        self.sendUpdate('setIslandTransform', [x, y, z, h])

    def b_setIslandTransform(self, x, y, z, h):
        self.setIslandTransform(x, y, z, h)
        self.d_setIslandTransform(x, y, z, h)

    def getIslandTransform(self):
        return [self.getX(), self.getY(), self.getZ(), self.getH()]

    def setZoneSphereSize(self, rad0, rad1, rad2):
        self.sphereRadii = [rad0, rad1, rad2]

        self.setGridSize()

    def d_setZoneSphereSize(self, rad0, rad1, rad2):
        self.sendUpdate('setZoneSphereSize', [rad0, rad1, rad2])

    def b_setZoneSphereSize(self, rad0, rad1, rad2):
        self.setZoneSphereSize(rad0, rad1, rad2)
        self.d_setZoneSphereSize(rad0, rad1, rad2)

    def getZoneSphereSize(self):
        return self.sphereRadii

    def setZoneSphereCenter(self, x, y):
        self.sphereCenter = [x, y]

        self.setGridSize()

    def d_setZoneSphereCenter(self, x, y):
        self.sendUpdate('setZoneSphereCenter', [x, y])

    def b_setZoneSphereCenter(self, x, y):
        self.setZoneSphereCenter(x, y)
        self.d_setZoneSphereCenter(x, y)

    def getZoneSphereCenter(self):
        return self.sphereCenter

    def setIslandModel(self, islandModel):
        self.islandModel = islandModel

    def d_setIslandModel(self, islandModel):
        self.sendUpdate('setIslandModel', [islandModel])

    def b_setIslandModel(self, islandModel):
        self.setIslandModel(islandModel)
        self.d_setIslandModel(islandModel)

    def getIslandModel(self):
        return self.islandModel

    def setUndockable(self, undockable):
        self.undockable = undockable

    def d_setUndockable(self, undockable):
        self.sendUpdate('setUndockable', [undockable])

    def b_setUndockable(self, undockable):
        self.setUndockable(undockable)
        self.d_setUndockable(undockable)

    def getUndockable(self):
        return self.undockable

    def setPortCollisionSpheres(self, collisionSpheres):
        self.collisionSpheres = collisionSpheres

    def d_setPortCollisionSpheres(self, collisionSpheres):
        self.sendUpdate('setPortCollisionSpheres', [collisionSpheres])

    def b_setPortCollisionSpheres(self, collisionSpheres):
        self.setPortCollisionSpheres(collisionSpheres)
        self.d_setPortCollisionSpheres(collisionSpheres)

    def getPortCollisionSpheres(self):
        return self.collisionSpheres

    def d_makeLavaErupt(self):
        self.sendUpdate('makeLavaErupt', [])

    def setFeastFireEnabled(self, feastFireEnabled):
        self.feastFireEnabled = feastFireEnabled

    def d_setFeastFireEnabled(self, feastFireEnabled):
        self.sendUpdate('setFeastFireEnabled', [feastFireEnabled])

    def b_setFeastFireEnabled(self, feastFireEnabled):
        self.setFeastFireEnabled(feastFireEnabled)
        self.d_setFeastFireEnabled(feastFireEnabled)

    def getFeastFireEnabled(self):
        return self.feastFireEnabled

    def setFireworkShowEnabled(self, isEnabled, showType):
        self.fireworkShowEnabled = [isEnabled, showType]

    def d_setFireworkShowEnabled(self, isEnabled, showType):
        self.sendUpdate('setFireworkShowEnabled', [isEnabled, showType])

    def b_setFireworkShowEnabled(self, isEnabled, showType):
        self.setFireworkShowEnabled(isEnabled, showType)
        self.d_setFireworkShowEnabled(isEnabled, showType)

    def getFireworkShowEnabled(self):
        return self.fireworkShowEnabled

    def delete(self):
        DistributedCartesianGridAI.delete(self)
        DistributedGameAreaAI.delete(self)
