from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.world.DistributedDoorAI import DistributedDoorAI
from pirates.teleport.InteriorDoorTeleportActorAI import InteriorDoorTeleportActorAI
from otp.distributed import OtpDoGlobals


class DistributedBuildingDoorAI(DistributedDoorAI):
    notify = directNotify.newCategory('DistributedBuildingDoorAI')

    def __init__(self, air):
        DistributedDoorAI.__init__(self, air)
        self.interiorDoId = 0
        self.interiorUid = ''
        self.interiorWorldParentId = 0
        self.interiorWorldZoneId = 0
        self.interior = None

    def setInteriorId(self, interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId):
        self.interiorDoId = interiorDoId
        self.interiorUid = interiorUid
        self.interiorWorldParentId = interiorWorldParentId
        self.interiorWorldZoneId = interiorWorldZoneId
        self.interior = self.air.doId2do.get(self.interiorDoId)

    def d_setInteriorId(self, interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId):
        self.sendUpdate('setInteriorId', [interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId])

    def b_setInteriorId(self, interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId):
        self.setInteriorId(interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId)
        self.d_setInteriorId(interiorDoId, interiorUid, interiorWorldParentId, interiorWorldZoneId)

    def getInteriorId(self):
        return [self.interiorDoId, self.interiorUid, self.interiorWorldParentId, self.interiorWorldZoneId]

    def requestPrivateInteriorInstance(self):
        avatar = self.air.doId2do.get(self.air.getAvatarIdFromSender())

        if not avatar:
            return

        tpActor = InteriorDoorTeleportActorAI(self.air)

        # Generate our teleport actor.
        tpActor.generateWithRequired(OtpDoGlobals.OTP_ZONE_ID_OLD_QUIET_ZONE)

        # Generate our OwnerView instance.
        self.air.sendSetOwnerDoId(tpActor.doId, avatar.doId)

        # Set our building information.
        tpActor.interiorInfo = self.getInteriorId()
        tpActor.doorId = self.interior.getInteriorDoor().doId

        # Set our area
        area = self.air.uidMgr.getDo(self.getInteriorId()[1])
        tpActor.setGoToArea(area)
        tpActor.setInstance(area.getParentObj())

        # Start the teleportation process.
        tpActor.sendGotoQuietZone()
        tpActor.sendCompleteShow()
