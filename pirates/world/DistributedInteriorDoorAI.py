from pirates.world.DistributedDoorAI import DistributedDoorAI
from direct.directnotify.DirectNotifyGlobal import directNotify


class DistributedInteriorDoorAI(DistributedDoorAI):
    notify = directNotify.newCategory('DistributedInteriorDoorAI')

    def __init__(self, air):
        DistributedDoorAI.__init__(self, air)

        self.interiorDoId = 0
        self.interiorParentId = 0
        self.interiorZoneId = 0
        self.exteriorDoId = 0
        self.exteriorWorldParentId = 0
        self.exteriorWorldZoneId = 0
        self.buildingDoorId = 0

    def handleRequestInteraction(self, avatar, interactType, instant):
        result = DistributedDoorAI.handleRequestInteraction(self, avatar, interactType, instant)

        if not result:
            return self.DENY

        exterior = self.air.doId2do.get(self.exteriorDoId)

        if not exterior:
            self.notify.warning(f'Cannot handle avatar {avatar.doId} interact for door {self.doId}, exterior {self.exteriorDoId} not found!')
            return self.DENY

        if not self.otherDoor:
            self.notify.warning(f'Cannot handle avatar {avatar.doId} interact for door {self.doId}, exterior door not found!')
            return self.DENY

        self.air.teleportMgr.createTeleport(self.getExteriorId(), self.getBuildingDoorId(), avatar.doId)

        return self.ACCEPT

    def setInteriorId(self, interiorDoId, interiorParentId, interiorZoneId):
        self.interiorDoId = interiorDoId
        self.interiorParentId = interiorParentId
        self.interiorZoneId = interiorZoneId
        self.interior = self.air.doId2do.get(self.interiorDoId)

    def d_setInteriorId(self, interiorDoId, interiorParentId, interiorZoneId):
        self.sendUpdate('setInteriorId', [interiorDoId, interiorParentId, interiorZoneId])

    def b_setInteriorId(self, interiorDoId, interiorParentId, interiorZoneId):
        self.setInteriorId(interiorDoId, interiorParentId, interiorZoneId)
        self.d_setInteriorId(interiorDoId, interiorParentId, interiorZoneId)

    def getInteriorId(self):
        return [self.interiorDoId, self.interiorParentId, self.interiorZoneId]

    def setExteriorId(self, exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId):
        self.exteriorDoId = exteriorDoId
        self.exteriorWorldParentId = exteriorWorldParentId
        self.exteriorWorldZoneId = exteriorWorldZoneId
        self.exterior = self.air.doId2do.get(self.exteriorDoId)

    def d_setExteriorId(self, exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId):
        self.sendUpdate('setExteriorId', [exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId])

    def b_setExteriorId(self, exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId):
        self.setExteriorId(exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId)
        self.d_setExteriorId(exteriorDoId, exteriorWorldParentId, exteriorWorldZoneId)

    def getExteriorId(self):
        return [self.exteriorDoId, self.exteriorWorldParentId, self.exteriorWorldZoneId]

    def setBuildingDoorId(self, buildingDoorId):
        self.buildingDoorId = buildingDoorId

    def d_setBuildingDoorId(self, buildingDoorId):
        self.sendUpdate('setBuildingDoorId', [buildingDoorId])

    def b_setBuildingDoorId(self, buildingDoorId):
        self.setBuildingDoorId(buildingDoorId)
        self.d_setBuildingDoorId(buildingDoorId)

    def getBuildingDoorId(self):
        return self.buildingDoorId
