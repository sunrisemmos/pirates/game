from pirates.world.AreaBuilderBaseAI import AreaBuilderBaseAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals
from pirates.leveleditor import ObjectList
from pirates.world.DistributedInteriorDoorAI import DistributedInteriorDoorAI
from pirates.minigame.DistributedPokerTableAI import DistributedPokerTableAI
from pirates.minigame.DistributedHoldemTableAI import DistributedHoldemTableAI
from pirates.minigame.DistributedBlackjackTableAI import DistributedBlackjackTableAI
from pirates.minigame.Distributed7StudTableAI import Distributed7StudTableAI
from pirates.minigame.DistributedBishopsHandTableAI import DistributedBishopsHandTableAI
from pirates.minigame.DistributedLiarsDiceAI import DistributedLiarsDiceAI

class InteriorAreaBuilderAI(AreaBuilderBaseAI):
    notify = directNotify.newCategory('InteriorAreaBuilderAI')

    def __init__(self, air, parent):
        AreaBuilderBaseAI.__init__(self, air, parent)

        self.wantParlorGames = config.GetBool('want-parlor-games', True)

    def createObject(self, objType, objectData, parent, parentUid, objKey, dynamic):
        newObj = None

        if objType == ObjectList.DOOR_LOCATOR_NODE:
            newObj = self.createDoorLocatorNode(objectData, parent, parentUid, objKey)
        elif objType == 'Parlor Game' and self.wantParlorGames:
            newObj = self.__createParlorTable(objectData, parent, parentUid, objKey)
        elif objType in ['Animal', 'Townsperson', 'Spawn Node', 'Dormant NPC Spawn Node', 'Skeleton', 'NavySailor', 'Creature', 'Ghost']:
            newObj = self.air.spawner.createObject(objType, objectData, parent, parentUid, objKey, dynamic, PiratesGlobals.InteriorDoorZone)

        return newObj

    def createDoorLocatorNode(self, objectData, parent, parentUid, objKey):
        from pirates.world.DistributedInteriorDoorAI import DistributedInteriorDoorAI

        doorLocatorNode = DistributedInteriorDoorAI(self.air)
        doorLocatorNode.setUniqueId(objKey)
        doorLocatorNode.setPos(objectData.get('Pos', (0, 0, 0)))
        doorLocatorNode.setHpr(objectData.get('Hpr', (0, 0, 0)))
        doorLocatorNode.setScale(objectData.get('Scale', (1, 1, 1)))
        doorLocatorNode.setInteriorId(self.parent.doId, self.parent.parentId, self.parent.zoneId)

        if not self.parent.getInteriorFrontDoor():
            self.parent.setInteriorFrontDoor(doorLocatorNode)
            exteriorDoor = self.parent.getExteriorFrontDoor()
        else:
            doorLocatorNode.setDoorIndex(1)
            self.parent.setInteriorBackDoor(doorLocatorNode)
            exteriorDoor = self.parent.getExteriorBackDoor()

        if not exteriorDoor:
            self.notify.debug('Cannot generate interior door %s, '
                'cant find other exterior door!' % objKey)

            return

        exteriorWorld = self.parent.getParentObj()
        if not exteriorWorld:
            self.notify.debug('Cannot create interior door %s, '
                'for exterior with no parent!' % objKey)

            return

        exterior = exteriorDoor.getParentObj()
        if not exterior:
            self.notify.debug('Cannot create interior door %s, '
                'no exterior found!' % objKey)

            return

        doorLocatorNode.setBuildingUid(exteriorDoor.getBuildingUid())
        doorLocatorNode.setOtherDoor(exteriorDoor)
        doorLocatorNode.setExteriorId(exterior.doId, exteriorWorld.doId, exterior.zoneId)
        doorLocatorNode.setBuildingDoorId(exteriorDoor.doId)
        self.parent.setInteriorDoor(doorLocatorNode)

        zoneId = self.parent.getZoneFromXYZ(doorLocatorNode.getPos())
        self.parent.generateChildWithRequired(doorLocatorNode, zoneId)
        exteriorDoor.setOtherDoor(doorLocatorNode)
        self.addObject(doorLocatorNode)

        # update the game area's links
        links = self.parent.getLinks()
        links.append(['', doorLocatorNode.doId, '', self.parent.parentId, self.parent.zoneId, '',
            exteriorWorld.parentId, exteriorWorld.zoneId])

        self.parent.b_setLinks(links)
        return doorLocatorNode

    def __createParlorTable(self, objectData, parent, parentUid, objKey):
        tableCls = None
        gameType = objectData.get('Category', 'Unknown')

        if gameType == 'Holdem':
            tableCls = DistributedHoldemTableAI
        elif gameType == 'Blackjack':
            tableCls = DistributedBlackjackTableAI
        elif gameType == '7Stud':
            tableCls = Distributed7StudTableAI
        elif gameType == 'Bishops':
            tableCls = DistributedBishopsHandTableAI
        elif gameType == 'LiarsDice':
            tableCls = DistributedLiarsDiceAI
        else:
            self.notify.warning('Failed to generate Parlor Table %s; %s is not a valid game type' % (objKey, gameType))
            return

        gameTable = tableCls(self.air)
        gameTable.setUniqueId(objKey)
        gameTable.setPos(objectData.get('Pos', (0, 0, 0)))
        gameTable.setHpr(objectData.get('Hpr', (0, 0, 0)))
        gameTable.setScale(objectData.get('Scale', 1))

        gameTable.generatePlayers(gameTable.AVAILABLE_SEATS, gameTable.TABLE_AI)

        if isinstance(gameTable, DistributedPokerTableAI):
            gameTable.setGameType(gameType)

        if isinstance(gameTable, DistributedPokerTableAI) or isinstance(gameTable, DistributedBlackjackTableAI):
            gameTable.setBetMultiplier(int(objectData.get('BetMultiplier', '1')))

        self.parent.generateChildWithRequired(gameTable, PiratesGlobals.InteriorDoorZone)
        self.addObject(gameTable)

        self.broadcastObjectPosition(gameTable)

        return gameTable