from pirates.battle.DistributedBattleAvatarAI import DistributedBattleAvatarAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedFortAI(DistributedBattleAvatarAI):
    notify = directNotify.newCategory('DistributedFortAI')

    def setIslandId(self, todo0):
        pass

    def setObjKey(self, todo0):
        pass

    def setHp(self, todo0, todo1):
        pass

    def setLevel(self, todo0):
        pass

    def setDrawbridgesLerpR(self, todo0):
        pass

    def hideDrawbridges(self):
        pass

    def hitByProjectile(self, todo0, todo1):
        pass