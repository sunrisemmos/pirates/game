from pirates.distributed.DistributedInteractiveAI import DistributedInteractiveAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedCellDoorAI(DistributedInteractiveAI):
    notify = directNotify.newCategory('DistributedCellDoorAI')

    def setCellIndex(self, todo0):
        pass

    def setHealth(self, todo0):
        pass

    def doorKicked(self):
        pass