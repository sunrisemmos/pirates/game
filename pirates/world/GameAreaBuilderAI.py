from pirates.world.AreaBuilderBaseAI import AreaBuilderBaseAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals
from pirates.leveleditor import ObjectList
from pirates.interact.DistributedSearchableContainerAI import DistributedSearchableContainerAI
from pirates.minigame.DistributedFishingSpotAI import DistributedFishingSpotAI
from pirates.minigame.DistributedPotionCraftingTableAI import DistributedPotionCraftingTableAI
from pirates.minigame.DistributedRepairBenchAI import DistributedRepairBenchAI
from pirates.world.DistributedBuildingDoorAI import DistributedBuildingDoorAI
from pirates.world.DistributedDinghyAI import DistributedDinghyAI
from pirates.treasuremap.DistributedBuriedTreasureAI import DistributedBuriedTreasureAI
from pirates.treasuremap.DistributedSurfaceTreasureAI import DistributedSurfaceTreasureAI
from pirates.instance.DistributedInstanceBaseAI import DistributedInstanceBaseAI

class GameAreaBuilderAI(AreaBuilderBaseAI):
    notify = directNotify.newCategory('GameAreaBuilderAI')

    def __init__(self, air, parent):
        AreaBuilderBaseAI.__init__(self, air, parent)

        self.wantSearchables = config.GetBool('want-searchables', True)
        self.wantFishing = config.GetBool('want-fishing-game', False)
        self.wantPotionTable = config.GetBool('want-potion-game', False)
        self.wantBuildingInteriors = config.GetBool('want-building-interiors', True)
        self.wantDinghys = config.GetBool('want-dinghys', True)
        self.wantSpawnNodes = config.GetBool('want-spawn-nodes', False)
        self.wantRepairBench = config.GetBool('want-repair-game', False)
        self.wantIslandAreas = config.GetBool('want-island-game-areas', True)

    def createObject(self, objType, objectData, parent, parentUid, objKey, dynamic):
        newObj = None

        if objType == 'Player Spawn Node':
            newObj = self.__createPlayerSpawnNode(objectData, parent, parentUid, objKey, dynamic)
        elif objType == 'Searchable Container' and self.wantSearchables:
            newObj = self.__createSearchableContainer(parent, parentUid, objKey, objectData)
        elif objType == 'FishingSpot' and self.wantFishing:
            newObj = self.__createFishingSpot(parent, parentUid, objKey, objectData)
        elif objType == 'PotionTable' and self.wantPotionTable:
            newObj = self.__createPotionTable(parent, parentUid, objKey, objectData)
        elif objType in ['Animal', 'Townsperson', 'Spawn Node', 'Dormant NPC Spawn Node', 'Skeleton', 'NavySailor', 'Creature', 'Ghost']:
            newObj = self.air.spawner.createObject(objType, objectData, parent, parentUid, objKey, dynamic, PiratesGlobals.IslandLocalZone)
        elif objType == 'Building Exterior' and self.wantBuildingInteriors:
            newObj = self.__createBuildingExterior(parent, parentUid, objKey, objectData)
        elif objType == ObjectList.DOOR_LOCATOR_NODE and self.wantBuildingInteriors:
            newObj = self.createDoorLocatorNode(parent, parentUid, objKey, objectData)
        elif objType == 'Dinghy' and self.wantDinghys:
            newObj = self.__createDinghy(parent, parentUid, objKey, objectData)
        elif objType == 'Object Spawn Node' and self.wantSpawnNodes:
            newObj = self.__createObjectSpawnNode(parent, parentUid, objKey, objectData)
        elif objType == 'RepairBench' and self.wantRepairBench:
            newObj = self.__createRepairBench(parent, parentUid, objKey, objectData)
        elif objType == ObjectList.AREA_TYPE_ISLAND_REGION and self.wantIslandAreas:
            newObj = self.__createIslandArea(parent, parentUid, objKey, objectData)

        return newObj

    def __createPlayerSpawnNode(self, objectData, parent, parentUid, objKey, dynamic):
        from pirates.world.DistributedIslandAI import DistributedIslandAI

        parent = self.parent.getParentObj()

        if isinstance(parent, DistributedIslandAI):
            parent = parent.getParentObj()

        if not parent or not isinstance(parent, DistributedInstanceBaseAI):
            self.notify.warning(f'Cannot setup player spawn point for {parent}!')
            return None

        (x, y, z), objectParent = self.getObjectTruePosAndParent(objKey, parentUid, objectData)
        h, p, r = objectData.get('Hpr', (0, 0, 0))

        parent.addSpawnPt(self.parent.getUniqueId(), (x, y, z, h))

        return None

    def __createSearchableContainer(self, parent, parentUid, objKey, objectData):
        container = DistributedSearchableContainerAI(self.air)
        container.setUniqueId(objKey)

        self.setObjectTruePosHpr(container, objKey, parentUid, objectData)
        container.setScale(objectData.get('Scale', (1, 1, 1)))
        container.setType(objectData.get('type', 'Crate'))

        if 'Visual' in objectData and 'Color' in objectData['Visual']:
            container.setContainerColor(objectData['Visual'].get('Color', (1.0, 1.0, 1.0, 1.0)))

        container.setSearchTime(float(objectData.get('searchTime', '6.0')))
        container.setVisZone(objectData.get('VisZone', ''))
        container.setSphereScale(float(objectData.get('Aggro Radius', 1.0)))

        parent.generateChildWithRequired(container, PiratesGlobals.IslandLocalZone)
        self.addObject(container)
        self.broadcastObjectPosition(container)

        return container

    def __createFishingSpot(self, parent, parentUid, objKey, objectData):
        fishingSpot = DistributedFishingSpotAI(self.air)

        self.setObjectTruePosHpr(fishingSpot, objKey, parentUid, objectData)
        fishingSpot.setScale(objectData.get('Scale', (1, 1, 1)))
        fishingSpot.setOceanOffset(float(objectData.get('Ocean Offset', 1)))

        parent.generateChildWithRequired(fishingSpot, PiratesGlobals.IslandLocalZone)
        self.addObject(fishingSpot)
        self.broadcastObjectPosition(fishingSpot)

        return fishingSpot

    def __createPotionTable(self, parent, parentUid, objKey, objectData):
        table = DistributedPotionCraftingTableAI(self.air)

        self.setObjectTruePosHpr(table, objKey, parentUid, objectData)
        table.setScale(objectData.get('Scale', (1, 1, 1)))

        table.setPotionZone(int(objectData.get('Potion Zone', 0)))

        parent.generateChildWithRequired(table, PiratesGlobals.IslandLocalZone)
        self.addObject(table)
        self.broadcastObjectPosition(table)

        return table

    def __createBuildingExterior(self, parent, parentUid, objKey, objectData):
        from pirates.world.DistributedGAInteriorAI import DistributedGAInteriorAI
        from pirates.world.DistributedJailInteriorAI import DistributedJailInteriorAI

        parent = parent.getParentObj()

        if not parent:
            self.notify.warning('Cannot create building exterior %s, current parent %s, has no parent object!' % (
                objKey, parentUid))

            return

        exteriorUid = objectData.get('ExtUid')

        if not exteriorUid:
            self.notify.warning('Cannot create building exterior %s, no exterior uid found!' % (
                objKey))

            return

        interiorFile = objectData.get('File')

        if not interiorFile:
            self.notify.debug('Cannot create building exterior %s, no interior file found!' % (
                objKey))

            return

        modelPath = self.air.worldCreator.getModelPathFromFile(interiorFile)

        if not modelPath:
            self.notify.warning('Cannot create building exterior %s, no model path found for file %s!' % (
                objKey, interiorFile))

            return

        if 'jail' in objectData['Visual']['Model']:
            interior = DistributedJailInteriorAI(self.air)
            self.parent.setJailInterior(interior)
        else:
            interior = DistributedGAInteriorAI(self.air)

        interior.setUniqueId(exteriorUid)
        interior.setName(interiorFile)
        interior.setModelPath(modelPath)
        interior.setScale(objectData.get('Scale', (1, 1, 1)))
        parent.generateChildWithRequired(interior, self.air.allocateZone())

        # add the interior object to the list of objects by uid's,
        # as both the exterior uid and the object key so we can listen
        # in on uid callback events...
        parent.builder.addObject(interior)
        parent.builder.addObject(interior, uniqueId = objKey)

        objectList = objectData.get('Objects', {})

        if not objectList:
            exteriorLocatorNode = self.createDoorLocatorNode(self.parent, objKey, exteriorUid, objectData, wantTruePosHpr = False)
            interiorLocatorNode = interior.builder.createDoorLocatorNode(self.parent, objKey, objKey, objectData)
        else:
            self.air.worldCreator.loadObjectDict(objectList, self.parent, objKey, True)

        self.air.worldCreator.loadObjectsFromFile(interiorFile + '.py', interior)

        return interior

    def createDoorLocatorNode(self, parent, parentUid, objKey, objectData, wantTruePosHpr = True):
        from pirates.world.DistributedBuildingDoorAI import DistributedBuildingDoorAI

        parent = parent.getParentObj()

        if not parent:
            self.notify.warning(f'Cannot create door locator node {objKey}, current parent {parentUid}, has no parent object!')
            return

        interior = self.air.uidMgr.justGetMeMeObject(parentUid)

        if not interior or parentUid == self.parent.getUniqueId():
            self.notify.debug(f'Cannot create door locator node {objKey}, interior not found!')
            return

        doorLocatorNode = DistributedBuildingDoorAI(self.air)
        doorLocatorNode.setUniqueId(objKey)
        doorLocatorNode.setPos(objectData.get('Pos', (0, 0, 0)))
        doorLocatorNode.setHpr(objectData.get('Hpr', (0, 0, 0)))
        doorLocatorNode.setScale(objectData.get('Scale', (1, 1, 1)))
        doorLocatorNode.setBuildingUid(parentUid)
        doorLocatorNode.setInteriorId(interior.doId, interior.getUniqueId(), interior.parentId, interior.zoneId)

        if not interior.getExteriorFrontDoor():
            doorLocatorNode.setDoorIndex(0)
            interior.setExteriorFrontDoor(doorLocatorNode)
        else:
            doorLocatorNode.setDoorIndex(1)
            interior.setExteriorBackDoor(doorLocatorNode)

        if wantTruePosHpr:
            self.setObjectTruePosHpr(doorLocatorNode, objKey, parentUid, objectData)

        self.parent.generateChildWithRequired(doorLocatorNode, PiratesGlobals.IslandLocalZone)
        self.addObject(doorLocatorNode)

        return doorLocatorNode

    def __createDinghy(self, parent, parentUid, objKey, objectData):
        dinghy = DistributedDinghyAI(self.air)

        pos = objectData.get('GridPos') or objectData.get('Pos')
        hpr = objectData.get('Hpr')

        dinghy.setInteractRadius(float(objectData.get('Aggro Radius', 25)))

        dinghy.setPos(pos)
        dinghy.setHpr(hpr)

        parent.generateChildWithRequired(dinghy, PiratesGlobals.IslandLocalZone)
        self.addObject(dinghy)
        #self.broadcastObjectPosition(dinghy)

        return dinghy

    def __createObjectSpawnNode(self, parent, parentUid, objKey, objectData):
        spawnClass = DistributedSurfaceTreasureAI if objectData['Spawnables'] == 'Surface Treasure' else DistributedBuriedTreasureAI

        spawnNode = spawnClass(self.air)

        self.setObjectTruePosHpr(spawnNode, objKey, parentUid, objectData)
        spawnNode.setScale(objectData.get('Scale', (1, 1, 1)))
        spawnNode.setStartingDepth(int(objectData.get('startingDepth', 10)))
        spawnNode.setCurrentDepth(spawnNode.getStartingDepth())
        spawnNode.setVisZone(objectData.get('VisZone', ''))

        parent.generateChildWithRequired(spawnNode, PiratesGlobals.IslandLocalZone)
        self.addObject(spawnNode)
        self.broadcastObjectPosition(spawnNode)

        return spawnNode

    def __createRepairBench(self, parent, parentUid, objKey, objectData):
        bench = DistributedRepairBenchAI(self.air)

        bench.setPos(objectData.get('Pos', (0, 0, 0)))
        bench.setHpr(objectData.get('Hpr', (0, 0, 0)))

        bench.setScale(objectData.get('Scale', (1, 1, 1)))
        bench.setDifficulty(int(objectData.get('difficulty', '0')))

        zoneId = parent.getZoneFromXYZ(bench.getPos())

        parent.generateChildWithRequired(bench, zoneId)
        self.addObject(bench)
        self.broadcastObjectPosition(bench)

        return bench

    def __createIslandArea(self, parent, parentUid, objKey, objectData):
        from pirates.world.DistributedGameAreaAI import DistributedGameAreaAI

        areaFile = objectData.get('File', None)
        if not areaFile:
            self.notify.warning(f'Failed to generate Island Game Area {objKey}; No file defined')
            return

        islandArea = DistributedGameAreaAI(self.air)
        islandArea.setUniqueId(objKey)
        islandArea.setName(islandArea.getLocalizerName())
        islandArea.setModelPath(objectData['Visual']['Model'])

        self.parent.generateChildWithRequired(islandArea, self.air.allocateZone())
        self.addObject(islandArea)
        self.broadcastObjectPosition(islandArea)
        self.air.worldCreator.loadObjectsFromFile(areaFile + '.py', islandArea)

        return islandArea