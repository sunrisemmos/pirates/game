from direct.distributed.DistributedNodeAI import DistributedNodeAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.GridChild import GridChild
from pirates.world.GameAreaBuilderAI import GameAreaBuilderAI
from pirates.piratesbase import PLocalizer
from pirates.pirate.DistributedPlayerPirateAI import DistributedPlayerPirateAI

class DistributedGameAreaAI(DistributedNodeAI, GridChild):
    notify = directNotify.newCategory('DistributedGameAreaAI')

    def __init__(self, air):
        DistributedNodeAI.__init__(self, air)
        GridChild.__init__(self)
        self.modelPath = ''
        self.links = []
        self.uniqueId = ''
        self.name = PLocalizer.Unknown
        self.builder = GameAreaBuilderAI(self.air, self)

    def setJailInterior(self, jailInterior):
        self.jailInterior = jailInterior

    def getJailInterior(self):
        return self.jailInterior

    def handleChildArrive(self, childObj, zoneId: int) -> bool:
        """
        This is called every time a new child enters this game area.
        If the child is a player, then we want to make sure that they
        are added to our grid and have interest on our objects.
        We return true if the child arriving is a player, and false otherwise.
        """

        # Check if the child object is a player.
        if isinstance(childObj, DistributedPlayerPirateAI):
            # If the child isn't already part of our grid, add them to it.
            if not self.isManagedChild(childObj):
                if not childObj.getParent():
                    self.parentObjectToArea(childObj)
                    self.manageChild(childObj)
                    self.air.worldGridManager.handleLocationChanged(self, childObj, zoneId)

            return True

        return False

    def handleChildLeave(self, childObj, zoneId):
        if isinstance(childObj, DistributedPlayerPirateAI):
            self.ignoreChild(childObj)
            childObj.setGridCell(None, 0)
            self.air.worldGridManager.clearAvatarInterest(self, childObj)

    def setModelPath(self, modelPath):
        self.modelPath = modelPath

    def d_setModelPath(self, modelPath):
        self.sendUpdate('setModelPath', [modelPath])

    def b_setModelPath(self, modelPath):
        self.setModelPath(modelPath)
        self.d_setModelPath(modelPath)

    def getModelPath(self):
        return self.modelPath

    def setLinks(self, links):
        self.links = links

    def d_setLinks(self, links):
        self.sendUpdate('setLinks', [links])

    def b_setLinks(self, links):
        self.setLinks(links)
        self.d_setLinks(links)

    def getLinks(self):
        return self.links

    def setUniqueId(self, uniqueId):
        self.uniqueId = uniqueId

    def d_setUniqueId(self, uniqueId):
        self.sendUpdate('setUniqueId', [uniqueId])

    def b_setUniqueId(self, uniqueId):
        self.setUniqueId(uniqueId)
        self.d_setUniqueId(uniqueId)

    def getUniqueId(self):
        return self.uniqueId

    def setName(self, name):
        self.name = name

    def d_setName(self, name):
        self.sendUpdate('setName', [name])

    def b_setName(self, name):
        self.setName(name)
        self.d_setName(name)

    def getName(self):
        return self.name

    def getLocalizerName(self):
        name = self.getName()
        if self.getUniqueId() in PLocalizer.LocationNames:
            name = PLocalizer.LocationNames[self.getUniqueId()]
        return name

    def d_addSpawnTriggers(self, triggerSpheres):
        self.sendUpdate('addSpawnTriggers', [triggerSpheres])

    def spawnNPC(self, spawnPtId, doId):
        pass

    def generateChildWithRequired(self, do, zoneId, optionalFields = []):
        do.generateWithRequiredAndId(self.air.allocateChannel(), self.doId, zoneId, optionalFields)