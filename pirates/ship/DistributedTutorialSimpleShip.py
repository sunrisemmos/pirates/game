# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.ship.DistributedTutorialSimpleShip
from pirates.audio import SoundGlobals
from pirates.piratesbase import PiratesGlobals
from pirates.ship import ShipGlobals
from pirates.ship.DistributedSimpleShip import DistributedSimpleShip

class DistributedTutorialSimpleShip(DistributedSimpleShip):

    def __init__(self, cr):
        DistributedSimpleShip.__init__(self, cr)
        self.interactTube = None
        return

    def announceGenerate(self):
        DistributedSimpleShip.announceGenerate(self)
        self.setupBoardingSphere(bitmask=PiratesGlobals.WallBitmask | PiratesGlobals.SelectBitmask | PiratesGlobals.RadarShipBitmask)
        self.addDeckInterest()

    def localPirateArrived(self, av):
        DistributedSimpleShip.localPirateArrived(self, av)
        if av.isLocal():
            self.gameFSM.stopCurrentMusic()
            self.gameFSM.startCurrentMusic(SoundGlobals.MUSIC_CUBA_COMBAT)

    def localPirateLeft(self, av):
        DistributedSimpleShip.localPirateLeft(self, av)