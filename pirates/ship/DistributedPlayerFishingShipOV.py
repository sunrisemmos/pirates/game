# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.ship.DistributedPlayerFishingShipOV
from direct.directnotify import DirectNotifyGlobal
from direct.distributed import DistributedObjectOV

class DistributedPlayerFishingShipOV(DistributedObjectOV.DistributedObjectOV):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedPlayerFishingShipOV')

    def __init__(self, cr):
        DistributedObjectOV.DistributedObjectOV.__init__(self, cr)