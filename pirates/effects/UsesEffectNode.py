# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.effects.UsesEffectNode
from pandac.PandaModules import *

class UsesEffectNode(NodePath):

    def __init__(self, offset=3.0):
        self.billboardNode = self.attachNewNode('billboardNode')
        self.billboardNode.node().setEffect(BillboardEffect.make(Vec3(0, 0, 1), 0, 1, offset, NodePath(), Point3(0, 0, 0)))
        self.effectNode = self.billboardNode.attachNewNode('effectNode')

    def getEffectParent(self):
        return self.effectNode

    def resetEffectParent(self):
        self.billboardNode.reparentTo(self)

    def delete(self):
        self.effectNode.removeNode()
        self.billboardNode.removeNode()
        del self.effectNode
        del self.billboardNode