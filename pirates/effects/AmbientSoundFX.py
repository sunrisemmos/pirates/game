# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.effects.AmbientSoundFX
from pirates.leveleditor.LevelEntity import LevelEntity
from pandac.PandaModules import *

class AmbientSoundFX(LevelEntity):

    def __init__(self):
        LevelEntity.__init__(self)
        base.cr.timeOfDayManager.registerAmbientSFXNode(self)

    def setProperty(self, propertyName, propertyValue):
        if propertyName == 'None':
            pass
        elif propertyName == 'Range':
            self.range = float(propertyValue)
        else:
            LevelEntity.setProperty(propertyName, propertyValue)

    def cleanUp(self):
        LevelEntity.cleanUp(self)
        if base.cr.timeOfDayManager:
            base.cr.timeOfDayManager.removeAmbientSFXNode(self)