# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.inventory.InventoryUISlotContainer
from direct.gui.DirectGui import *
from direct.interval.IntervalGlobal import *
from pandac.PandaModules import *
from pirates.piratesgui import GuiPanel, PiratesGuiGlobals
from pirates.piratesbase import PiratesGlobals
from pirates.piratesbase import PLocalizer
from otp.otpbase import OTPLocalizer
from pirates.inventory import InventoryUIContainer
from pirates.inventory.InventoryUIGlobals import *

class InventoryUISlotContainer(InventoryUIContainer.InventoryUIContainer):
    ReferenceSlots = True

    def __init__(self, manager, sizeX=1.0, sizeZ=1.0, countX=None, countZ=None, slotList=[]):
        self.slotCount = 0
        InventoryUIContainer.InventoryUIContainer.__init__(self, manager, sizeX, sizeZ, countX, countZ, slotList)

    def manageCells(self, slotList):
        self.slotList = slotList
        for index in range(len(self.slotList)):
            slot = self.slotList[index]
            cell = self.cellList[index]
            self.manager.assignCellSlot(cell, self.slotList[self.slotCount])
            self.slotCount += 1