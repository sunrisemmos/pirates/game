import sys, os, time, string, bz2, random
from direct.showbase.MessengerGlobal import *
from direct.showbase.DirectObject import DirectObject
from direct.showbase.EventManagerGlobal import *
from direct.task.TaskManagerGlobal import *
from direct.task.Task import Task
from direct.task.MiniTask import MiniTask, MiniTaskManager
from direct.directnotify.DirectNotifyGlobal import *
from pandac.PandaModules import *
from otp.launcher.LauncherBase import LauncherBase
from pirates.piratesbase import PLocalizer

class PiratesQuickLauncher(LauncherBase):
    GameName = 'Pirates'
    ForegroundSleepTime = 0.001
    Localizer = PLocalizer

    def __init__(self):
        print('Running: PiratesQuickLauncher')
        LauncherBase.__init__(self)
        self.showPhase = -1
        self.maybeStartGame()
        self.mainLoop()
        return

    def getValue(self, key, default = None):
        return os.environ.get(key, default)

    def setValue(self, key, value):
        os.environ[key] = str(value)

    def getTestServerFlag(self):
        return self.getValue('IS_TEST_SERVER', 0)

    def getGameServer(self):
        return self.getValue('GAME_SERVER', '')

    def getLogFolder(self):
        return 'logs/'

    def getLogFileName(self):
        return 'pirates'

    def getAccountServer(self):
        return

    def getNeedPwForSecretKey(self):
        return 0

    def getParentPasswordSet(self):
        return 0

    def canLeaveFirstIsland(self):
        return self.getPhaseComplete(4)

    def startGame(self):
        self.newTaskManager()
        eventMgr.restart()
        from pirates.piratesbase import PiratesStart