if __debug__:
    from panda3d.core import loadPrcFile, NodePath

    loadPrcFile('config/client.prc')

    loadPrcFile('config/dev.prc')

for dtool in ('children', 'parent', 'name'):
 del NodePath.DtoolClassDict[dtool]

from pirates.launcher.PiratesQuickLauncher import PiratesQuickLauncher
launcher = PiratesQuickLauncher()
launcher.notify.info('Reached end of StartPiratesLauncher.py.')
