from panda3d.core import loadPrcFile, ConfigVariableString
import requests, json

loadPrcFile('../../../config/server.prc')

def main():
    url = 'http://127.0.0.1:8080/jsonrpc'
    secretKey = ConfigVariableString('secret-key').value

    params = {}
    params['secretKey'] = secretKey
    params['action'] = 'systemMessage'
    params['arguments'] = ['Ahoy, mateys!']
    print(params)

    # System message payload.
    payload = {
        'method': 'action',
        'params': params,
        'jsonrpc': '2.0',
        'id': 0
    }

    response = requests.post(url, json = payload).json()
    print(response)

    assert response['result'] == 'Broadcasted system message to shard.'
    assert response['jsonrpc']
    assert response['id'] == 0

if __name__ == '__main__':
    main()