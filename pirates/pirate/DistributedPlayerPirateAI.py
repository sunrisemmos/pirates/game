from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.GridChild import GridChild
from otp.avatar.DistributedPlayerAI import DistributedPlayerAI
from pirates.pirate.HumanDNA import HumanDNA
from pirates.battle.DistributedBattleAvatarAI import DistributedBattleAvatarAI
from pirates.quest.DistributedQuestAvatarAI import DistributedQuestAvatarAI
from pirates.quest.QuestStatus import QuestStatus
from pirates.piratesbase import PiratesGlobals
from pirates.tutorial import TutorialGlobals
from pirates.quest import QuestDB


class DistributedPlayerPirateAI(DistributedPlayerAI, HumanDNA, DistributedBattleAvatarAI, DistributedQuestAvatarAI, GridChild):
    notify = directNotify.newCategory('DistributedPlayerPirateAI')

    def __init__(self, air):
        GridChild.__init__(self)
        DistributedPlayerAI.__init__(self, air)
        HumanDNA.__init__(self)
        DistributedBattleAvatarAI.__init__(self, air)
        DistributedQuestAvatarAI.__init__(self, air)

        self.inventoryId = 0
        self.founder = False
        self.defaultShard = 0
        self.defaultZone = 0
        self.questStatus = None
        self.returnLocation = ''
        self.isNpc = False
        self.jailCellIndex = 0
        self.MAPClothes = []
        self.isGM = False
        self.gmNameTagState = 0
        self.gmNameTagColor = 'whiteGM'
        self.gmNameTagString = ''
        self.zombie = (0, False)
        self.tutorialInstance = None

    def announceGenerate(self):
        DistributedPlayerAI.announceGenerate(self)
        DistributedBattleAvatarAI.announceGenerate(self)
        DistributedQuestAvatarAI.announceGenerate(self)

        # Set the default Pirate speeds.
        # TODO: These values might be wrong:
        self.sendUpdate('setHasteMod', [0.5])
        self.sendUpdate('setStunMod', [0.5])
        self.sendUpdate('setSwiftnessMod', [0.5])

        if not config.GetBool('want-tutorial', False):
            # TEMP
            self.sendUpdate('updateClientTutorialStatus', [PiratesGlobals.TUT_FINISHED])
            self.d_setTutorial(PiratesGlobals.TUT_FINISHED)

    def d_setTutorial(self, state):
        self.sendUpdate('setTutorial', [state])

    def setInventoryId(self, inventoryId):
        self.inventoryId = inventoryId

        if not self.questStatus:
            self.questStatus = QuestStatus(self)

    def d_setInventoryId(self, inventoryId):
        self.sendUpdate('setInventoryId', [inventoryId])

    def b_setInventoryId(self, inventoryId):
        self.setInventoryId(inventoryId)
        self.d_setInventoryId(inventoryId)

    def getInventoryId(self):
        return self.inventoryId

    def getInventory(self):
        return self.air.doId2do.get(self.inventoryId)

    def d_relayTeleportLoc(self, shardId, zoneId, teleportMgrDoId):
        self.sendUpdateToAvatarId(self.doId, 'relayTeleportLoc', [shardId, zoneId, teleportMgrDoId])

    def setFounder(self, founder):
        self.founder = founder

    def d_setFounder(self, founder):
        self.sendUpdate('setFounder', [founder])

    def b_setFounder(self, founder):
        self.setFounder(founder)
        self.d_setFounder(founder)

    def getFounder(self):
        return self.founder

    def setDefaultShard(self, defaultShard):
        self.defaultShard = defaultShard

    def d_setDefaultShard(self, defaultShard):
        self.sendUpdate('setDefaultShard', [defaultShard])

    def b_setDefaultShard(self, defaultShard):
        self.setDefaultShard(defaultShard)
        self.d_setDefaultShard(defaultShard)

    def getDefaultShard(self):
        return self.defaultShard

    def setDefaultZone(self, defaultZone):
        self.defaultZone = defaultZone

    def d_setDefaultZone(self, defaultZone):
        self.sendUpdate('setDefaultZone', [defaultZone])

    def b_setDefaultZone(self, defaultZone):
        self.setDefaultZone(defaultZone)
        self.d_setDefaultZone(defaultZone)

    def getDefaultZone(self):
        return self.defaultZone

    def d_forceTeleportStart(self, instanceName, tzDoId, thDoId, worldGridDoId, tzParent, tzZone):
        self.sendUpdateToAvatarId(self.doId, 'forceTeleportStart', [instanceName, tzDoId, thDoId, worldGridDoId, tzParent, tzZone])

    def setReturnLocation(self, returnLocation):
        self.returnLocation = returnLocation

    def d_setReturnLocation(self, returnLocation):
        self.sendUpdate('setReturnLocation', [returnLocation])

    def b_setReturnLocation(self, returnLocation):
        self.setReturnLocation(returnLocation)
        self.d_setReturnLocation(returnLocation)

    def getReturnLocation(self):
        return self.returnLocation

    def setJailCellIndex(self, jailCellIndex):
        self.jailCellIndex = jailCellIndex

    def d_setJailCellIndex(self, jailCellIndex):
        self.sendUpdate('setJailCellIndex', [jailCellIndex])

    def b_setJailCellIndex(self, jailCellIndex):
        self.setJailCellIndex(jailCellIndex)
        self.d_setJailCellIndex(jailCellIndex)

    def getJailCellIndex(self):
        return self.jailCellIndex

    def giveDefaultQuest(self):
        inventory = self.getInventory()

        if inventory:
            questHistory = self.getQuestHistory()
            questDNA = QuestDB.QuestDict[TutorialGlobals.SKIP_TUTORIAL_QUEST]

            if questDNA.getQuestInt() not in questHistory:
                self.air.questMgr.createQuest(self, questDNA.getQuestId())

    def requestMAPClothes(self, clothes):
        self.notify.debug('Received requestMAPClothes from client.')
        self.notify.debug(clothes)

    def b_setMAPClothes(self, MAPClothes):
        self.d_setMAPClothes(MAPClothes)
        self.setMAPClothes(MAPClothes)

    def d_setMAPClothes(self, MAPClothes):
        self.sendUpdate('setMAPClothes', [MAPClothes])

    def setMAPClothes(self, MAPClothes):
        self.MAPClothes = MAPClothes

    def getMAPClothes(self):
        return self.MAPClothes

    def updateGMNameTag(self, gmNameTagState, gmNameTagColor, gmNameTagString):
        self.gmNameTagState = gmNameTagState
        self.gmNameTagColor = gmNameTagColor
        self.gmNameTagString = gmNameTagString

    def d_updateGMNameTag(self, gmNameTagState, gmNameTagColor, gmNameTagString):
        self.sendUpdate('updateGMNameTag', [gmNameTagState, gmNameTagColor, gmNameTagString])

    def b_updateGMNameTag(self, gmNameTagState, gmNameTagColor, gmNameTagString):
        self.d_updateGMNameTag(gmNameTagState, gmNameTagColor, gmNameTagString)
        self.updateGMNameTag(gmNameTagState, gmNameTagColor, gmNameTagString)

    def setAsGM(self, flag):
        self.isGM = flag

    def d_setAsGM(self, flag):
        self.sendUpdate('setAsGM', [flag])

    def b_setAsGM(self, flag):
        self.d_setAsGM(flag)
        self.setAsGM(flag)

    def setZombie(self, value, cursed = False):
        self.zombie = (value, cursed)

    def d_setZombie(self, value, cursed  = False):
        self.sendUpdate('setZombie', [value, cursed])

    def b_setZombie(self, value, cursed = False):
        self.setZombie(value, cursed)
        self.d_setZombie(value, cursed)

    def getZombie(self):
        return self.zombie

    def delete(self):
        if self.tutorialInstance:
            self.tutorialInstance.requestDelete()

        # Clear all active avatar grid interests.
        self.air.worldGridManager.clearAvatarInterests(self)

        DistributedPlayerAI.delete(self)
        DistributedBattleAvatarAI.delete(self)

    def destroy(self):
        self.d_setGameState('TeleportOut')

        DistributedPlayerAI.destroy(self)
        DistributedBattleAvatarAI.destroy(self)
