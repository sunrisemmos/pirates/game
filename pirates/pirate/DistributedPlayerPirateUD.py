from direct.distributed.DistributedObjectUD import DistributedObjectUD
from direct.directnotify import DirectNotifyGlobal

class DistributedPlayerPirateUD(DistributedObjectUD):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedPlayerPirateUD')

    def __init__(self, air):
        DistributedObjectUD.__init__(self, air)
