# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.pirate.CannonDefenseCamera
from pirates.minigame import CannonDefenseGlobals
from pirates.pirate.CannonCamera import CannonCamera
from direct.showbase.PythonUtil import ParamObj

class CannonDefenseCamera(CannonCamera):

    class ParamSet(CannonCamera.ParamSet):
        Params = {'minH': -60.0, 'maxH': 60.0, 'minP': -32.0, 'maxP': 2.0, 'sensitivityH': CannonDefenseGlobals.MOUSE_SENSITIVITY_H, 'sensitivityP': CannonDefenseGlobals.MOUSE_SENSITIVITY_P}

    def __init__(self, params=None):
        CannonCamera.__init__(self, params)
        self.keyboardRate = CannonDefenseGlobals.KEYBOARD_RATE

    def enterActive(self):
        CannonCamera.enterActive(self)
        camera.setPos(0, -20, 15)
        camera.setP(-25)

    def changeModel(self, prop):
        if self.cannonProp:
            if prop.ship:
                self.reparentTo(prop.ship.avCannonView)
            else:
                self.reparentTo(prop.hNode)
        self.cannonProp = prop