# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.pirate.PlayerPirateGameFSM
from pandac.PandaModules import *
from direct.interval.IntervalGlobal import *
from pirates.destructibles import ShatterableSkeleton
from pirates.pirate.BattleAvatarGameFSM import BattleAvatarGameFSM

class PlayerPirateGameFSM(BattleAvatarGameFSM):

    def __init__(self, av, fsmName='PlayerPirateGameFSM'):
        BattleAvatarGameFSM.__init__(self, av, fsmName)

    def enterDeath(self, extraArgs=[]):
        BattleAvatarGameFSM.enterDeath(self, extraArgs)

    def exitDeath(self):
        BattleAvatarGameFSM.exitDeath(self)