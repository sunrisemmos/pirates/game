from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedPopulationTrackerAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedPopulationTrackerAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

        self.shardId = 0
        self.population = 0
        self.popLimits = [0, 250]

    def setShardId(self, shardId):
        self.shardId = shardId

    def d_setShardId(self, shardId):
        self.sendUpdate('setShardId', [shardId])

    def b_setShardId(self, shardId):
        self.setShardId(shardId)
        self.d_setShardId(shardId)

    def getShardId(self):
        return self.shardId

    def setPopulation(self, population):
        self.population = population

    def d_setPopulation(self, population):
        self.sendUpdate('setPopulation', [population])

    def b_setPopulation(self, population):
        self.setPopulation(population)
        self.d_setPopulation(population)

    def getPopulation(self):
        return self.population

    def setPopLimits(self, minimum, maximum):
        self.popLimits = [minimum, maximum]

    def d_setPopLimits(self, minimum, maximum):
        self.sendUpdate('setPopLimits', [minimum, maximum])

    def b_setPopLimits(self, minimum, maximum):
        self.setPopLimits(minimum, maximum)
        self.d_setPopLimits(minimum, maximum)

    def getPopLimits(self):
        return self.popLimits