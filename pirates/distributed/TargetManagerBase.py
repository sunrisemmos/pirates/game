class TargetManagerBase:

    def __init__(self):
        self.objectDict = {}

    def delete(self):
        del self.objectDict

    def getUniqueId(self, obj):
        return hash(obj)

    def addTarget(self, nodePathId, obj):
        self.objectDict[nodePathId] = obj

    def removeTarget(self, nodePathId):
        if nodePathId in self.objectDict:
            del self.objectDict[nodePathId]

    def getObjectFromNodepath(self, nodePath):
        target = nodePath.getNetPythonTag('MonstrousObject')
        if not target:
            target = self.objectDict.get(hash(nodePath), None)
        return target