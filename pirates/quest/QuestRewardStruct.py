# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.quest.QuestRewardStruct
from direct.showbase.PythonUtil import POD

class QuestRewardStruct(POD):
    DataSet = {'rewardType': None, 'amount': None, 'questId': None}