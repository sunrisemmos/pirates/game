# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.quest.QuestIndicatorNodeNPCOffset
from pirates.quest.QuestIndicatorNodeNPC import QuestIndicatorNodeNPC

class QuestIndicatorNodeNPCOffset(QuestIndicatorNodeNPC):

    def __init__(self, questStep):
        QuestIndicatorNodeNPC.__init__(self, questStep)
        self.wantBottomEffect = False

    def startNearEffect(self):
        QuestIndicatorNodeNPC.startNearEffect(self)
        if self.nearEffect and self.stepObj:
            if hasattr(self.stepObj, 'getQuestIndicatorOffset'):
                self.nearEffect.setPos(self.stepObj.getQuestIndicatorOffset())
            else:
                self.nearEffect.setPos(0, 0, 0)

    def startFarEffect(self):
        QuestIndicatorNodeNPC.startFarEffect(self)
        if self.farEffect and self.stepObj:
            if hasattr(self.stepObj, 'getQuestIndicatorOffset'):
                self.farEffect.setPos(self.stepObj.getQuestIndicatorOffset())
            else:
                self.farEffect.setPos(0, 0, 0)