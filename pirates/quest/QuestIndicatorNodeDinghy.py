# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.quest.QuestIndicatorNodeDinghy
from pirates.quest.QuestIndicatorNode import QuestIndicatorNode
from direct.showbase.PythonUtil import report, StackTrace

class QuestIndicatorNodeDinghy(QuestIndicatorNode):

    def __init__(self, questStep):
        self.nearEffect = None
        QuestIndicatorNode.__init__(self, 'DinghyIndicator', [
         50], questStep)
        return

    def placeInWorld(self):
        originObj = base.cr.doId2do.get(self.questStep.getOriginDoId())
        if originObj:
            posH = self.questStep.getPosH()
            pos, h = posH[:3], posH[3]
            self.reparentTo(originObj)
            self.setPos(*pos)
            self.setHpr(h, 0, 0)

    def loadZoneLevel(self, level):
        QuestIndicatorNode.loadZoneLevel(self, level)
        if level == 0:
            self.stopTargetRefresh()
        elif level == 1:
            self.request('Far')
            self.requestTargetRefresh()

    def unloadZoneLevel(self, level):
        QuestIndicatorNode.unloadZoneLevel(self, level)
        if level == 0:
            self.requestTargetRefresh()
        elif level == 1:
            self.stopTargetRefresh()
            self.request('Off')