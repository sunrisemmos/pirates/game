# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.quest.QuestHolderBase

class QuestHolderBase:

    def __init__(self):
        self._rewardCollectors = {}

    def getQuests(self):
        raise 'derived must implement'

    def _addQuestRewardCollector(self, collector):
        cId = collector._serialNum
        self._rewardCollectors[cId] = collector

    def _removeQuestRewardCollector(self, collector):
        cId = collector._serialNum
        del self._rewardCollectors[cId]

    def _trackRewards(self, trade):
        for collector in self._rewardCollectors.values():
            collector.collect(trade)