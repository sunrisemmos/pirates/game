from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectAI import DistributedObjectAI

from pirates.quest.QuestAvatarBase import QuestAvatarBase
from pirates.quest.QuestHolder import QuestHolder

class DistributedQuestAvatarAI(DistributedObjectAI, QuestAvatarBase, QuestHolder):
    notify = directNotify.newCategory('DistributedQuestAvatarAI')

    def __init__(self, air):
        self.air = air
        self.activeQuest = ''
        self.questHistory = []
        self.questLadderHistory = []
        self.currentQuestChoiceContainers = []

    def setActiveQuest(self, activeQuest):
        self.activeQuest = activeQuest

    def d_setActiveQuest(self, activeQuest):
        self.sendUpdate('setActiveQuest', [activeQuest])

    def b_setActiveQuest(self, activeQuest):
        self.setActiveQuest(activeQuest)
        self.d_setActiveQuest(activeQuest)

    def getActiveQuest(self):
        return self.activeQuest

    def setQuestHistory(self, questHistory):
        self.questHistory = questHistory

    def d_setQuestHistory(self, questHistory):
        self.sendUpdate('setQuestHistory', [questHistory])

    def b_setQuestHistory(self, questHistory):
        self.setQuestHistory(questHistory)
        self.d_setQuestHistory(questHistory)

    def getQuestHistory(self):
        return self.questHistory

    def setQuestLadderHistory(self, questLadderHistory):
        self.questLadderHistory = questLadderHistory

    def d_setQuestLadderHistory(self, questLadderHistory):
        self.sendUpdate('setQuestLadderHistory', [questLadderHistory])

    def b_setQuestLadderHistory(self, questLadderHistory):
        self.setQuestLadderHistory(questLadderHistory)
        self.d_setQuestLadderHistory(questLadderHistory)

    def getQuestLadderHistory(self):
        return self.questLadderHistory

    def setCurrentQuestChoiceContainers(self, currentQuestChoiceContainers):
        self.currentQuestChoiceContainers = currentQuestChoiceContainers

    def d_setCurrentQuestChoiceContainers(self, currentQuestChoiceContainers):
        self.sendUpdate('setCurrentQuestChoiceContainers', [currentQuestChoiceContainers])

    def b_setCurrentQuestChoiceContainers(self, currentQuestChoiceContainers):
        self.setCurrentQuestChoiceContainers(currentQuestChoiceContainers)
        self.d_setCurrentQuestChoiceContainers(currentQuestChoiceContainers)

    def getCurrentQuestChoiceContainers(self):
        return self.currentQuestChoiceContainers

    def requestActiveQuest(self, activeQuest):
        inventory = self.getInventory()

        if not inventory:
            self.notify.warning(f'Failed to set active quest for avatar {self.doId}, no inventory found!')
            return

        if not self.air.questMgr.hasQuest(self, questId = activeQuest):
            self.notify.debug(f'Failed to set active quest {activeQuest} for avatar {self.doId}, quest not found in the avatar\'s questList!')
            return

        self.b_setActiveQuest(activeQuest)

    def requestDropQuest(self, questId):
        inventory = self.getInventory()

        if not inventory:
            self.notify.warning(f'Failed to drop quest for avatar {self.doId}, no inventory found!')
            return

        activeQuest = self.air.questMgr.getQuest(self, questId = questId)

        if not activeQuest:
            self.notify.debug(f'Failed to drop active quest {activeQuest} for avatar {self.doId}, quest not found in the avatar\'s questList!')
            return

        if not activeQuest.isDroppable():
            self.notify.debug(f'Failed to drop active quest {activeQuest} for avatar {self.doId}, quest is not droppable!')
            return

        self.air.questMgr.dropQuest(self, activeQuest)

    def requestQuestStep(self, questId):
        avatarId = self.air.getAvatarIdFromSender()

        inventory = self.getInventory()

        if not inventory:
            self.notify.warning(f'Failed to get quest step for avatar {self.doId}, no inventory found!')
            return

        activeQuest = self.air.questMgr.getQuest(self, questId = questId)

        if not activeQuest:
            self.notify.debug(f'Failed to get step for quest {activeQuest} for avatar {self.doId}, quest not found in the avatar\'s questList!')
            return

        if self.activeQuest != questId:
            return

        activeTask = self.air.questMgr.getActiveTask(activeQuest)

        if not activeTask:
            return

        # attempt to get the quest step object in which,
        # the ray of light will hover over...
        goalUid = activeTask.getGoalUid()
        stepType, goalObject = self.air.questMgr.getQuestStep(self, goalUid)

        if not stepType or not goalObject:
            self.sendUpdateToAvatarId(avatarId, 'setQuestStep', [[self.doId, 0, [0, [0, 0, 0, 0], '', '']]])
            return

        (x, y, z), (h, p, r) = goalObject.getPos(), goalObject.getHpr()
        self.sendUpdateToAvatarId(avatarId, 'setQuestStep', [[self.doId, goalObject.doId, [stepType, [x, y, z, h], goalUid, goalUid]]])

    def _swapQuest(self, oldQuests = [], giverId = None, questIds = None, rewards = None):
        inventory = self.getInventory()

        if not inventory:
            self.notify.debug(f'Failed to accept quest(s) {questIds} for avatar {self.doId}, no inventory found!')
            return

        # drop all of our old quests
        for quest in oldQuests:
            assert(quest is not None)
            messenger.send(quest.getCompleteEventString())
            self.air.questMgr.dropQuest(self, quest)

        def _questsCreatedCallback():
            pass

        # give the avatar all of it's new quests that come after the
        # previous quests they've just completed
        self.air.questMgr.createQuests(self, questIds, _questsCreatedCallback)

    def _acceptQuest(self, nextQuestId, giverId, rewards):
        inventory = self.getInventory()

        if not inventory:
            self.notify.debug(f'Failed to accept quest {nextQuestId} for avatar {self.doId}, no inventory found!')
            return

        def questCreatedCallback():
            messenger.send(f'quest-available-{nextQuestId}-{self.doId}', [self, inventory.getQuestList()])

        self.air.questMgr.createQuest(self, nextQuestId, questCreatedCallback)

    def d_popupProgressBlocker(self, questId):
        self.sendUpdateToAvatarId(self.doId, 'popupProgressBlocker', [questId])
