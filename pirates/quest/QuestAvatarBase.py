# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.quest.QuestAvatarBase
from direct.directnotify.DirectNotifyGlobal import directNotify

class QuestAvatarBase:
    notify = directNotify.newCategory('QuestAvatarBase')

    def __init__(self):
        self.questNPCInterest = {}

    def getQuests(self):
        inventory = self.getInventory()
        if inventory is None:
            err = 'could not get inventory'
            if hasattr(self, 'doId'):
                err += ' for %s' % self.doId
            return []
        else:
            return inventory.getQuestList()
        return

    def getQuestById(self, questId):
        quests = self.getQuests()
        for currQuest in quests:
            if currQuest.questId == questId:
                return currQuest

        return

    def addQuestNPCInterest(self, npcId, questId):
        self.questNPCInterest[npcId] = questId
        messenger.send('questInterestChange-%s' % npcId, [])

    def removeQuestNPCInterest(self, npcId):
        self.questNPCInterest.pop(npcId, None)
        messenger.send('questInterestChange-%s' % npcId, [])
        return

    def hasQuestNPCInterest(self, npcId):
        return self.questNPCInterest.get(npcId)