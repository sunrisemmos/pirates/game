# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.trades.TradeManager
from direct.distributed.ClockDelta import *
from direct.directnotify import DirectNotifyGlobal
from direct.distributed.DistributedObject import DistributedObject
from pirates.uberdog.UberDogGlobals import *

class TradeManager(DistributedObject):
    notify = DirectNotifyGlobal.directNotify.newCategory('TradeManager')

    def __init__(self, cr):
        DistributedObject.__init__(self, cr)
        self.cr.tradeManager = self

    def delete(self):
        self.ignoreAll()
        if self.cr.tradeManager == self:
            self.cr.tradeManager = None
        DistributedObject.delete(self)
        return

    def sendRequestCreateTrade(self, otherAvatarId):
        self.sendUpdate('requestCreateTrade', [otherAvatarId])

    def rejectCreateTrade(self, otherAvatarId, reasonCode):
        messenger.send('rejectCreateTrade-%s' % (otherAvatarId,), [reasonCode])

    def createTradeResponse(self, otherAvatarId, tradeId):
        self.addInterest(tradeId)