from otp.friends.GuildManagerUD import GuildManagerUD
from direct.directnotify.DirectNotifyGlobal import directNotify

class PCGuildManagerUD(GuildManagerUD):
    notify = directNotify.newCategory('PCGuildManagerUD')

    def sendSCQuest(self, questInt, msgType, taskNum):
        sender = self.air.getAvatarIdFromSender()

        guildId = self.getGuildIdByAvId(sender)
        if not guildId:
            # This is suspicious. You can't chat if you aren't in the guild.
            self.air.writeServerEvent('suspicious', sender, 'Player tried to sendSCQuest when not in guild!')
            return

        # Send the update to the guild channel.
        self.sendUpdateToChannel(guildId, 'recvSCQuest', [sender, questInt, msgType, taskNum])