from direct.distributed.DistributedSmoothNodeAI import DistributedSmoothNodeAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.distributed.DistributedTargetableObjectAI import DistributedTargetableObjectAI
from pirates.battle import EnemyGlobals

class DistributedMovingObjectAI(DistributedSmoothNodeAI, DistributedTargetableObjectAI):
    notify = directNotify.newCategory('DistributedMovingObjectAI')

    def __init__(self, air):
        DistributedSmoothNodeAI.__init__(self, air)
        DistributedTargetableObjectAI.__init__(self, air)

        self.maxAISpeed = 0
        self.startState = 'Idle'
        self.aggroMode = EnemyGlobals.AGGRO_MODE_FORCED
        self.aggroRadius = 0

    def generate(self):
        DistributedSmoothNodeAI.generate(self)
        DistributedTargetableObjectAI.generate(self)

    def setMaxSpeed(self, speed):
        self.maxAISpeed = speed

    def d_setMaxSpeed(self, speed):
        self.sendUpdate('setMaxSpeed', [speed])

    def b_setMaxSpeed(self, speed):
        self.setMaxSpeed(speed)
        self.d_setMaxSpeed(speed)

    def getMaxSpeed(self):
        return self.maxAISpeed

    def setStartState(self, startState):
        self.startState = startState

    def d_setStartState(self, startState):
        self.sendUpdate('setStartState', [startState])

    def b_setStartState(self, startState):
        self.setStartState(startState)
        self.d_setStartState(startState)

    def getStartState(self):
        return self.startState

    def setAggroRadius(self, aggroRadius):
        self.aggroRadius = aggroRadius

    def d_setAggroRadius(self, aggroRadius):
        self.sendUpdate('setAggroRadius', [aggroRadius])

    def b_setAggroRadius(self, aggroRadius):
        self.setAggroRadius(aggroRadius)
        self.d_setAggroRadius(aggroRadius)

    def getAggroRadius(self):
        return self.aggroRadius

    def setAggroMode(self, aggroMode):
        self.aggroMode = aggroMode

    def d_setAggroMode(self, aggroMode):
        self.sendUpdate('setAggroMode', [aggroMode])

    def b_setAggroMode(self, aggroMode):
        self.setAggroMode(aggroMode)
        self.d_setAggroMode(aggroMode)

    def getAggroMode(self):
        return self.aggroMode