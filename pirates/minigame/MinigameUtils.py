# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.minigame.MinigameUtils

def getAcuteAngle(angle1, angle2):
    angle1 = getNormalizedAngle(angle1)
    angle2 = getNormalizedAngle(angle2)
    angleDifference = (angle1 - angle2) % 360
    if angleDifference == 0:
        return 0
    if abs(angleDifference) > 180:
        sign = angleDifference / abs(angleDifference)
        angleDifference = (angleDifference - 360) * sign
    return angleDifference

def getNormalizedAngle(angle):
    angle %= 360
    if angle < 0:
        angle = (360 + angle) % 360
    return angle