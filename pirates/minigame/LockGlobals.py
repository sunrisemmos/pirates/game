# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.minigame.LockGlobals
from pirates.piratesbase import PLocalizer
LSTATE_ACTIVE = 1
LSTATE_TRY = 2
LSTATE_FAIL = 3
LSTATE_OPEN = 4
LSTATE_RESET = 5
LSTATE_DONE = 6
LGUI_EXIT = -1
LGUI_MECHLEFT = 1
LGUI_MECHRIGHT = 2
LGUI_TRYLOCK = 3
StartZPos = -0.2
StartXPos = 0.0
StartSpeed = 2.2
MaxTool = 12
LayerXPos = -2.1
LayerZPos = 1.3
LockXPos = 0.0
LockZPos = 0.5
NewXPos = 1.9
NewZPos = 1.3