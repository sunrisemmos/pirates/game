from pirates.minigame.DistributedPokerTableAI import DistributedPokerTableAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedHoldemTableAI(DistributedPokerTableAI):
    notify = directNotify.newCategory('DistributedHoldemTableAI')