# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.minigame.DistributedCannonDefenseEntrance
from pandac.PandaModules import *
from direct.distributed.DistributedObject import DistributedObject
from direct.directnotify import DirectNotifyGlobal
from direct.gui.OnscreenText import OnscreenText
from direct.interval.IntervalGlobal import *
from direct.distributed.GridChild import GridChild
from pirates.piratesbase import PiratesGlobals
from pirates.piratesbase import PLocalizer
from pirates.piratesgui import PiratesGuiGlobals
from pirates.minigame import CannonDefenseGlobals

class DistributedCannonDefenseEntrance(DistributedObject, GridChild):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedInteractive')

    def __init__(self, cr):
        DistributedObject.__init__(self, cr)
        GridChild.__init__(self)
        self._gameFullTxt = None
        self._gameFullSeq = None
        return

    def generate(self):
        DistributedObject.generate(self)

    def announceGenerate(self):
        DistributedObject.announceGenerate(self)

    def teleport(self):
        base.loadingScreen.showTarget(cannonDefense=True)