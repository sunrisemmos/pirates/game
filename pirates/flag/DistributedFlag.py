# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.flag.DistributedFlag
from pandac.PandaModules import *
from direct.distributed.DistributedObject import DistributedObject
from . import FlagGlobals
from .Flag import Flag

class DistributedFlag(DistributedObject, Flag):
    notify = directNotify.newCategory('DistributedFlag')

    def __init__(self, cr):
        DistributedObject.__init__(self, cr)
        Flag.__init__(self, 'flag')

    def setDNAString(self, dnaStr):
        self.notify.debug('setDNAString: ' + repr(dnaStr))
        Flag.setDNAString(self, dnaStr)
        self.flatten()

    def d_requestDNAString(self, dnaStr):
        self.notify.debug('d_requestDNAString: ' + repr(dnaStr))
        self.sendUpdate('requestDNAString', [dnaStr])

    def announceGenerate(self):
        DistributedObject.announceGenerate(self)
        self.notify.debug('generated')

    def disable(self):
        self.detachNode()
        DistributedObject.disable(self)