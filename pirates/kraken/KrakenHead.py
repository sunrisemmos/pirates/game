# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.kraken.KrakenHead
from pirates.creature.DistributedCreature import DistributedCreature

class bp:
    kraken = bpdb.bpPreset(cfg='kraken', static=1)

class KrakenHead(DistributedCreature):

    def __init__(self, cr):
        DistributedCreature.__init__(self, cr)
        self.krakenId = 0

    def setupCreature(self, avatarType):
        DistributedCreature.setupCreature(self, avatarType)

    def announceGenerate(self):
        DistributedCreature.announceGenerate(self)
        getBase().khead = self

    def delete(self):
        if getBase().khead == self:
            getBase().khead = None
        DistributedCreature.delete(self)
        return

    def setKrakenId(self, krackenId):
        self.krakenId = krackenId

    def getKrakenId(self):
        return self.krakenId

    def getKraken(self):
        return self.cr.getDo(self.krakenId)