# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.kraken.KrakenGameFSM
from direct.directnotify import DirectNotifyGlobal
from direct.fsm.FSM import FSM

class bp:
    kraken = bpdb.bpPreset(cfg='krakenfsm', static=1)
    krakenCall = bpdb.bpPreset(cfg='krakenfsm', call=1, static=1)

class KrakenGameFSM(FSM):
    notify = DirectNotifyGlobal.directNotify.newCategory('KrakenGameFSM')

    def __init__(self, av):
        FSM.__init__(self, 'KrakenGameFSM')
        self.av = av

    @bp.krakenCall()
    def enterRam(self):
        pass

    @bp.krakenCall()
    def exitRam(self):
        pass

    @bp.krakenCall()
    def enterGrab(self):
        self.av.emergeInterval.pause()
        self.av.submergeInterval.start()

    @bp.krakenCall()
    def exitGrab(self):
        pass