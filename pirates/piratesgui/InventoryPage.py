# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.piratesgui.InventoryPage
from direct.gui.DirectGui import *
from pandac.PandaModules import *
from pirates.piratesgui import PiratesGuiGlobals
from pirates.uberdog import InventoryRequestBase

class InventoryPage(DirectFrame, InventoryRequestBase.InventoryRequestBase):

    def __init__(self):
        DirectFrame.__init__(self, parent=NodePath(), relief=None, state=DGG.DISABLED, frameColor=PiratesGuiGlobals.FrameColor, borderWidth=PiratesGuiGlobals.BorderWidth, frameSize=(0.0, PiratesGuiGlobals.InventoryPageWidth, 0.0, PiratesGuiGlobals.InventoryPageHeight), pos=(-0.54, 0, -0.72))
        InventoryRequestBase.InventoryRequestBase.__init__(self)
        self.initialiseoptions(InventoryPage)
        return

    def show(self):
        DirectFrame.show(self)

    def hide(self):
        DirectFrame.hide(self)

    def slideOpenCallback(self):
        pass

    def slideCloseCallback(self):
        pass

    def slideOpenPrecall(self):
        pass