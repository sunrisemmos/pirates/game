# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.piratesgui.RadarObjDef
RADAR_OBJ_TYPE_DEFAULT = 0
RADAR_OBJ_TYPE_LANDMARK = 1
RADAR_OBJ_TYPE_QUEST = 2
RADAR_OBJ_TYPE_SHIP = 3
RADAR_OBJ_TYPE_EXIT = 4
RADAR_OBJ_TYPE_TUTORIAL = 5
RADAR_OBJ_EXIT_OFFSET_Z = -0.15