# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.piratesgui.ScoreFrame
from pirates.piratesgui.SheetFrame import SheetFrame

class ScoreFrame(SheetFrame):

    def __init__(self, w, h, holder, team, **kw):
        self.team = team
        title = holder.getTeamName(self.team)
        SheetFrame.__init__(self, w, h, title, holder, **kw)
        self.initialiseoptions(ScoreFrame)
        self.scoreChanged = False

    def getItemList(self):
        return self.holder.getItemList(self.team)

    def _handleItemChange(self):
        self.scoreChanged = True

    def show(self):
        if self.scoreChanged:
            self.scoreChanged = False
            self.redraw()
        SheetFrame.show(self)