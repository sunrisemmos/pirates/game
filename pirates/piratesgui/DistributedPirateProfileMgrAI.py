from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.pirate.HumanDNA import HumanDNA
from pirates.piratesbase import PLocalizer
from pirates.piratesgui import PiratesGuiGlobals
from pirates.reputation import ReputationGlobals
from pirates.uberdog.UberDogGlobals import InventoryType
from pirates.tutorial import TutorialGlobals

class DistributedPirateProfileMgrAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedPirateProfileMgrAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

    def getRequiredAccumulators(self):
        return [InventoryType.OverallRep, InventoryType.CannonRep, InventoryType.SailingRep, InventoryType.CutlassRep,
                InventoryType.PistolRep, InventoryType.DollRep, InventoryType.DaggerRep, InventoryType.GrenadeRep,
                InventoryType.WandRep, InventoryType.PotionsRep, InventoryType.FishingRep]

    def getLevelFromAccumulator(self, accumulators, accumulator):
        return ReputationGlobals.getLevelFromTotalReputation(accumulator, accumulators.get(accumulator, 0))[0]

    def getLevelsFromAccumulators(self, accumulators):
        return [self.getLevelFromAccumulator(accumulators, accumulator) for accumulator in self.getRequiredAccumulators()]

    def requestAvatar(self, avatarId, doId):
        avatar = self.air.doId2do.get(doId)

        def fieldsCallback(dclass, fields):
            if dclass != self.air.dclassesByName['DistributedPlayerPirateAI']:
                return

            humanDNA = HumanDNA()

            for fieldName, fieldValue in list(fields.items()):
                if hasattr(humanDNA, fieldName):
                    getattr(humanDNA, fieldName)(*fieldValue)

            guildId = fields.get('setGuildId', [0])[0]
            guildName = fields.get('setGuildName', [''])[0]
            founder = fields.get('setFounder', [''])[0]
            hp = fields['setHp'][0]
            maxHp = fields['setMaxHp'][0]
            voodoo = fields['setMojo'][0]
            maxVoodoo = fields['setMaxMojo'][0]
            shardId = fields['setDefaultShard'][0]
            inventoryId = fields['setInventoryId'][0]
            disableButtons = False
            showGoTo = False if avatarId == doId else True

            def handleRetrieveInventory(dclass, fields):
                accumulators = dict(fields['setAccumulators'][0])
                levels = self.getLevelsFromAccumulators(accumulators)

                # Send our skill levels to our avatar.
                self.sendUpdateToAvatarId(doId, 'receiveAvatarSkillLevels', levels)

            self.air.dbInterface.queryObject(self.air.dbId, inventoryId, handleRetrieveInventory)

            returnLoc = avatar.getReturnLocation()

            if returnLoc:
                islandName = PLocalizer.LocationNames[returnLoc]
            else:
                islandName = TutorialGlobals.RAMBLESHACK_ISLE_UID

            siege = False
            profileIcon = PiratesGuiGlobals.PROFILE_ICON_LAND
            shardName = self.air.distributedDistrict.getName()

            # Send our online info to our avatar.
            self.sendUpdateToAvatarId(doId, 'receiveAvatarOnlineInfo', [islandName, shardName, siege, profileIcon])

            # Send our avatar info to our avatar.
            self.sendUpdateToAvatarId(doId, 'receiveAvatarInfo', [humanDNA, guildId, guildName, founder, hp, maxHp, voodoo, maxVoodoo, shardId, disableButtons, showGoTo])

        self.air.dbInterface.queryObject(self.air.dbId, doId, fieldsCallback)