# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.audio.PiratesAmbientManager
from direct.directnotify import DirectNotifyGlobal
from pirates.audio import AmbientManagerBase
from pirates.audio import SoundGlobals

class PiratesAmbientManager(AmbientManagerBase.AmbientManagerBase):
    notify = DirectNotifyGlobal.directNotify.newCategory('PiratesAmbientManager')

    def __init__(self):
        AmbientManagerBase.AmbientManagerBase.__init__(self)
        self.volumeModifierDict = {SoundGlobals.AMBIENT_JUNGLE: 0.6, SoundGlobals.AMBIENT_SWAMP: 0.75, SoundGlobals.AMBIENT_JAIL: 1.25}

    def requestFadeIn(self, name, duration=5, finalVolume=1.0, priority=0):
        if name and name not in self.ambientDict:
            self.load(name, 'audio/' + name)
        AmbientManagerBase.AmbientManagerBase.requestFadeIn(self, name, duration, finalVolume, priority)

    def requestChangeVolume(self, name, duration, finalVolume, priority=0):
        newFinalVolume = finalVolume
        if name in list(self.volumeModifierDict.keys()):
            newFinalVolume = finalVolume * self.volumeModifierDict[name]
        AmbientManagerBase.AmbientManagerBase.requestChangeVolume(self, name, duration, newFinalVolume, priority)