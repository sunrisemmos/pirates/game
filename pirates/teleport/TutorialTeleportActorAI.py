from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.teleport.DistributedTeleportActorAI import DistributedTeleportActorAI


class TutorialTeleportActorAI(DistributedTeleportActorAI):
    notify = directNotify.newCategory('TutorialTeleportActorAI')

    def __init__(self, air):
        DistributedTeleportActorAI.__init__(self, air)

    def requestFSMState(self, contextId: int, requestData):
        print('requestFSMState', contextId, requestData)

        requestState = requestData[0][0]

        if requestState == 'InQuietZone':
            self.sendOpenShard(contextId)
        elif requestState == 'ShardOpen':
            self.sendOpenWorld(contextId)
        elif requestState == 'WorldOpen':
            self.sendOpenGame(self.air.getAvatarIdFromSender(), contextId)
        elif requestState == 'GameOpen':
            self.sendStartShow(contextId)
