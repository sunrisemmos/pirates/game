from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedFSMAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedFSMAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)