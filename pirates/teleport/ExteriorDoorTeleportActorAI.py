from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.teleport.DoorTeleportActorAI import DoorTeleportActorAI

class ExteriorDoorTeleportActorAI(DoorTeleportActorAI):
    notify = directNotify.newCategory('ExteriorDoorTeleportActorAI')

    def __init__(self, air):
        DoorTeleportActorAI.__init__(self, air)

        self.exteriorInfo = []
        self.doorId = 0

    def requestFSMState(self, contextId: int, requestData):
        avatarId = self.air.getAvatarIdFromSender()

        requestState = requestData[0][0]

        if requestState == 'InQuietZone':
            self.sendOpenShard(contextId)
        elif requestState == 'ShardOpen':
            self.sendOpenWorld(contextId)
        elif requestState == 'WorldOpen':
            self.sendOpenGame(avatarId, contextId)
        elif requestState == 'GameOpen':
            self.sendStartShow(contextId)
        elif requestState == 'ShowComplete':
            self.sendOpenWorld(contextId)

    def sendOpenGame(self, avatarId: int, contextId: int):
        spawnPt = self.instance.getSpawnPt(self.area.getUniqueId())
        xPos, yPos, zPos, h = spawnPt

        # Set the instance spawn info.
        self.instance.d_setSpawnInfo(avatarId, xPos, yPos, zPos, h, 0, [self.area.doId, self.area.parentId, self.area.zoneId])

        # Send our response.
        self.sendUpdateToAvatarId(avatarId, 'setFSMState', [contextId, [['OpenGame', self.area.doId, self.exteriorInfo[2], self.doorId]]])
