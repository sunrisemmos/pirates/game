from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.teleport.DistributedTeleportActorAI import DistributedTeleportActorAI

class InstanceTeleportActorAI(DistributedTeleportActorAI):
    notify = directNotify.newCategory('InstanceTeleportActorAI')

    def __init__(self, air):
        DistributedTeleportActorAI.__init__(self, air)

    def requestFSMState(self, requestContext: int, requestData):
        avatarId = self.air.getAvatarIdFromSender()

        requestState = requestData[0][0]

        if requestState == 'InQuietZone':
            self.sendOpenShard(requestContext)
        elif requestState == 'ShardOpen':
            self.sendOpenWorld(requestContext)
        elif requestState == 'WorldOpen':
            self.sendOpenGame(avatarId, requestContext)
        elif requestState == 'GameOpen':
            self.sendStartShow(requestContext)
        elif requestState == 'ShowComplete':
            self.sendOpenWorld(requestContext)
