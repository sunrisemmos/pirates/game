from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.teleport.AreaTeleportActorAI import AreaTeleportActorAI

class DoorTeleportActorAI(AreaTeleportActorAI):
    notify = directNotify.newCategory('DoorTeleportActorAI')

    def __init__(self, air):
        AreaTeleportActorAI.__init__(self, air)