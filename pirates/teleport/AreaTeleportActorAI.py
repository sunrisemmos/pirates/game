from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.teleport.DistributedTeleportActorAI import DistributedTeleportActorAI

class AreaTeleportActorAI(DistributedTeleportActorAI):
    notify = directNotify.newCategory('AreaTeleportActorAI')

    def __init__(self, air):
        DistributedTeleportActorAI.__init__(self, air)