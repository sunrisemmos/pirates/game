from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.teleport.AreaTeleportActorAI import AreaTeleportActorAI

class PlayerAreaTeleportActorAI(AreaTeleportActorAI):
    notify = directNotify.newCategory('PlayerAreaTeleportActorAI')

    def __init__(self, air):
        AreaTeleportActorAI.__init__(self, air)