from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.teleport.DistributedFSMAI import DistributedFSMAI
from pirates.tutorial import TutorialGlobals


class DistributedTeleportActorAI(DistributedFSMAI):
    notify = directNotify.newCategory('DistributedTeleportActorAI')

    def __init__(self, air):
        DistributedFSMAI.__init__(self, air)

        self.instance = None
        self.area = None
        self.tutorialState = 0
        self.isInTutorial = False

    def setGoToArea(self, area):
        self.area = area

    def setInstance(self, instance):
        self.instance = instance

    def setTutorialState(self, state):
        self.tutorialState = state
        self.isInTutorial = True

        if self.tutorialState == 1:
            self.area = self.air.uidMgr.getDo(TutorialGlobals.RAMBLESHACK_ISLE_UID)

    def sendGotoQuietZone(self):
        self.sendUpdate('setFSMState', [0, [['GoToQuietZone', self.air.districtId]]])

    def sendOpenShard(self, contextId: int):
        self.sendUpdate('setFSMState', [contextId, [['OpenShard']]])

    def sendCompleteShow(self):
        self.sendUpdate('setFSMState', [0, [['CompleteShow', True]]])

    def sendOpenWorld(self, contextId: int):
        self.sendUpdate('setFSMState', [contextId, [['OpenWorld', [[self.area.parentId, self.area.zoneId]], self.instance.doId]]])

    def sendOpenGame(self, avatarId: int, contextId: int):
        avatar = self.air.doId2do.get(avatarId)

        spawnPt = self.instance.getSpawnPt(self.area.getUniqueId())

        if self.isInTutorial and self.tutorialState == 0:
            xPos, yPos, zPos, h = 25.955, 19.1, -1.5, -175.005
        else:
            xPos, yPos, zPos, h = spawnPt

        # Set the instance spawn info.
        self.instance.d_setSpawnInfo(avatarId, xPos, yPos, zPos, h, 0, [self.area.doId, self.area.parentId, self.area.zoneId])

        if self.isInTutorial:
            # We are in the Tutorial Jail interior.
            # This needs to be handled slightly different.
            self.sendUpdate('setFSMState', [contextId, [['OpenGame', avatar.tutorialInstance.doId, self.area.doId, (xPos, yPos, zPos, h, 0, 0)]]])
        else:
            self.sendUpdate('setFSMState', [contextId, [['OpenGame', self.area.doId, (xPos, yPos, zPos, h, 0, 0)]]])

    def sendStartShow(self, contextId: int):
        self.sendUpdate('setFSMState', [contextId, [['StartShow']]])

        # Delete ourselves.
        self.requestDelete()
