from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.teleport.DoorTeleportActorAI import DoorTeleportActorAI

class InteriorDoorTeleportActorAI(DoorTeleportActorAI):
    notify = directNotify.newCategory('InteriorDoorTeleportActorAI')

    def __init__(self, air):
        DoorTeleportActorAI.__init__(self, air)

        self.interiorInfo = []
        self.doorId = 0

    def requestFSMState(self, requestContext: int, requestData):
        avatarId = self.air.getAvatarIdFromSender()

        requestState = requestData[0][0]

        if requestState == 'InQuietZone':
            self.sendOpenShard(requestContext)
        elif requestState == 'ShardOpen':
            self.sendOpenWorld(requestContext)
        elif requestState == 'WorldOpen':
            self.sendOpenGame(requestContext)
        elif requestState == 'GameOpen':
            self.sendStartShow(requestContext)
        elif requestState == 'ShowComplete':
            self.sendOpenWorld(requestContext)

    def sendOpenWorld(self, requestContext: int):
        self.sendUpdate('setFSMState', [requestContext, [['CloseGame']]])

        instance = self.air.doId2do.get(self.interiorInfo[2])
        self.sendUpdate('setFSMState', [requestContext, [['OpenWorld', [[self.interiorInfo[2], self.interiorInfo[3]]], instance.doId]]])

    def sendOpenGame(self, requestContext: int):
        self.sendUpdate('setFSMState', [requestContext, [['OpenGame', self.interiorInfo[0], self.doorId]]])
