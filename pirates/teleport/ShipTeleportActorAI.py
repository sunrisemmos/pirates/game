from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.teleport.DistributedTeleportActorAI import DistributedTeleportActorAI

class ShipTeleportActorAI(DistributedTeleportActorAI):
    notify = directNotify.newCategory('ShipTeleportActorAI')

    def __init__(self, air):
        DistributedTeleportActorAI.__init__(self, air)