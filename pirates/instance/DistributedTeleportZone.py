# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.instance.DistributedTeleportZone
from pirates.instance import DistributedInstanceBase
from pandac.PandaModules import NodePath

class DistributedTeleportZone(DistributedInstanceBase.DistributedInstanceBase, NodePath):

    def __init__(self, cr):
        DistributedInstanceBase.DistributedInstanceBase.__init__(self, cr)

    def getInstanceNodePath(self):
        return self