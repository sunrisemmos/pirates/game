from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals
from pirates.piratesbase.UniqueIdManager import UniqueIdManager
from pirates.world.AreaBuilderBaseAI import AreaBuilderBaseAI
import random


class DistributedInstanceBaseAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedInstanceBaseAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

        self.uniqueId = ''
        self.name = ''
        self.fileName = ''
        self.instanceType = PiratesGlobals.INSTANCE_NONE
        self.spawnPts = {}

        self.parentInstance = None
        self.subInstances = []

        self.canBePrivate = False

        self.uidMgr = UniqueIdManager(self.air, self)
        self.builder = AreaBuilderBaseAI(self.air, self)

    def getParentingRules(self):
        return ['', '']

    def setParentInstance(self, parentInstance):
        self.parentInstance = parentInstance

    def getParentInstance(self):
        return self.parentInstance

    def createSubInstance(self):
        from pirates.instance.DistributedInstanceWorldAI import DistributedInstanceWorldAI

        subInstance = DistributedInstanceWorldAI(self.air)
        subInstance.instanceType = PiratesGlobals.INSTANCE_MAINSUB
        subInstance.setParentInstance(self)
        subInstance.generateWithRequiredAndId(self.air.allocateChannel(), self.doId, self.air.allocateZone())

        self.subInstances.append(subInstance)
        return subInstance

    def getSubInstances(self):
        return self.subInstances

    def clearSubInstances(self):
        for instance in self.subInstances:
            if instance is not None:
                instance.requestDelete()
        self.subInstances = []

    def setUniqueId(self, uniqueId):
        self.uniqueId = uniqueId

    def getUniqueId(self):
        return self.uniqueId

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setFileName(self, fileName):
        self.fileName = fileName

    def d_setFileName(self, fileName):
        self.sendUpdate('setFileName', [fileName])

    def b_setFileName(self, fileName):
        self.setFileName(fileName)
        self.d_setFileName(fileName)

    def getFileName(self):
        return self.fileName

    def uid2doSearch(self, uid):
        objDoId = self.uidMgr.getDoId(uid)
        obj = self.air.doId2do.get(objDoId)
        objInstance = None
        if obj:
            objInstance = obj.getInstance()
        return (objDoId, objInstance)

    def setType(self, instanceType):
        self.instanceType = instanceType

    def d_setType(self, instanceType):
        self.sendUpdate('setType', [instanceType])

    def b_setType(self, instanceType):
        self.setType(instanceType)
        self.d_setType(instanceType)

    def getType(self):
        return self.instanceType

    def d_setSpawnInfo(self, avatarId, xPos, yPos, zPos, h, spawnZone, parents):
        self.sendUpdateToAvatarId(avatarId, 'setSpawnInfo', [xPos, yPos, zPos, h, spawnZone, parents])

    def addSpawnPt(self, area, spawnPt):
        self.spawnPts[area] = self.spawnPts.setdefault(area, [])
        self.spawnPts[area].append(spawnPt)

    def removeSpawnPt(self, area, spawnPt):
        if area not in self.spawnPts:
            return

        del self.spawnPts[area]

    def getSpawnPt(self, area, index = None):
        if area not in self.spawnPts:
            return (0, 0, 0, 0)

        return self.spawnPts[area][index] if index else random.choice(self.spawnPts[area])

    def avatarDied(self):
        pass

    def setCanBePrivate(self, canBePrivate):
        self.canBePrivate = canBePrivate

    def getCanBePrivate(self):
        return self.canBePrivate

    def generateChildWithRequired(self, do, zoneId, optionalFields = []):
        do.generateWithRequiredAndId(self.air.allocateChannel(), self.doId, zoneId, optionalFields)

    def d_sendAutoInterest(self, avatarId, zoneList):
        self.sendUpdateToAvatarId(avatarId, 'AutoInterest', zoneList)
