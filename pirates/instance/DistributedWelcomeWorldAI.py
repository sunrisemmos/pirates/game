from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.instance.DistributedInstanceWorldAI import DistributedInstanceWorldAI
from pirates.piratesbase import PiratesGlobals


class DistributedWelcomeWorldAI(DistributedInstanceWorldAI):
    notify = directNotify.newCategory('DistributedWelcomeWorldAI')

    def __init__(self, air):
        DistributedInstanceWorldAI.__init__(self, air)

        self.instanceType = PiratesGlobals.INSTANCE_WELCOME
