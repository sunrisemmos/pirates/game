
from pirates.instance.DistributedInstanceBaseAI import DistributedInstanceBaseAI
from direct.directnotify import DirectNotifyGlobal

class DistributedTeleportZoneUD(DistributedInstanceBaseAI):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedTeleportZoneUD')

    def __init__(self, air):
        DistributedInstanceBaseAI.__init__(self, air)

