from pirates.instance.DistributedInstanceWorldAI import DistributedInstanceWorldAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals


class DistributedMainWorldAI(DistributedInstanceWorldAI):
    notify = directNotify.newCategory('DistributedMainWorldAI')

    def __init__(self, air):
        DistributedInstanceWorldAI.__init__(self, air)

        self.instanceType = PiratesGlobals.INSTANCE_MAIN
