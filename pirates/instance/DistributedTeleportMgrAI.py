from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals
from pirates.teleport.ExteriorDoorTeleportActorAI import ExteriorDoorTeleportActorAI
from pirates.world.LocationConstants import LocationIds
from pirates.instance.DistributedInstanceBaseAI import DistributedInstanceBaseAI
from pirates.world.DistributedGameAreaAI import DistributedGameAreaAI
from otp.distributed import OtpDoGlobals
from pirates.teleport.InstanceTeleportActorAI import InstanceTeleportActorAI
from pirates.teleport.TutorialTeleportActorAI import TutorialTeleportActorAI
from pirates.tutorial import TutorialGlobals
from pirates.world import WorldGlobals
from pirates.tutorial.DistributedPiratesTutorialAI import DistributedPiratesTutorialAI


class DistributedTeleportMgrAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedTeleportMgrAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

    def announceGenerate(self):
        DistributedObjectAI.announceGenerate(self)

        # Set up our messenger events
        self.air.netMessenger.accept('teleportOnAI', self, self.initiateTeleport)

    def getWorld(self, instanceType, instanceName):
        for doObj in list(self.air.doId2do.values()):

            if not doObj or not isinstance(doObj, DistributedInstanceBaseAI):
                continue

            if doObj.getType() == instanceType and doObj.getFileName() == instanceName:
                return doObj

        return None

    def requestInstanceTeleport(self, instanceType, instanceName):
        avatar = self.air.doId2do.get(self.air.getAvatarIdFromSender())

        if not avatar:
            self.notify.debug(f'Cannot teleport non-existant avatar to instance {instanceType} {instanceName}!')
            return

        self.doInitiateTeleport(avatar, instanceType = instanceType, instanceName = instanceName)

    def requestIslandTeleport(self, islandUid):
        avatar = self.air.doId2do.get(self.air.getAvatarIdFromSender())

        if not avatar:
            self.notify.debug(f'Cannot teleport non-existant avatar to island {islandUid}!')
            return

        self.doInitiateTeleport(avatar, islandUid = islandUid)

    def doInitiateTeleport(self, avatar, instanceType = None, instanceName = None, islandUid = LocationIds.PORT_ROYAL_ISLAND, spawnPt = None):
        instance = avatar.getParentObj()

        if instance and instance.getUniqueId() == islandUid:
            self.notify.debug(f'Cannot initiate teleport for {avatar.doId}, already there!')
            self.d_failTeleportRequest(avatar.doId, PiratesGlobals.TFSameArea)
            return

        if instanceType is None and instanceName is None:
            world = instance.getParentObj() if instance else None
        else:
            world = self.getWorld(instanceType, instanceName)

        if not world or not isinstance(world, DistributedInstanceBaseAI):
            self.notify.debug(f'Cannot initiate teleport for {avatar.doId}, unknown world {instanceType} {instanceName}!')
            return

        instance = self.air.uidMgr.getDo(islandUid)

        if not instance or not isinstance(instance, DistributedGameAreaAI):
            self.notify.debug(f'Cannot initiate teleport for {avatar.doId}, unknown instance {islandUid}!')
            return

        if not spawnPt:
            spawnPt = world.getSpawnPt(instance.getUniqueId())

        self.createLoginTeleport(avatar.doId, False, True, instance)

    def createTeleport(self, exteriorInfo: list, doorId: int, avatarId: int):
        actor = ExteriorDoorTeleportActorAI(self.air)

        actor.exteriorInfo = exteriorInfo
        actor.doorId = doorId

        actor.setTutorialState(1) # Outside

        instance = self.air.doId2do.get(exteriorInfo[1])
        actor.setInstance(instance)

        # Generate our teleport actor.
        actor.generateWithRequired(OtpDoGlobals.OTP_ZONE_ID_OLD_QUIET_ZONE)

        # Generate our OwnerView instance.
        self.air.sendSetOwnerDoId(actor.doId, avatarId)

        # Start the teleportation process.
        taskMgr.doMethodLater(3.5, actor.sendCompleteShow, 'beginTeleportProcess', extraArgs = [])

    def createLoginTeleport(self, avatarId, isTutorial = False, nonLogin = False, instance = None):
        avatar = self.air.doId2do.get(avatarId)

        if isTutorial:
            self.air.worldCreator.loadObjectsFromFile(WorldGlobals.PiratesTutorialSceneFile)

            instanceActor = TutorialTeleportActorAI(self.air)
            instanceActor.isInTutorial = True

            # Setup the tutorial instance for this avatar.
            avatar.tutorialInstance = DistributedPiratesTutorialAI(self.air)
            avatar.tutorialInstance.generateWithRequired(OtpDoGlobals.OTP_ZONE_ID_MANAGEMENT)
        else:
            instanceActor = InstanceTeleportActorAI(self.air)

        # Generate our teleport actor.
        instanceActor.generateWithRequired(OtpDoGlobals.OTP_ZONE_ID_OLD_QUIET_ZONE)

        # Generate our OwnerView instance.
        self.air.sendSetOwnerDoId(instanceActor.doId, avatarId)

        # Grab our return location.
        retLoc = avatar.getReturnLocation()

        if not retLoc:
            # This must be our first time going in-game.
            if isTutorial:
                retLoc = TutorialGlobals.JAIL_INTERIOR
            else:
                retLoc = LocationIds.PORT_ROYAL_ISLAND

        # If we have a instance already, we are probably teleporting to another Island.
        if not instance:
            # We do not have a instance already.
            instance = self.air.uidMgr.getDo(retLoc)

        instanceActor.setGoToArea(instance)
        instanceActor.setInstance(instance.getParentObj())

        if not nonLogin:
            instanceActor.sendGotoQuietZone()
        else:
            instanceActor.sendCompleteShow()

        # taskMgr.doMethodLater(3.5, instanceActor.sendGotoQuietZone if not nonLogin else instanceActor.sendCompleteShow, 'beginTeleportProcess', extraArgs = [])

    def d_failTeleportRequest(self, avatarId, reasonBit):
        self.sendUpdateToAvatarId(avatarId, 'failTeleportRequest', [reasonBit.getHighestOnBit()])

    def initiateTeleport(self, avatarId, isTutorial):
        self.notify.debug(f'initiateTeleport - {avatarId} - {isTutorial}')

        def avatarGenerated(avatar):
            if not avatar:
                self.notify.warning('Cannot initiate teleport for non-existant avatar!')
                return

            self.createLoginTeleport(avatarId, isTutorial)

        self.acceptOnce(f'generate-{avatarId}', avatarGenerated)
        self.air.setAIReceiver(avatarId)
