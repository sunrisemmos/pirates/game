from panda3d.core import ConfigVariableString
from pypresence import Presence
from direct.directnotify.DirectNotifyGlobal import directNotify
from pirates.piratesbase import PiratesGlobals
import time

class PiratesDiscordPresence:
    notify = directNotify.newCategory('PiratesDiscordPresence')
    notify.setInfo(True)

    def __init__(self):
        self.clientId = ConfigVariableString('discord-client-id', '').getValue()

        self.rpcClient = None
        self.startTime = time.time()

        self.baseData = {
            'start': self.startTime,
            'large_image': '512x512'
        }

        base.hasDiscordOpen = self.initialize()

        self.currentLocation = ''
        self.currentShard = ''
        self.lastLocImage = ''

    def setLocation(self, locationUid, locationName):
        self.notify.debug(f'setLocation() - {locationUid} - {locationName}')

        # Store some data for usage later.
        self.currentLocation = locationName
        self.currentShard = base.cr.getShardName(base.localAvatar.defaultShard)
        self.lastLocImage = PiratesGlobals.getDiscordImage(locationUid)

        self.startLocationPresence()

    def startLocationPresence(self):
        # Construct our presence data.
        data = {
            'details': f'{base.localAvatar.getName()} ({base.localAvatar.getHp()} / {base.localAvatar.getMaxHp()})',
            'state': f'{self.currentLocation} ({self.currentShard})',
            'large_image': self.lastLocImage
        }

        # Send it off to the RPC client.
        self.update(data)

    def initialize(self) -> bool:
        self.notify.info('Trying to connect...')

        try:
            self.rpcClient = Presence(self.clientId)
            self.rpcClient.connect()

            self.notify.info('Connected.')
            return True
        except:
            self.notify.warning('Failed to connect!')
            return False

        # Send our base data.
        self.update(self.baseData)

    def update(self, additionalData):
        if not base.wantDiscordPresence or not base.hasDiscordOpen:
            # We don't want to continue.
            return

        # We will start by making a copy of the base data.
        data = self.baseData

        # Next, we will extend the base data dict with the additional data.
        data.update(additionalData)

        # TODO: Discuss potential brand merge with Fade.
        buttons = [
            {
                'label': 'Website', 'url': 'https://sunrise.games'
            },
            {
                'label': 'Discord', 'url': 'https://discord.gg/3M5SwEybWW'
            }
        ]

        # Send the update to the RPC client.
        self.notify.debug(f'Updating RPC with data: {data}')
        self.rpcClient.update(**data, buttons = buttons)

    def shutOff(self):
        if not base.wantDiscordPresence or not base.hasDiscordOpen:
            # We don't want to continue.
            return

        self.notify.info('Shutting down...')

        self.rpcClient.clear()