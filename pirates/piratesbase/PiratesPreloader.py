# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.piratesbase.PiratesPreloader
from direct.directnotify.DirectNotifyGlobal import giveNotify
from . import PiratesGlobals

class PiratesPreloader(object):

    def __init__(self):
        self.baseLoadCounter = 0
        self.preloadPool = []
        self.doLoad()

    def doLoad(self):
        if self.baseLoadCounter >= len(PiratesGlobals.preLoadSet):
            return
        loader.loadModel(PiratesGlobals.preLoadSet[self.baseLoadCounter], callback=self.callback)

    def callback(self, model):
        self.preloadPool.append(model)
        self.baseLoadCounter += 1
        self.doLoad()

giveNotify(PiratesPreloader)