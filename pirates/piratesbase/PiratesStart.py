from . import PiratesPreloader
print('PiratesStart: Starting the game.')
import builtins

class game:
    name = 'pirates'
    process = 'client'

builtins.game = game()
import sys, builtins, gc
gc.disable()
try:
    launcher
except:
    print('Creating PiratesDummyLauncher')
    from pirates.launcher.PiratesDummyLauncher import PiratesDummyLauncher
    launcher = PiratesDummyLauncher()
    builtins.launcher = launcher

from direct.gui import DirectGuiGlobals
from . import PiratesGlobals
DirectGuiGlobals.setDefaultFontFunc(PiratesGlobals.getInterfaceFont)
launcher.setPandaErrorCode(7)
from pandac.PandaModules import *
from . import PiratesBase
PiratesBase.PiratesBase()
if ConfigVariableBool('want-preloader', 0):
    base.preloader = PiratesPreloader.PiratesPreloader()
if base.win == None:
    print('Unable to open window; aborting.')
    sys.exit()
launcher.setPandaErrorCode(0)
launcher.setPandaWindowOpen()
base.sfxPlayer.setCutoffDistance(500.0)
from pirates.audio import SoundGlobals
from pirates.audio.SoundGlobals import loadSfx
rolloverSound = loadSfx(SoundGlobals.SFX_GUI_ROLLOVER_01)
rolloverSound.setVolume(0.5)
DirectGuiGlobals.setDefaultRolloverSound(rolloverSound)
clickSound = loadSfx(SoundGlobals.SFX_GUI_CLICK_01)
DirectGuiGlobals.setDefaultClickSound(clickSound)
clearColor = Vec4(0.0, 0.0, 0.0, 1.0)
base.win.setClearColor(clearColor)
from pirates.shader.Hdr import *
hdr = Hdr()
from pirates.seapatch.Reflection import Reflection
Reflection.initialize(render)
serverVersion = ConfigVariableString('server-version', 'no_version_set').value
print('serverVersion: ', serverVersion)
from pirates.distributed import PiratesClientRepository
cr = PiratesClientRepository.PiratesClientRepository(serverVersion, launcher)
base.initNametagGlobals()
base.startShow(cr)
from pirates.piratesbase import UserFunnel
UserFunnel.logSubmit(1, 'CLIENT_OPENS')
UserFunnel.logSubmit(0, 'CLIENT_OPENS')
if ConfigVariableBool('want-portal-cull', 0):
    base.cam.node().setCullCenter(base.camera)
    base.graphicsEngine.setPortalCull(1)
if base.options:
    base.options.options_to_config()
    base.options.setRuntimeOptions()
    if launcher.isDummy() and not Thread.isTrueThreads():
        base.run()
    elif __name__ == '__main__':
        base.run()