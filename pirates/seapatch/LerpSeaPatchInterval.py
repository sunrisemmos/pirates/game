# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.seapatch.LerpSeaPatchInterval
from libpirates import CLerpSeaPatchInterval

class LerpSeaPatchInterval(CLerpSeaPatchInterval):
    lerpNum = 1

    def __init__(self, name, duration, blendType, patch, initial, target):
        if name == None:
            name = 'LerpSeaPatchInterval-%d' % self.lerpNum
            LerpSeaPatchInterval.lerpNum += 1
        blendType = self.stringBlendType(blendType)
        if target == None:
            target = SeaPatchRoot()
        CLerpSeaPatchInterval.__init__(self, name, duration, blendType, patch, initial, target)
        return