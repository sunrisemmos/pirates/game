# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.shipparts.WheelInteractive
from pirates.interact.SimpleInteractive import SimpleInteractive
from pirates.piratesbase import PLocalizer

class WheelInteractive(SimpleInteractive):

    def __init__(self, ship):
        self.ship = ship
        wheel = ship.model.locators.find('**/location_wheel')
        if not wheel:
            wheel = ship.model.root.attachNewNode('dummyWheel')
        SimpleInteractive.__init__(self, wheel, 'wheel-%s' % ship.doId, PLocalizer.InteractWheel)

    def interactionAllowed(self, avId):
        return self.ship.canTakeWheel(avId)

    def requestInteraction(self, avId):
        self.ship.requestPilot(avId)