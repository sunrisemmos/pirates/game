from direct.distributed.DistributedObjectGlobalUD import DistributedObjectGlobalUD
from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.MsgTypes import *
from direct.distributed.PyDatagram import PyDatagram
from direct.fsm.FSM import FSM

from otp.distributed import OtpDoGlobals
from otp.ai import AIMsgTypes

from pirates.uberdog import InventoryInit

from pirates.ai.DatabaseObject import DatabaseObject

from pirates.ai.PiratesAIMsgTypes import DATABASE_OBJECT_TYPE_INVENTORY

class InventoryOperationFSM(FSM):
    notify = directNotify.newCategory('InventoryOperationFSM')

    def __init__(self, air, avatarId, callback=None):
        FSM.__init__(self, self.__class__.__name__)
        self.air = air
        self.avatarId = avatarId
        self.callback = callback

    def getAvatarClassName(self):
        return 'DistributedPlayerPirateUD'

    def getInventoryClassName(self):
        return 'PirateInventoryUD'

    def enterOff(self):
        pass

    def exitOff(Self):
        pass

    def enterStart(self):
        pass

    def exitStart(self):
        pass

    def cleanup(self, *args, **kwargs):
        del self.air.inventoryManager.avatar2fsm[self.avatarId]
        self.ignoreAll()
        self.demand('Off')

        if self.callback:
            self.callback(*args, **kwargs)

class CreateInventoryFSM(InventoryOperationFSM):
    notify = directNotify.newCategory('CreateInventoryFSM')

    def enterStart(self):
        categoryLimits = []
        for key, limit in list(InventoryInit.CategoryLimits.items()):
            categoryLimits.append((key, limit))

        accumulators = []
        for key, limit in list(InventoryInit.AccumulatorLimits.items()):
            accumulators.append((key, 0))

        stackLimits = []
        for key, limit in list(InventoryInit.StackLimits.items()):
            stackLimits.append((key, limit))

        startStacks = []
        for key, amount in list(InventoryInit.StackStartsWith.items()):
            startStacks.append((key, amount))

        fields = {
            'setOwnerId': (self.avatarId,),
            'setInventoryVersion': (InventoryInit.UberDogRevision,),
            'setCategoryLimits': (categoryLimits,),
            'setDoIds': ([],),
            'setAccumulators': (accumulators,),
            'setStackLimits': (stackLimits,),
            'setStacks': (startStacks,)
        }

        doneEvent = self.air.uniqueName(f'invCreation-{self.avatarId}')
        self.acceptOnce(doneEvent, self.inventoryCreatedCallback)

        db = DatabaseObject(self.air)
        db.dclass = self.air.dclassesByName[self.getInventoryClassName()]
        db.doneEvent = doneEvent
        db.createObject(DATABASE_OBJECT_TYPE_INVENTORY, fields)

    def inventoryCreatedCallback(self, dbo, retCode):
        self.inventoryId = dbo.doId
        if retCode != 0:
            self.notify.warning('Failed to create inventory for avatar %d, '
                'inventory database creation failed!' % (self.avatarId))

            self.cleanup(0)
            return

        fields = {
            'setInventoryId': (self.inventoryId,),
        }

        db = DatabaseObject(self.air, self.avatarId)
        db.dclass = self.air.dclassesByName[self.getAvatarClassName()]
        db.setFields(fields)

        self.cleanup(self.inventoryId)

    def exitStart(self):
        pass

class CreateShipInventoryFSM(CreateInventoryFSM):

    def getAvatarClassName(self):
        return 'PlayerShipUD'

    def getInventoryClassName(self):
        return 'DistributedInventoryUD'

class ActivateInventoryFSM(InventoryOperationFSM):
    notify = directNotify.newCategory('ActivateInventoryFSM')

    def enterStart(self, inventoryId):
        if not inventoryId:
            self.notify.warning('Failed to activate inventory for avatar %d, '
                'no inventory found!' % self.avatarId)

            self.cleanup(0)
            return

        self.air.sendActivate(inventoryId,
            self.avatarId,
            OtpDoGlobals.OTP_ZONE_ID_MANAGEMENT,
            self.air.dclassesByName[self.getInventoryClassName()])

        """
        self.air.requestDatabaseGenerate(
            classId=self.air.dclassesByName[self.getInventoryClassName()].getNumber(),
            context=self.air.allocateChannel(),
            parentId=0,
            zoneId=OtpDoGlobals.OTP_ZONE_ID_MANAGEMENT,
            ownerChannel=0,
            ownerAvId=self.avatarId,
            databaseId=AIMsgTypes.DBSERVER_ID,
            values={}
        )
        """

        self.cleanup(inventoryId)

    def exitStart(self):
        pass

class ActivateShipInventoryFSM(ActivateInventoryFSM):

    def getAvatarClassName(self):
        return 'PlayerShipUD'

    def getInventoryClassName(self):
        return 'DistributedInventoryUD'

    def enterStart(self, parentId, inventoryId):
        if not parentId:
            self.notify.warning('Failed to activate inventory for avatar %d, '
                'invalid parent id specified' % self.avatarId)

            self.cleanup(0)
            return

        if not inventoryId:
            self.notify.warning('Failed to activate inventory for avatar %d, '
                'no inventory found!' % self.avatarId)

            self.cleanup(0)
            return

        self.air.sendActivate(inventoryId,
            parentId,
            OtpDoGlobals.OTP_ZONE_ID_MANAGEMENT,
            dclass=self.air.dclassesByName[self.getInventoryClassName()])

        self.cleanup(inventoryId)

    def exitStart(self):
        pass

class DistributedInventoryManagerUD(DistributedObjectGlobalUD):
    notify = directNotify.newCategory('DistributedInventoryManagerUD')

    def __init__(self, air):
        DistributedObjectGlobalUD.__init__(self, air)

        self.avatar2fsm = {}

    def announceGenerate(self):
        DistributedObjectGlobalUD.announceGenerate(self)

        self.air.netMessenger.accept('hasInventoryResponse', self, self.processCallbackResponse)
        self.air.netMessenger.accept('getInventoryResponse', self, self.processCallbackResponse)

    def hasInventory(self, inventoryId, callback):
        self.air.netMessenger.send('hasInventory', [inventoryId, callback])

    def addInventory(self, inventory):
        if inventory and inventory.doId:
            self.air.netMessenger.send('addInventory', [inventory])

    def removeInventory(self, inventory):
        if inventory and inventory.doId:
            self.air.netMessenger.send('removeInventory', [inventory])

    def getInventory(self, avatarId, callback):
        self.air.netMessenger.send('getInventory', [avatarId, callback])

    def runInventoryFSM(self, fsmtype, avatarId, *args, **kwargs):
        if avatarId in self.avatar2fsm:
            self.notify.debug('Failed to run inventory FSM for avatar %d, '
                'an inventory FSM is already running!' % avatarId)

            return

        callback = kwargs.pop('callback', None)

        self.avatar2fsm[avatarId] = fsmtype(self.air, avatarId, callback)
        self.avatar2fsm[avatarId].request('Start', *args, **kwargs)

    def avatarOnline(self, inventoryId, avatarType):
        # avatarType is not used.
        self.runInventoryFSM(ActivateInventoryFSM, self.air.getAvatarIdFromSender(), inventoryId, callback=None)

    def createInventory(self, avatarId, callback):
        self.runInventoryFSM(CreateInventoryFSM, avatarId, callback=callback)

    def queryShipInventory(self, shipId, callback=None):
        self.runInventoryFSM(QueryShipInventoryFSM, shipId, callback=callback)

    def createShipInventory(self, shipId, callback=None):
        self.runInventoryFSM(CreateShipInventoryFSM, shipId, callback=callback)

    def activateShipInventory(self, avatarId, shipId, inventoryId, callback=None):
        self.runInventoryFSM(ActivateShipInventoryFSM, shipId, avatarId, inventoryId, callback=callback)

    def processCallbackResponse(self, callback, *args, **kwargs):
        if callback and callable(callback):
            callback(*args, **kwargs)
            return

        self.notify.warning('No valid callback for a callback response!'
            'What was the purpose of that?')
