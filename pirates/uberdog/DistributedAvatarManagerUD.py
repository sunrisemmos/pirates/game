from panda3d.core import ConfigVariableBool

from otp.uberdog.OtpAvatarManagerUD import OtpAvatarManagerUD
from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.pirate.HumanDNA import HumanDNA
from pirates.makeapirate.PCPickANamePattern import PCPickANamePattern
from pirates.piratesbase import PLocalizer, PiratesGlobals
from pirates.ai.DatabaseObject import DatabaseObject
from pirates.ai.PiratesAIMsgTypes import DATABASE_OBJECT_TYPE_ACCOUNT, DATABASE_OBJECT_TYPE_PIRATE

from otp.otpbase.OTPGlobals import AvatarNumSlots

from otp.uberdog.OtpAvatarManagerUD import AvatarOperationFSM

import random

panp = PCPickANamePattern('', 'm')

NAME_PATTERN = 0
NAME_TYPED_INVALID = 1
NAME_TYPED_VALID = 2

def judgeName(name, gender):
    if not name:
        return NAME_TYPED_INVALID

    pattern = panp._compute(name, gender)

    if pattern:
        return NAME_PATTERN
    else:
        return NAME_TYPED_VALID

class CreateAvatarFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('CreateAvatarFSM')

    def enterStart(self, subId):
        self.demand('RetrieveAccount')
        self.subId = subId

    def enterRetrieveAccount(self):
        # Query the account:
        gotAccEvent = self.manager.air.uniqueName(f'gotAcc-{self.target}')
        self.acceptOnce(gotAccEvent, self.__handleRetrieve)

        db = DatabaseObject(self.manager.air, self.target)
        db.doneEvent = gotAccEvent
        db.dclass = self.manager.air.dclassesByName['AccountUD']
        db.getFields(['ACCOUNT_AV_SET'])

    def __handleRetrieve(self, db, retCode):
        if retCode != 0:
            self.demand('Kill', 'Your account object was not found in the database!')
            return

        self.account = db.values

        self.avList = self.account['ACCOUNT_AV_SET']

        # Sanitize:
        self.avList = self.avList[:AvatarNumSlots]
        self.avList += [0] * (AvatarNumSlots - len(self.avList))

        # Make sure there is an open space:
        self.index = 0
        emptySlot = False

        for slot in self.avList:
            if slot != 0:
                self.index = self.index + 1
                continue
            else:
                emptySlot = True
                break

        if emptySlot == False:
            self.demand('Kill', 'There are no open slots to create an avatar!')
            return

        # Okay, there's space. Let's create the avatar!
        self.demand('CreateAvatar')

    def enterCreateAvatar(self):
        pirateFields = {
            'setName': (random.choice(PLocalizer.TempNameList),),
            'setDISLid': (self.target,)
        }

        if ConfigVariableBool('skip-tutorial', False).getValue():
            pirateFields['setTutorial'] = (PiratesGlobals.TUT_FINISHED,)

        doneEvent = self.manager.air.uniqueName(f'avCreation-{self.target}')
        self.acceptOnce(doneEvent, self.__handleCreate)

        db = DatabaseObject(self.manager.air)
        db.dclass = self.manager.air.dclassesByName['DistributedPlayerPirateUD']
        db.doneEvent = doneEvent
        db.createObject(DATABASE_OBJECT_TYPE_PIRATE, pirateFields)

    def __handleCreate(self, dbo, retCode):
        if retCode != 0:
            self.demand('Kill', 'Database failed to create the new avatar object!')
            return

        self.avId = dbo.doId

        def __onInvCreate(inventoryId):
            self.demand('StoreAvatar')

        self.manager.air.inventoryManager.createInventory(self.avId, __onInvCreate)

    def enterStoreAvatar(self):
        # Associate the avatar with the account...
        self.avList[self.index] = self.avId

        db = DatabaseObject(self.manager.air, self.target)
        db.dclass = self.manager.air.dclassesByName['AccountUD']
        db.setFields({
            'ACCOUNT_AV_SET': self.avList
        })

        # TODO: Fields that need to be handled
        self.access = 0
        self.founder = True

        self.manager.sendUpdateToAccountId(self.target, 'createAvatarResponse', [self.avId, self.subId, self.access, self.founder])

        self.demand('Off')

class PopulateAvatarFSM(AvatarOperationFSM):
    notify = directNotify.newCategory('PopulateAvatarFSM')
    POST_ACCOUNT_STATE = 'HandleFields'

    def enterStart(self, avId, avDNA, usePattern, nicknameIndex, firstIndex, prefixIndex, suffixIndex):
        self.avId = avId
        self.avDNA = avDNA
        self.usePattern = usePattern
        self.nicknameIndex = nicknameIndex
        self.firstIndex = firstIndex
        self.prefixIndex = prefixIndex
        self.suffixIndex = suffixIndex
        self.gender = self.getAvatarGender()

        self.initNameLists()

        self.demand('RetrieveAccount')

    def retrieveDNAFields(self):
        dclass = self.manager.air.dclassesByName['HumanDNA']
        fields = dclass.getNumFields()
        fieldNames = []

        for fieldNum in range(fields):
            field = dclass.getField(fieldNum)

            if field.asMolecularField():
                continue

            fieldNames.append(field.getName())

        return fieldNames

    def getAvatarGender(self):
        fieldNames = self.retrieveDNAFields()

        for name in fieldNames:
            attr = getattr(self.avDNA, 'g' + name[1:], None)

            if not attr:
                continue

            data = attr()

            if name == 'setGender':
                gender = data
                return gender

    def initNameLists(self):
        buf = ['        ', '        ']
        self.nicknamesMale = PLocalizer.PirateNames_NickNamesGeneric + PLocalizer.PirateNames_NickNamesMale
        self.nicknamesFemale = PLocalizer.PirateNames_NickNamesGeneric + PLocalizer.PirateNames_NickNamesFemale
        self.firstNamesMale = PLocalizer.PirateNames_FirstNamesGeneric + PLocalizer.PirateNames_FirstNamesMale
        self.firstNamesFemale = PLocalizer.PirateNames_FirstNamesGeneric + PLocalizer.PirateNames_FirstNamesFemale
        self.lastPrefixesMale = PLocalizer.PirateNames_LastNamePrefixesGeneric + PLocalizer.PirateNames_LastNamePrefixesCapped + PLocalizer.PirateNames_LastNamePrefixesMale
        self.lastPrefixesFemale = PLocalizer.PirateNames_LastNamePrefixesGeneric + PLocalizer.PirateNames_LastNamePrefixesCapped + PLocalizer.PirateNames_LastNamePrefixesFemale
        self.lastSuffixesMale = PLocalizer.PirateNames_LastNameSuffixesGeneric + PLocalizer.PirateNames_LastNameSuffixesMale
        self.lastSuffixesFemale = PLocalizer.PirateNames_LastNameSuffixesGeneric + PLocalizer.PirateNames_LastNameSuffixesFemale
        self.nicknamesMale.sort()
        self.nicknamesFemale.sort()
        self.firstNamesMale.sort()
        self.firstNamesFemale.sort()
        self.lastPrefixesMale.sort()
        self.lastPrefixesFemale.sort()
        self.lastSuffixesMale.sort()
        self.lastSuffixesFemale.sort()
        self.nicknamesMale = buf + self.nicknamesMale + buf
        self.nicknamesFemale = buf + self.nicknamesFemale + buf
        self.firstNamesMale = buf + self.firstNamesMale + buf
        self.firstNamesFemale = buf + self.firstNamesFemale + buf
        self.lastPrefixesMale = buf + self.lastPrefixesMale + buf
        self.lastPrefixesFemale = buf + self.lastPrefixesFemale + buf
        self.lastSuffixesMale = buf + self.lastSuffixesMale + buf
        self.lastSuffixesFemale = buf + self.lastSuffixesFemale + buf

    def generateName(self):
        name = ''
        firstName = ''
        prefixName = ''
        suffixName = ''

        if self.gender == 'm':
            firstNames = self.firstNamesMale
            lastPrefixes = self.lastPrefixesMale
            lastSuffixes = self.lastSuffixesMale
        else:
            firstNames = self.firstNamesFemale
            lastPrefixes = self.lastPrefixesFemale
            lastSuffixes = self.lastSuffixesFemale

        if len(firstNames) >= self.firstIndex:
            firstName = firstNames[self.firstIndex]
        if len(lastPrefixes) >= self.prefixIndex:
            prefixName = lastPrefixes[self.prefixIndex]
        if len(lastSuffixes) >= self.suffixIndex:
            suffixName = lastSuffixes[self.suffixIndex]

        # We have to do a bit of sanity checking here because
        # Disney would allow not using certain name parts.
        # This is only necessary if somebody hacked the
        # pick a name process which regulates this.
        if not firstName and not prefixName and not suffixName:
            return name
        if not prefixName and suffixName:
            return name
        if not suffixName and prefixName:
            return name

        name = (firstName + ' ' + prefixName + suffixName)

        return name.strip()

    def enterHandleFields(self):
        # Make sure the target avatar is part of the account:
        if self.avId not in self.avList:
            self.demand('Kill', 'Tried to populate non-existent/unowned avatar!')
            return

        # Check if the received DNA is valid.
        if not isinstance(self.avDNA, HumanDNA):
            self.demand('Kill', 'Invalid Pirate DNA!')
            return

        self.name = random.choice(PLocalizer.TempNameList)

        # They're using pick-a-name.
        if self.usePattern == 1:
            self.name = self.generateName()

            if not self.name:
                self.demand('Kill', 'Avatar requested invalid pattern name!')
                return

        # Query the avatar:
        avExists = self.manager.air.uniqueName(f'avExists-{self.avId}')
        self.acceptOnce(avExists, self.__populateAvatar)

        db = DatabaseObject(self.manager.air, self.avId)
        db.dclass = self.manager.air.dclassesByName['DistributedPlayerPirateUD']
        db.doneEvent = avExists
        db.getFields(db.getDatabaseFields(db.dclass))

    def __populateAvatar(self, db, retCode):
        if retCode != 0:
            self.demand('Kill', f'The avatar associated with {self.avId} is invalid!')
            return

        fieldNames = self.retrieveDNAFields()

        pirateFields = {
          'setName': (self.name,),
          'setDISLid': (self.target,)
        }

        for name in fieldNames:
            attr = getattr(self.avDNA, 'g' + name[1:], None)

            if not attr:
                continue

            data = attr()

            if isinstance(data, list):
                data = tuple(data)
                pirateFields[name] = data
                continue

            pirateFields[name] = (data,)


        # Push the change back through:
        db = DatabaseObject(self.manager.air, self.avId)
        db.dclass = self.manager.air.dclassesByName['DistributedPlayerPirateUD']
        db.setFields(pirateFields)

        self.__handlePopulateAvatar()

        self.avDNA = str(self.avDNA)

    def __handlePopulateAvatar(self):
        # Update the name on our active avatar object.
        self.manager.air.sendUpdateToDoId('DistributedAvatar', 'setName', self.avId, [self.name])

        self.manager.sendUpdateToAccountId(self.target, 'populateAvatarResponse', [True])

        self.demand('Off')

class DistributedAvatarManagerUD(OtpAvatarManagerUD):
    notify = directNotify.newCategory('AvatarManagerUD')
    notify.setInfo(True)

    def __init__(self, air):
        self.air = air

        OtpAvatarManagerUD.__init__(self, self)

    def announceGenerate(self):
        OtpAvatarManagerUD.announceGenerate(self)

        self.notify.info('Ready for requests.')

    def requestCreateAvatar(self, _, subId):
        self.runAvatarFSM(CreateAvatarFSM, subId)

    def requestPopulateAvatar(self, _, avId, avDNA, usePattern, nicknameIndex, firstIndex, prefixIndex, suffixIndex):
        self.runAvatarFSM(PopulateAvatarFSM, avId, avDNA, usePattern, nicknameIndex, firstIndex, prefixIndex, suffixIndex)
