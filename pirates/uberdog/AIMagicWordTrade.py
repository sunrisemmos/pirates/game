# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.uberdog.AIMagicWordTrade
from .AITrade import AITrade
from pirates.uberdog.UberDogGlobals import GiftOrigin, InventoryCategory, InventoryType, InventoryId
from direct.directnotify.DirectNotifyGlobal import directNotify

class AIMagicWordTrade(AITrade):
    notify = directNotify.newCategory('AIMagicWordTrade')

    def __init__(self, distObj, fromId, avatarId=None, inventoryId=None, timeout=4.0):
        AITrade.__init__(self, distObj, avatarId, inventoryId, timeout)
        self.giftOrigin = GiftOrigin.MAGIC_WORD
        self.fromId = fromId

    def _checkRules(self, givingLimitChanges, givingStacks, givingAccumulators, givingDoIds, givingLocatable, takingLimitChanges, takingStacks, takingAccumulators, takingDoIds, takingLocatable):
        pass

    def setAccumulator(self, accumulatorType, quantity):
        setAccumulator = InventoryId.getLimitChange(accumulatorType)
        self.giving.append((setAccumulator, quantity))

    def setReputation(self, category, amount):
        self.setAccumulator(category, amount)

    def getOrigin(self):
        return self.giftOrigin