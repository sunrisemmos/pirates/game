"""
The Pirates Uber Distributed Object Globals server.
"""

from pandac.PandaModules import *
import time
if __debug__:
    from direct.showbase.PythonUtil import *

from direct.directnotify.DirectNotifyGlobal import directNotify

from otp.distributed import OtpDoGlobals
from otp.ai.AIMsgTypes import *
from otp.ai import TimeManagerAI
from otp.uberdog.UberDog import UberDog

from otp.friends.AvatarFriendsManagerUD import AvatarFriendsManagerUD

from otp.uberdog.RejectCode import RejectCode

class PiratesUberDog(UberDog):
    notify = directNotify.newCategory("UberDog")

    def __init__(
            self, mdip, mdport, esip, esport, dcFilenames,
            serverId, minChannel, maxChannel):
        assert self.notify.debugStateCall(self)

        def isManagerFor(name):
            return len(uber.objectNames) == 0 or name in uber.objectNames
        self.isFriendsManager = False # latest from Ian this should not run anymore
        #self.isFriendsManager = isManagerFor("friends")

        UberDog.__init__(
            self, mdip, mdport, esip, esport, dcFilenames,
            serverId, minChannel, maxChannel)

    def createObjects(self):
        UberDog.createObjects(self)

        # Ask for the ObjectServer so we can check the dc hash value
        context = self.allocateContext()
        self.queryObjectAll(self.serverId, context)

        self.avatarManager = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_AVATAR_MANAGER, "DistributedAvatarManager")
        self.travelAgent = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_TRAVEL_AGENT, "DistributedTravelAgent")
        self.guildManager = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_GUILD_MANAGER, "PCGuildManager")
        self.inventoryManager = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_INVENTORY_MANAGER_BASE, "DistributedInventoryManager")

        self.settingsMgr = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_SETTINGS_MANAGER, "PiratesSettingsMgr")
        self.codeRedemption = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_CODE_REDEMPTION, "CodeRedemption")

        if not self.wantProduction:
            self.injectionManager = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_INJECTION_MGR, "InjectionManager")

        self.shipLoader = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_SHIP_MANAGER, "DistributedShipLoader")

        if self.isFriendsManager:
            self.avatarFriendsManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_AVATAR_FRIENDS_MANAGER,
                "PCAvatarFriendsManager")
            self.playerFriendsManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_PLAYER_FRIENDS_MANAGER,
                "PCPlayerFriendsManager")

        self.holidayMgr = self.generateGlobalObject(OtpDoGlobals.OTP_DO_ID_PIRATES_HOLIDAY_MANAGER, "HolidayManager")

    def getDatabaseIdForClassName(self, className):
        return DatabaseIdFromClassName.get(
            className, DefaultDatabaseChannelId)

    if __debug__:
        def status(self):
            if self.isFriendsManager:
                print("playerFriendsManager is ", self.playerFriendsManager)
