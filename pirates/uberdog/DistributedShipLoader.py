# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.uberdog.DistributedShipLoader
from direct.distributed.ClockDelta import *
from direct.directnotify import DirectNotifyGlobal
from direct.distributed import DistributedObject
from pickle import loads, dumps
from pirates.uberdog.UberDogGlobals import *

class DistributedShipLoader(DistributedObject.DistributedObject):
    notify = DirectNotifyGlobal.directNotify.newCategory('DistributedShipLoader')

    def __init__(self, cr):
        DistributedObject.DistributedObject.__init__(self, cr)
        self.ships = {}
        self.notify.warning('ShipLoader going online')

    def delete(self):
        self.ignoreAll()
        self.notify.warning('ShipLoader going offline')
        self.cr.shipLoader = None
        DistributedObject.DistributedObject.delete(self)
        return