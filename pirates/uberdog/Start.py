"""
Start the Pirates UberDog (Uber Distributed Object Globals server).
"""

import builtins
from direct.task.Task import Task
from panda3d.core import loadPrcFile

loadPrcFile("config/server.prc")

if __debug__:
    loadPrcFile("config/dev.prc")

class game:
    name = "uberDog"
    process = "server"
builtins.game = game()

import time
import os
import sys

from direct.showbase.PythonUtil import *
from otp.uberdog.UberDogGlobal import *
from pirates.uberdog.PiratesUberDog import PiratesUberDog

print("Initializing the Pirates UberDog (Uber Distributed Object Globals server)...")

uber.mdip = ConfigVariableString("msg-director-ip", "127.0.0.1").getValue()
uber.mdport = ConfigVariableInt("msg-director-port", 6666).getValue()

uber.esip = ConfigVariableString("event-server-ip", "127.0.0.1").getValue()
uber.esport = ConfigVariableInt("event-server-port", 4343).getValue()

stateServerId = ConfigVariableInt("state-server-id", 20100000).getValue()

uber.objectNames = set(os.getenv("uberdog_objects", "").split())

minChannel = ConfigVariableInt("uberdog-min-channel", 200400000).getValue()
maxChannel = ConfigVariableInt("uberdog-max-channel", 200449999).getValue()

uber.sbNSHost = ConfigVariableString("sb-host", "").getValue()
uber.sbNSPort = ConfigVariableInt("sb-port", 6053).getValue()
uber.sbListenPort = 6060
uber.clHost = "127.0.0.1"
uber.clPort = 9090
uber.allowUnfilteredChat = ConfigVariableInt("allow-unfiltered-chat", 0).getValue()
uber.bwDictPath = ""

uber.cpuInfoMgrHTTPListenPort = ConfigVariableInt("security_ban_mgr_port", 8892).getValue()

uber.air = PiratesUberDog(
        uber.mdip, uber.mdport,
        uber.esip, uber.esport,
        None,
        stateServerId,
        minChannel,
        maxChannel)

# How we let the world know we are not running a service
uber.aiService = 0

try:
    run()
except:
    info = describeException()
    #uber.air.writeServerEvent('uberdog-exception', districtNumber, info)
    raise
