from direct.distributed.PyDatagram import *
from direct.distributed.MsgTypes import *
from direct.directnotify.DirectNotifyGlobal import directNotify
from otp.distributed.OtpDoGlobals import *
from otp.otpbase import OTPGlobals, OTPLocalizer
from pirates.chat.PWhiteList import PWhiteList
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
import json, time, os, binascii, base64, limeade, requests

class JSONBridge:

    def __init__(self):
        self.__bridgeFile = config.GetString('account-bridge-file', 'astron/databases/accounts.json')

        if not os.path.exists(self.__bridgeFile):
            self.__bridge = {}
        else:
            with open(self.__bridgeFile, 'r') as f:
                self.__bridge = json.load(f)

    def __save(self):
        """
        Save the bridge to the file.
        """

        with open(self.__bridgeFile, 'w+') as f:
            json.dump(self.__bridge, f, sort_keys = 1, indent = 4, separators = [',', ': '])

    def put(self, playToken, accountId):
        """
        Throw an Account ID by its play token into the bridge.
        """

        self.__bridge[playToken] = accountId
        self.__save()

    def query(self, playToken):
        """
        Return the Account ID from the play token if it exists in the bridge.
        Otherwise, return False.
        """

        if playToken in self.__bridge:
            return self.__bridge[playToken]
        else:
            return False

class ExtAgent:
    notify = directNotify.newCategory('ExtAgent')

    def __init__(self, air):
        self.air = air
        self.air.registerForChannel(5536)

        self.air.registerEvent('registerShard', self.registerShard)
        self.air.registerEvent('refreshModules', self.refreshModules)

        self.shardInfo = {}

        self.bridge = JSONBridge()
        self.whiteList = PWhiteList()

        self.loadAvProgress = set()

        # Map of client channels -> avId.
        self.clientChannel2avId = {}

        # A list of staff members.
        # This is used for magic words.
        self.staffMembers = {}

        # A list of paid members.
        # Used when we activate on the DBSS.
        self.memberInfo = {}

        # Path where database objects will be stored.
        self.databasePath = 'astron/databases/astrondb'

        # Are we running on the production server?
        self.isProduction = config.GetBool('want-production', False)

        # Key used for decrypting our play tokens.
        self.decryptKey = config.GetString('token-decrypt-key', '')

        # Headers used for web requests.
        self.requestHeaders = {
            'User-Agent': 'Pirates of the Caribbean - ExtAgent'
        }

        # Do we want membership?
        self.wantMembership = config.GetBool('want-membership', False)

        # Used for sub accounts.
        self.subDataToAccId = {}

        # Map of accountId -> playToken.
        self.accId2playToken = {}

        # Base for API requests.
        self.banEndpointBase = 'http://127.0.0.1/bans'

        if not os.path.exists(self.databasePath):
            # Create our database folder.
            os.makedirs(self.databasePath)

    def isProdServer(self):
        return config.GetBool('want-production', False)

    def refreshModules(self):
        limeade.refresh()

    def sendKick(self, avId, reason):
        clientChannel = self.air.GetPuppetConnectionChannel(avId)

        errorCode = 152
        message = OTPLocalizer.CRBootedReasons.get(errorCode)

        self.sendBoot(clientChannel, errorCode, message)
        self.sendEject(clientChannel, errorCode, message)

    def sendEject(self, clientChannel, errorCode, errorStr):
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_EJECT)
        dg.addUint16(errorCode)
        dg.addString(errorStr)
        self.air.send(dg)

    def sendBoot(self, clientChannel, errorCode, errorStr):
        # Prepare the eject response.
        resp = PyDatagram()
        resp.addUint16(CLIENT_GO_GET_LOST)
        resp.addUint16(errorCode)
        resp.addString(errorStr)

        # Send it.
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SEND_DATAGRAM)
        dg.addBlob(resp.getMessage())
        self.air.send(dg)

    def registerShard(self, shardId, shardName):
        self.shardInfo[shardId] = (shardName, 0)

    def loginAccount(self, fields, clientChannel, accountId, playToken, isPaid, subData):
        # If somebody is logged into this account, disconnect them.
        self.sendEject(self.air.GetAccountConnectionChannel(accountId), 100, 'This account has been logged into elsewhere.')

        # Wait half a second before continuing to avoid a race condition.
        taskMgr.doMethodLater(0.5,
                              lambda x: self.finishLoginAccount(fields, clientChannel, accountId, playToken, isPaid, subData),
                              self.air.uniqueName('wait-acc-%s' % accountId), appendTask=False)

    def finishLoginAccount(self, fields, clientChannel, accountId, playToken, isPaid, subData):
        self.subDataToAccId[accountId] = subData

        # If there's anybody on the account, kill them for redundant login:
        datagram = PyDatagram()
        datagram.addServerHeader(self.air.GetAccountConnectionChannel(accountId), self.air.ourChannel, CLIENTAGENT_EJECT)
        datagram.addUint16(100)
        datagram.addString('This account has been logged in from elsewhere.')
        self.air.send(datagram)

        # Add the connection to the account channel.
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_OPEN_CHANNEL)
        dg.addChannel(self.air.GetAccountConnectionChannel(accountId))
        self.air.send(dg)

        # Set the client channel to the account ID shifted 32 bits to the left.
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SET_CLIENT_ID)
        dg.addChannel(accountId << 32)
        self.air.send(dg)

        # Un-sandbox the client.
        self.air.setClientState(clientChannel, 2)

        # Prepare the login response.
        resp = PyDatagram()
        resp.addUint16(110) # CLIENT_LOGIN_3_RESP

        resp.addInt8(0) # returnCode
        resp.addString('') # errorString

        resp.addString('YES') # openChatEnabled
        resp.addString('YES') # createFriendsWithChat
        resp.addString('YES') # chatCodeCreation

        # Access (Membership)
        if self.isProduction:
            if isPaid:
                resp.addString('FULL')
                self.memberInfo[accountId] = playToken
            else:
                resp.addString('VELVET')
        else:
            if self.wantMembership:
                resp.addString('FULL')
            else:
                resp.addString('VELVET')

        resp.addInt32(0) # familyAccountId
        resp.addInt32(accountId) # playerAccountId

        resp.addString(playToken) # playerName
        resp.addInt8(1) # playerNameApproved
        resp.addInt32(4 if self.isProduction and isPaid else 2 or 4 if self.wantMembership else 2) # maxAvatars
        resp.addUint16(len(subData)) # numSubs

        for i in range(len(subData)):
            resp.addUint32(subData[i][0]) # subId
            resp.addUint32(accountId) # subOwnerId

            resp.addString(subData[i][1]) # subName
            resp.addString(subData[i][2]) # subActive
            resp.addString('FULL' if self.isProduction and isPaid else 'FULL' if self.wantMembership else 'VELVET') # subAccess
            resp.addUint8(1) # subLevel
            resp.addUint8(4 if self.isProduction and isPaid else 2 or 4 if self.wantMembership else 2) # subNumAvatars
            resp.addUint8(1) # subNumConcur
            resp.addString('YES') # subFounder

        resp.addString('YES') # WLChatEnabled

        # Dispatch the response to the client.
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SEND_DATAGRAM)
        dg.addBlob(resp.getMessage())
        self.air.send(dg)

        self.accId2playToken[accountId] = playToken

    def filterWhiteList(self, message):
        modifications = []
        words = message.split(' ')
        offset = 0

        for word in words:
            if word and not self.whiteList.isWord(word):
                modifications.append((offset, offset + len(word) - 1))

            offset += len(word) + 1

        cleanMesssage = message

        for modStart, modStop in modifications:
            cleanMesssage = cleanMesssage[:modStart] + '*' * (modStop - modStart + 1) + cleanMesssage[modStop + 1:]

        return cleanMesssage, modifications

    def banAccount(self, playToken, message, banReason = 'Chat Filter', silent = False):
        endpoint = f'{self.banEndpointBase}/BanAccount.php'
        emailDispatchEndpoint = f'{self.banEndpointBase}/ChatBanEmail.php'

        secretKey = config.GetString('secret-key')

        banData = {
            'username': playToken,
            'banReason': banReason,
            'secretKey': secretKey
        }

        emailData = {
            'playToken': playToken,
            'chatMessages': message,
            'secretKey': secretKey
        }

        banRequest = requests.post(endpoint, banData, headers = self.requestHeaders)

        if not silent:
            emailRequest = requests.post(emailDispatchEndpoint, emailData, headers = self.requestHeaders)

    def approveName(self, avId):
        pirateDC = simbase.air.dclassesByName['DistributedPlayerPirateUD']

        fields = {
            'WishNameState': ('APPROVED',)
        }

        simbase.air.dbInterface.updateObject(simbase.air.dbId, avId, pirateDC, fields)

    def rejectName(self, avId):
        pirateDC = simbase.air.dclassesByName['DistributedPlayerPirateUD']

        fields = {
            'WishNameState': ('REJECTED',)
        }

        simbase.air.dbInterface.updateObject(simbase.air.dbId, avId, pirateDC, fields)

    def sendSystemMessage(self, clientChannel, message, aknowledge = False):
        # Prepare the System Message response.
        resp = PyDatagram()

        if aknowledge:
            resp.addUint16(123) # CLIENT_SYSTEMMESSAGE_AKNOWLEDGE
        else:
            resp.addUint16(78) # CLIENT_SYSTEM_MESSAGE

        resp.addString(message)

        # Send it.
        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SEND_DATAGRAM)
        dg.addBlob(resp.getMessage())
        self.air.send(dg)

    def warnPlayer(self, clientChannel, reason):
        self.sendSystemMessage(clientChannel, reason, True)

    def handleDatagram(self, dgi):
        """
        This handles datagrams coming directly from the Pirates 2013 client.
        These datagrams may be reformatted in a way that can appease Astron,
        or are totally game-specific and will have their own handling.
        """

        clientChannel = dgi.getUint64()
        msgType = dgi.getUint16()

        if msgType == 111: # CLIENT_LOGIN_3
            playToken = dgi.getString()
            serverVersion = dgi.getString()
            hashVal = dgi.getUint32()
            tokenType = dgi.getInt32()
            validateDownload = dgi.getString()
            wantMagicWords = dgi.getString()

            if tokenType != CLIENT_LOGIN_3_DISL_TOKEN:
                reason = 'Invalid token type.'
                self.sendEject(clientChannel, 122, reason)
                return

            # Dummy variables.
            # This is used if we aren't in production mode.
            isPaid = 0
            subData = [
                [1, playToken, 'YES']
            ]

            if self.isProduction:
                try:
                    # Decrypt the play token.
                    key = binascii.unhexlify(self.decryptKey)
                    encrypted = json.loads(base64.b64decode(playToken))
                    encryptedData = base64.b64decode(encrypted['data'])
                    iv = base64.b64decode(encrypted['iv'])
                    cipher = AES.new(key, AES.MODE_CBC, iv)
                    data = unpad(cipher.decrypt(encryptedData), AES.block_size)
                    jsonData = json.loads(data)

                    # Retrieve data from the API response.
                    # Currently, we do not use all these values.
                    playToken = str(jsonData['playToken'])
                    dislId = int(jsonData['dislId'])
                    accountType = str(jsonData['accountType'])

                    accountId = self.bridge.query(playToken)

                    if accountId:
                        self.air.sendEvent('magicWordApproved', [accountId])

                        data = {
                            'accountId': accountId,
                            'playToken': playToken
                        }

                        # Retrieve our sub data.
                        endpointUrl = config.GetString('sub-data-endpoint')
                        request = requests.post(endpointUrl, data, headers = self.requestHeaders).json()
                        subData = request['subData']

                    if accountType in ('Administrator', 'Developer', 'Moderator'):
                        # This is a staff member.
                        self.staffMembers[playToken] = accountType
                except:
                    # Bad play token.
                    reason = 'Invalid play token.'
                    self.sendEject(clientChannel, 122, reason)
                    return

            # Check the server version against the real one.
            ourVersion = config.GetString('server-version', '')
            if serverVersion != ourVersion:
                # Version mismatch.
                reason = 'Client version mismatch: client=%s, server=%s' % (serverVersion, ourVersion)
                self.sendEject(clientChannel, 122, reason)
                return

            # Check the client DC hash against the server's DC hash.
            correctHash = 2790912635
            if hashVal != correctHash:
                # DC hash mismatch.
                reason = 'Client DC hash mismatch: client=%s, server=%s' % (hashVal, correctHash)
                self.sendEject(clientChannel, 122, reason)
                return

            # Check if this play token exists in the bridge.
            accountId = self.bridge.query(playToken)
            if accountId:

                def queryLoginResponse(dclass, fields):
                    if dclass != self.air.dclassesByName['AccountUD']:
                        # This isn't an Account object.
                        self.sendEject(clientChannel, 122, 'The Account object was unable to be queried.')
                        return

                    # Log in the account.
                    self.loginAccount(fields, clientChannel, accountId, playToken, isPaid, subData)

                # Query the Account object.
                self.air.dbInterface.queryObject(self.air.dbId, accountId, queryLoginResponse)
                return

            # Create a new Account object.
            account = {'ACCOUNT_AV_SET': [0] * 6,
                       'ACCOUNT_AV_SET_DEL': [],
                       'CREATED': time.ctime(),
                       'LAST_LOGIN': time.ctime()}

            def createLoginResponse(accountId):
                if not accountId:
                    self.sendEject(clientChannel, 122, 'The Account object was unable to be created.')
                    return

                # Put the account id in the account bridge.
                self.bridge.put(playToken, accountId)

                # Log in the account.
                self.loginAccount(account, clientChannel, accountId, playToken, isPaid, subData)

            # Create the Account in the database.
            self.air.dbInterface.createObject(self.air.dbId,
                                              self.air.dclassesByName['AccountUD'],
                                              account,
                                              createLoginResponse)
        elif msgType == CLIENT_OBJECT_UPDATE_FIELD:
            # Certain fields need to be handled specially...
            # The only example of this right now is setChat.
            doId = dgi.getUint32()
            fieldNumber = dgi.getUint16()
            dcData = dgi.getRemainingBytes()

            pirate = self.air.dclassesByName['DistributedPlayerPirateUD']
            talkField = pirate.getFieldByName('setTalk')
            setTalk = talkField.getNumber()

            if fieldNumber == setTalk:
                # We'll have to unpack the data and send our own datagrams.
                unpacker = DCPacker()
                unpacker.setUnpackData(dcData)
                unpacker.beginUnpack(talkField)
                fieldArgs = talkField.unpackArgs(unpacker)
                unpacker.endUnpack()

                if len(fieldArgs) != 6:
                    # Bad field data.
                    return

                message = fieldArgs[3]

                if not message:
                    return

                accId = int(self.air.getAccountIdFromSender())
                playToken = self.accId2playToken.get(accId, '')

                if message[0] in ('~', '@'):
                    if playToken in self.staffMembers or not self.isProdServer():
                        # We must be using the stock Disney client.
                        # Route this to the Magic Word manager.
                        self.air.sendEvent('magicWord', [message, doId])
                        return

                cleanMessage, modifications = self.filterWhiteList(message)

                def queryAvatar(dclass, fields):
                    if not dclass or not fields:
                        return

                    # Construct a new aiFormatUpdate.
                    resp = pirate.aiFormatUpdate('setTalk', doId, doId, self.air.ourChannel, [0, 0, fields.get('setName', ('',))[0], cleanMessage, modifications, 0])
                    self.air.send(resp)
                    return

                self.air.dbInterface.queryObject(self.air.dbId, doId, queryAvatar, dclass = self.air.dclassesByName['DistributedAvatarUD'])

            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, msgType)
            resp.addUint32(doId)
            resp.addUint16(fieldNumber)
            resp.appendData(dcData)
            self.air.send(resp)
        elif msgType == 32: # CLIENT_SET_AVATAR
            avId = dgi.getUint32()
            puppetChannel = self.air.GetPuppetConnectionChannel(avId)

            if avId == 0:
                # We assume the avatar is going back to the Avatar Chooser.
                # Lets unload.

                # Grab the avId.
                avId = self.air.getAvatarIdFromSender()
                puppetChannel = self.air.GetPuppetConnectionChannel(avId)

                target = clientChannel >> 32
                channel = self.air.GetAccountConnectionChannel(target)

                # We need to let our friends know we are going offline.
                self.air.sendEvent('avatarOffline', [avId])

                # Remove our POST_REMOVES.
                dg = PyDatagram()
                dg.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_CLEAR_POST_REMOVES)
                self.air.send(dg)

                # Remove avatar channel:
                dg = PyDatagram()
                dg.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_CLOSE_CHANNEL)
                dg.addChannel(puppetChannel)
                self.air.send(dg)

                # Reset the session object.
                dg = PyDatagram()
                dg.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_REMOVE_SESSION_OBJECT)
                dg.addUint32(avId)
                self.air.send(dg)

                # Unload avatar object:
                dg = PyDatagram()
                dg.addServerHeader(avId, channel, STATESERVER_OBJECT_DELETE_RAM)
                dg.addUint32(avId)
                self.air.send(dg)
                return

            def handleAvatarRetrieve(dclass, fields):
                if dclass != self.air.dclassesByName['DistributedPlayerPirateUD']:
                    # This is not an Avatar object.
                    self.sendEject(clientChannel, 122, 'An Avatar object was not retrieved.')
                    return

                target = clientChannel >> 32
                channel = self.air.GetAccountConnectionChannel(target)

                # First, give them a POSTREMOVE to unload the avatar, just in case they
                # disconnect while we're working.
                datagramCleanup = PyDatagram()
                datagramCleanup.addServerHeader(avId, channel, STATESERVER_OBJECT_DELETE_RAM)
                datagramCleanup.addUint32(avId)

                datagram = PyDatagram()
                datagram.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_ADD_POST_REMOVE)
                datagram.addBlob(datagramCleanup.getMessage())
                self.air.send(datagram)

                try:
                    # Tell the AvatarFriendsManager that we are online.
                    # [avatarId, accountId, playerName, playerNameApproved, openChatEnabled, createFriendsWithChat, chatCodeCreation]
                    avatarName = fields['setName'][0]
                    self.air.sendEvent('avatarOnlinePlusAccountInfo', [avId, target, avatarName, 1, 1, 1, 1])
                except:
                    # The MySQL database must of died.
                    self.notify.warning('Failed to call avatarOnlinePlusAccountInfo for AvatarFriendsManager!')

                # Assign a POST_REMOVE incase we disconnect unexpectedly.
                cleanupDatagram = self.air.messenger.prepare('avatarOffline', [avId])

                datagram = PyDatagram()
                datagram.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_ADD_POST_REMOVE)
                datagram.addBlob(cleanupDatagram.getMessage())
                self.air.send(datagram)

                def inventoryQueryCallback(inventoryId):
                    channel = self.air.GetAccountConnectionChannel(target)

                    if self.isProduction:
                        playToken = self.memberInfo.get(target, '')
                        access = OTPGlobals.AccessFull if playToken else OTPGlobals.AccessVelvetRope
                    else:
                        access = OTPGlobals.AccessFull if self.wantMembership else OTPGlobals.AccessVelvetRope

                    activateFields = {
                        'setCommonChatFlags': [0],
                        'setAccess':  [access],
                        'setInventoryId': (inventoryId,),
                        'setFounder': [fields['setFounder'][0]]
                    }

                    # Activate the avatar on the DBSS.
                    self.air.sendActivate(avId, 0, 0, self.air.dclassesByName['DistributedPlayerPirateUD'], activateFields)

                    # Add the client to the avatar channel.
                    datagram = PyDatagram()
                    datagram.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_OPEN_CHANNEL)
                    datagram.addChannel(puppetChannel)
                    self.air.send(datagram)

                    # Save the account ID.
                    self.loadAvProgress.add(target)

                    # Set the client ID to associate the client channel with the avatar ID.
                    datagram = PyDatagram()
                    datagram.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_SET_CLIENT_ID)
                    datagram.addChannel(target << 32 | avId)
                    self.air.send(datagram)

                    # Eliminate race conditions.
                    taskMgr.doMethodLater(0.2, enterSetAvatarTask, f'avatarTask-{avId}', extraArgs = [channel, inventoryId], appendTask = True)

                # get the doId of the avatar's inventory object that it will
                # be activated on by the DBSS...
                self.air.inventoryManager.queryInventory(avId, inventoryQueryCallback)

                def enterSetAvatarTask(channel, inventoryId, task):
                    # Make the client own the avatar.
                    self.air.setOwner(avId, channel, True)

                    # add a post remove for removing the avatar as a session object on the ClientAgent
                    datagramCleanup = PyDatagram()
                    datagramCleanup.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_REMOVE_SESSION_OBJECT)

                    datagramCleanup.addUint32(avId)

                    datagram = PyDatagram()
                    datagram.addServerHeader(channel, self.air.ourChannel, CLIENTAGENT_ADD_POST_REMOVE)
                    datagram.addBlob(datagramCleanup.getMessage())
                    self.air.send(datagram)

                    self.air.clientAddSessionObject(puppetChannel, avId)

                    def inventoryActivatedCallback(inventoryId):
                        self.air.writeServerEvent('avatarChosen', avId = avId, target = target)
                        self.air.sendEvent('populateInventory', [avId])

                    # activate the avatar's inventory on the DBSS
                    self.air.inventoryManager.activateInventory(avId, inventoryId, inventoryActivatedCallback)

                    return task.done

            def handleAccountRetrieve(dclass, fields):
                if dclass != self.air.dclassesByName['AccountUD']:
                    # This is not an Account object.
                    self.sendEject(clientChannel, 122, 'An Account object was not retrieved.')
                    return

                # Get the avatar list.
                avList = fields['ACCOUNT_AV_SET'][:4]
                avList += [0] * (4 - len(avList))

                # Make sure the requested avatar is part of this account.
                if avId not in avList:
                    # Nope.
                    self.sendEject(clientChannel, 122, 'Attempted to play an Avatar not part of the Account.')
                    return

                # Fetch the avatar details.
                self.air.dbInterface.queryObject(self.air.dbId, avId, handleAvatarRetrieve)

            # Query the account.
            self.air.dbInterface.queryObject(self.air.dbId, clientChannel >> 32, handleAccountRetrieve)
        elif msgType == CLIENT_DISCONNECT:
            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, msgType)
            self.air.send(resp)
        elif msgType == CLIENT_HEARTBEAT:
            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, msgType)
            self.air.send(resp)
        elif msgType == 70: # CLIENT_SET_WISHNAME
            avId = dgi.getUint32()
            name = dgi.getString()

            if avId:
                fields = {
                    'WishNameState': ('PENDING',),
                    'WishName': (name,)
                }

                # Update the Avatar object with the new wish name.
                self.air.dbInterface.updateObject(self.air.dbId, avId, self.air.dclassesByName['DistributedPlayerPirateUD'], fields)

            # Prepare the wish name response.
            resp = PyDatagram()
            resp.addUint16(71) # CLIENT_SET_WISHNAME_RESP
            resp.addUint32(avId)
            resp.addUint16(0) # returnCode
            resp.addString(name) # pendingName
            resp.addString('') # approvedName
            resp.addString('') # rejectedName

            # Send it.
            dg = PyDatagram()
            dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SEND_DATAGRAM)
            dg.addBlob(resp.getMessage())
            self.air.send(dg)
        elif msgType == CLIENT_ADD_INTEREST:
            # Reformat the packets for the CA.
            handle = dgi.getUint16()
            context = dgi.getUint32()
            parentId = dgi.getUint32()
            zones = []

            while dgi.getRemainingSize() != 0:
                zones.append(dgi.getUint32())

            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, CLIENT_ADD_INTEREST_MULTIPLE)
            resp.addUint32(context)
            resp.addUint16(handle)
            resp.addUint32(parentId)
            resp.addUint16(len(zones))
            for zone in zones:
                resp.addUint32(zone)
            self.air.send(resp)
        elif msgType == CLIENT_REMOVE_INTEREST:
            handle = dgi.getUint16()
            context = 0
            if dgi.getRemainingSize() > 0:
                context = dgi.getUint32()

            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, msgType)
            resp.addUint32(context)
            resp.addUint16(handle)
            self.air.send(resp)
        elif msgType == CLIENT_OBJECT_LOCATION:
            resp = PyDatagram()
            resp.addServerHeader(clientChannel, self.air.ourChannel, msgType)
            resp.addUint32(dgi.getUint32())
            resp.addUint32(dgi.getUint32())
            resp.addUint32(dgi.getUint32())
            self.air.send(resp)
        else:
            self.notify.warning('Received unknown message type %s from Client' % msgType)

    def handleResp(self, dgi):
        """
        This handles Astron message types that under normal conditions
        would go to the client. The packets are rearranged
        for the Disney client here.
        """

        clientChannel = dgi.getUint64()
        msgType = dgi.getUint16()
        resp = None

        if msgType == CLIENT_DONE_INTEREST_RESP:
            contextId = dgi.getUint32()
            handle = dgi.getUint16()

            resp = PyDatagram()
            resp.addUint16(msgType)
            resp.addUint16(handle)
            resp.addUint32(contextId)
        elif msgType in (CLIENT_ADD_INTEREST, CLIENT_ADD_INTEREST_MULTIPLE, CLIENT_REMOVE_INTEREST):
            pass
        elif msgType == CLIENT_OBJECT_UPDATE_FIELD:
            doId = dgi.getUint32()
            dcData = dgi.getRemainingBytes()

            resp = PyDatagram()
            resp.addUint16(msgType)
            resp.addUint32(doId)
            resp.appendData(dcData)
        elif msgType == CLIENT_OBJECT_LOCATION:
            resp = PyDatagram()
            resp.addUint16(102) # CLIENT_OBJECT_LOCATION
            resp.addUint32(dgi.getUint32())
            resp.addUint32(dgi.getUint32())
            resp.addUint32(dgi.getUint32())
        elif msgType == CLIENT_OBJECT_DISABLE:
            resp = PyDatagram()
            resp.addUint16(25) # CLIENT_OBJECT_DISABLE
            resp.appendData(dgi.getRemainingBytes())
        elif msgType == CLIENT_OBJECT_DISABLE_OWNER:
            resp = PyDatagram()
            resp.addUint16(26) # CLIENT_OBJECT_DISABLE_OWNER_RESP
            resp.appendData(dgi.getRemainingBytes())
        elif msgType == CLIENT_CREATE_OBJECT_REQUIRED:
            doId = dgi.getUint32()
            parentId = dgi.getUint32()
            zoneId = dgi.getUint32()
            classId = dgi.getUint16()
            dcData = dgi.getRemainingBytes()

            resp = PyDatagram()
            resp.addUint16(msgType)
            resp.addUint32(parentId)
            resp.addUint32(zoneId)
            resp.addUint16(classId)
            resp.addUint32(doId)
            resp.appendData(dcData)
        elif msgType == CLIENT_CREATE_OBJECT_REQUIRED_OTHER:
            doId = dgi.getUint32()
            parentId = dgi.getUint32()
            zoneId = dgi.getUint32()
            classId = dgi.getUint16()
            dcData = dgi.getRemainingBytes()

            resp = PyDatagram()
            resp.addUint16(msgType)
            resp.addUint32(parentId)
            resp.addUint32(zoneId)
            resp.addUint16(classId)
            resp.addUint32(doId)
            resp.appendData(dcData)
        elif msgType == CLIENT_CREATE_OBJECT_REQUIRED_OTHER_OWNER:
            isAvatar = True

            if clientChannel >> 32 not in self.loadAvProgress:
                isAvatar = False

            doId = dgi.getUint32()
            parentId = dgi.getUint32()
            zoneId = dgi.getUint32()
            classId = dgi.getUint16()
            dcData = dgi.getRemainingBytes()

            resp = PyDatagram()

            if isAvatar:
                resp.addUint16(15) # CLIENT_GET_AVATAR_DETAILS_RESP
                resp.addUint32(doId)
                resp.addUint8(0)
                resp.appendData(dcData)
            else:
                resp.addUint16(msgType)
                resp.addUint16(classId)
                resp.addUint32(doId)
                resp.addUint32(parentId)
                resp.addUint32(zoneId)
                resp.appendData(dcData)

            self.clientChannel2avId[clientChannel] = doId

            if clientChannel >> 32 in self.loadAvProgress:
                self.loadAvProgress.remove(clientChannel >> 32)
        else:
            self.notify.warning('Received unknown message type %s from Astron' % msgType)

        if not resp:
            return

        dg = PyDatagram()
        dg.addServerHeader(clientChannel, self.air.ourChannel, CLIENTAGENT_SEND_DATAGRAM)
        dg.addBlob(resp.getMessage())
        self.air.send(dg)