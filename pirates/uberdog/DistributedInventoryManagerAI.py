from direct.distributed.DistributedObjectGlobalAI import DistributedObjectGlobalAI
from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.fsm.FSM import FSM

from pirates.uberdog.UberDogGlobals import InventoryId, InventoryType, InventoryCategory
from pirates.uberdog.DistributedInventoryBase import DistributedInventoryBase

from pirates.ai.DatabaseObject import DatabaseObject

from pirates.ai.PiratesAIMsgTypes import DATABASE_OBJECT_TYPE_INVENTORY

class InventoryOperationFSM(FSM):
    notify = directNotify.newCategory('InventoryOperationFSM')

    def __init__(self, air, avatar, callback=None):
        FSM.__init__(self, self.__class__.__name__)
        self.air = air
        self.avatar = avatar
        self.callback = callback

    def getAvatarClassName(self):
        return 'DistributedPlayerPirateAI'

    def getInventoryClassName(self):
        return 'PirateInventoryAI'

    def enterOff(self):
        pass

    def exitOff(Self):
        pass

    def cleanup(self, *args, **kwargs):
        del self.air.inventoryManager.avatar2fsm[self.avatar.doId]
        self.ignoreAll()
        self.demand('Off')

        if self.callback:
            self.callback(*args, **kwargs)

class LoadInventoryFSM(InventoryOperationFSM):

    def enterStart(self):
        # Query the pirate:
        gotPirateEvent = self.air.uniqueName(f'gotPirate-{self.avatar.doId}')
        self.acceptOnce(gotPirateEvent, self._avatarQueryCallback)

        db = DatabaseObject(self.air, self.avatar.doId)
        db.doneEvent = self._avatarQueryCallback
        db.dclass = self.air.dclassesByName[self.getAvatarClassName()]
        db.getFields(['setInventoryId'])

        self.acceptOnce(self.avatar.getDeleteEvent(), lambda: self.cleanup(None))

    def _avatarQueryCallback(self, db, retCode):
        if retCode != 0:
            self.notify.debug('Failed to query avatar %d!' % self.avatar.doId)
            self.cleanup(None)
            return

        inventoryId, = db.values.get('setInventoryId', (0,))
        if not inventoryId:
            self.notify.warning('Avatar %d does not have an inventory!' % self.avatar.doId)
            self.cleanup(None)
            return

        inventory = self.air.doId2do.get(inventoryId)
        if not inventory:
            self.acceptOnce('generate-%d' % inventoryId, self._inventoryArrivedCallback)
        else:
            self._inventoryArrivedCallback(inventory)

    def finalizeInventory(self):
        self.inventory.b_setStackLimit(InventoryType.Hp, self.avatar.getMaxHp())
        self.inventory.b_setStackLimit(InventoryType.Mojo, self.avatar.getMaxMojo())

    def _inventoryArrivedCallback(self, inventory):
        self.inventory = inventory
        if not inventory:
            self.notify.warning('Failed to retrieve inventory for avatar %d' % self.avatar.doId)
            self.cleanup(None)
            return

        # Query inventory:
        gotInventoryEvent = self.air.uniqueName(f'gotInventory-{self.inventory.doId}')
        self.acceptOnce(gotInventoryEvent, self.inventoryQueryCallback)

        db = DatabaseObject(self.air, self.inventory.doId)
        db.doneEvent = self.inventoryQueryCallback
        db.dclass = self.air.dclassesByName['DistributedInventoryAI']
        db.getFields(['setCategoryLimits', 'setDoIds', 'setAccumulators', 'setStackLimits', 'setStacks'])

    def inventoryQueryCallback(self, db, retCode):
        if retCode != 0:
            self.notify.debug('Failed to query inventory %d for avatar %d!' % (
                self.inventory.id, self.avatar.doId))

            self.cleanup(None)
            return

        categoriesAndLimits, = db.values.get('setCategoryLimits', [])
        categoriesAndDoIds, = db.values.get('setDoIds', [])
        accumulatorTypesAndQuantities, = db.values.get('setAccumulators', [])
        stackTypesAndLimits, = db.values.get('setStackLimits', [])
        stackTypesAndQuantities, = db.values.get('setStacks', [])

        for categoryAndLimit in categoriesAndLimits:
            self.inventory.b_setCategoryLimit(*categoryAndLimit)

        self.inventory.b_setDoIds(categoriesAndDoIds)
        for accumulatorTypeAndQuantity in accumulatorTypesAndQuantities:
            self.inventory.b_setAccumulator(*accumulatorTypeAndQuantity)

        for stackTypeAndLimit in stackTypesAndLimits:
            self.inventory.b_setStackLimit(*stackTypeAndLimit)

        for stackTypeAndQuantity in stackTypesAndQuantities:
            self.inventory.b_setStackQuantity(*stackTypeAndQuantity)

        self.finalizeInventory()
        self.inventory.populateInventory()
        self.cleanup(self.inventory)

    def exitStart(self):
        pass

class LoadShipInventoryFSM(LoadInventoryFSM):

    def getAvatarClassName(self):
        return 'PlayerShipAI'

    def getInventoryClassName(self):
        return 'DistributedInventoryAI'

    def finalizeInventory(self):
        pass

class DistributedInventoryManagerAI(DistributedObjectGlobalAI):
    notify = directNotify.newCategory('DistributedInventoryManagerAI')

    def __init__(self, air):
        DistributedObjectGlobalAI.__init__(self, air)

        self.avatar2fsm = {}
        self.inventories = {}

    def announceGenerate(self):
        DistributedObjectGlobalAI.announceGenerate(self)

        self.air.netMessenger.accept('hasInventory', self, self.sendHasInventory)
        self.air.netMessenger.accept('addInventory', self, self.addInventory)
        self.air.netMessenger.accept('removeInventory', self, self.removeInventory)
        self.air.netMessenger.accept('getInventory', self, self.sendGetInventory)
        self.air.netMessenger.accept('populateInventory', self, self.populateInventory)

    def populateInventory(self, avId):
        inventory = self.getInventory(avId)

        if inventory:
            inventory.b_setStackQuantity(InventoryType.CutlassWeaponL5, 1)
            inventory.b_setStackQuantity(InventoryType.PistolWeaponL5, 1)
            inventory.b_setStackQuantity(InventoryType.DollWeaponL5, 1)
            inventory.b_setStackQuantity(InventoryType.DaggerWeaponL5, 1)
            inventory.b_setStackQuantity(InventoryType.WandWeaponL5, 1)
            return 'Populated inventory!'

        return 'Failed to populate.'

    def hasInventory(self, inventoryId):
        return inventoryId in self.inventories

    def sendHasInventory(self, inventoryId, callback):
        self.air.netMessenger.send('hasInventoryResponse', [callback, self.hasInventory(inventoryId)])

    def addInventory(self, inventory):
        if self.hasInventory(inventory.doId):
            self.notify.debug('Tried to add an already existing inventory %d!' % inventory.doId)
            return

        self.inventories[inventory.doId] = inventory

    def removeInventory(self, inventory):
        if not self.hasInventory(inventory.doId):
            self.notify.debug('Tried to remove a non-existant inventory %d!' % inventory.doId)
            return

        inventory.requestDelete()
        del self.inventories[inventory.doId]

    def getInventory(self, avatarId):
        for inventory in list(self.inventories.values()):
            if inventory.getOwnerId() == avatarId:
                return inventory

        return None

    def sendGetInventory(self, avatarId, callback):
        self.air.netMessenger.send('getInventoryResponse', [callback, self.getInventory(avatarId)])

    def runInventoryFSM(self, fsmtype, avatar, *args, **kwargs):
        if avatar.doId in self.avatar2fsm:
            self.notify.debug('Failed to run inventory FSM for avatar %d, '
                'an FSM already running!' % avatar.doId)

            return

        callback = kwargs.pop('callback', None)

        self.avatar2fsm[avatar.doId] = fsmtype(self.air, avatar, callback)
        self.avatar2fsm[avatar.doId].request('Start', *args, **kwargs)

    def requestInventory(self):
        avatarId = self.air.getAvatarIdFromSender()
        if not avatarId:
            return

        avatar = self.air.doId2do.get(avatarId)
        if not avatar:
            self.accept('generate-%d' % avatarId, self.initiateInventory)
        else:
            self.initiateInventory(avatar)

    def initiateInventory(self, avatar, callback=None):
        self.runInventoryFSM(LoadInventoryFSM, avatar, callback=callback)

    def initiateShipInventory(self, ship, callback=None):
        self.runInventoryFSM(LoadShipInventoryFSM, ship, callback=callback)

    def proccessCallbackResponse(self, callback, *args, **kwargs):
        if callback and callable(callback):
            callback(*args, **kwargs)
            return

        self.notify.warning('No valid callback for a callback response!'
            'What was the purpose of that?')
