from direct.distributed.DistributedObjectUD import DistributedObjectUD
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedInventoryUD(DistributedObjectUD):
    notify = directNotify.newCategory('DistributedInventoryUD')

    def __init__(self, air):
        DistributedObjectUD.__init__(self, air)

    def setParentingRules(self, todo0, todo1):
        pass

    def setInventoryVersion(self, todo0):
        pass

    def setOwnerId(self, todo0):
        pass

    def setCategoryLimits(self, todo0):
        pass

    def setDoIds(self, todo0):
        pass

    def accumulator(self, todo0, todo1):
        pass

    def setAccumulators(self, todo0):
        pass

    def stackLimit(self, todo0, todo1):
        pass

    def stack(self, todo0, todo1):
        pass

    def setStackLimits(self, todo0):
        pass

    def setStacks(self, todo0):
        pass

    def setTemporaryInventory(self, todo0):
        pass

    def setTemporaryStack(self, todo0, todo1):
        pass

    def sendMaxHp(self, todo0, todo1):
        pass

    def sendMaxMojo(self, todo0, todo1):
        pass

    def requestInventoryComplete(self):
        pass

    def approvedTradeResponse(self, todo0):
        pass
