
from otp.uberdog.OtpAvatarManagerAI import OtpAvatarManagerAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedAvatarManagerAI(OtpAvatarManagerAI):
    notify = directNotify.newCategory('DistributedAvatarManagerAI')

    def __init__(self, air):
        OtpAvatarManagerAI.__init__(self, air)