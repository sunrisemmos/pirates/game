# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.creature.Animal
from pirates.creature.Creature import Creature

class Animal(Creature):

    def __init__(self, animationMixer=None):
        Creature.__init__(self, animationMixer)

    @report(types=['module', 'args'], dConfigParam='nametag')
    def initializeNametag3d(self):
        pass

    def initializeNametag3dPet(self):
        Creature.initializeNametag3d(self)