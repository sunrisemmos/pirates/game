# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.creature.DistributedAnimal
from direct.directnotify import DirectNotifyGlobal
from pirates.creature import DistributedCreature
from pirates.pirate import AvatarTypes
from pirates.piratesbase import PiratesGlobals
from otp.otpbase import OTPRender

class DistributedAnimal(DistributedCreature.DistributedCreature):

    def __init__(self, cr):
        DistributedCreature.DistributedCreature.__init__(self, cr)
        self.battleCollisionBitmask = PiratesGlobals.WallBitmask | PiratesGlobals.TargetBitmask
        OTPRender.renderReflection(False, self, 'p_animal', None)
        return

    def setupCreature(self, avatarType):
        DistributedCreature.DistributedCreature.setupCreature(self, avatarType)
        self.motionFSM.motionAnimFSM.setupSplashAnimOverride(self.creature.getSplashOverride())
        self.motionFSM.motionAnimFSM.setupSplashAnimOverrideDelay(12)

    def customInteractOptions(self):
        self.setInteractOptions(proximityText=None, allowInteract=False)
        return

    def showHpMeter(self):
        pass

    def isBattleable(self):
        return 0

    def initializeBattleCollisions(self):
        pass

    def getMinimapObject(self):
        return

    def canIdleSplashEver(self):
        return self.creature.getSplashOverride()

    def canIdleSplash(self):
        return self.creature.getCurrentAnim() == 'idle'