# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.web.PiratesSettingsMgrBase
from pirates.pvp import PVPGlobals
from pirates.ship import ShipBalance

class PiratesSettingsMgrBase:

    def _initSettings(self):
        self._addSettings(PVPGlobals.WantIslandRegen, PVPGlobals.WantShipRepairSpots, PVPGlobals.WantShipRepairKit, PVPGlobals.ShipRegenRadiusScale, PVPGlobals.ShipRegenHps, PVPGlobals.ShipRegenSps, PVPGlobals.ShipRegenPeriod, PVPGlobals.RepairRate, PVPGlobals.RepairRateMultipliers, PVPGlobals.RepairAcceleration, PVPGlobals.RepairAccelerationMultipliers, PVPGlobals.RepairKitHp, PVPGlobals.RepairKitSp, PVPGlobals.MainWorldInvulnerabilityDuration, PVPGlobals.MainWorldInvulnerabilityWantCutoff, PVPGlobals.MainWorldInvulnerabilityCutoffRadiusScale, PVPGlobals.SinkHpBonusPercent, ShipBalance.RepairRate, ShipBalance.RepairPeriod, ShipBalance.FalloffShift, ShipBalance.FalloffMultiplier, ShipBalance.SpeedModifier, ShipBalance.ArmorAbsorb, ShipBalance.ArmorBounce, ShipBalance.NPCArmorModifier, ShipBalance.NPCDamageIn, ShipBalance.NPCDamageOut, PVPGlobals.SinkStreakPeriod, PVPGlobals.MaxPrivateerShipsPerTeam)
        self._addSettings(*list(PVPGlobals.ShipClass2repairLocators.values()))