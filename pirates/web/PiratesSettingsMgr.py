# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.web.PiratesSettingsMgr
from direct.directnotify.DirectNotifyGlobal import directNotify
from otp.web.SettingsMgr import SettingsMgr
from pirates.web.PiratesSettingsMgrBase import PiratesSettingsMgrBase

class PiratesSettingsMgr(SettingsMgr, PiratesSettingsMgrBase):
    notify = directNotify.newCategory('PiratesSettingsMgr')

    def _initSettings(self):
        SettingsMgr._initSettings(self)
        PiratesSettingsMgrBase._initSettings(self)