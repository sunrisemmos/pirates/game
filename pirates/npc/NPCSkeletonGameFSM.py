# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.npc.NPCSkeletonGameFSM
from direct.gui.DirectGui import *
from pandac.PandaModules import *
from direct.distributed import DistributedSmoothNode
from direct.interval.IntervalGlobal import *
from pirates.piratesbase import PiratesGlobals
from pirates.pirate import BattleNPCGameFSM

class NPCSkeletonGameFSM(BattleNPCGameFSM.BattleNPCGameFSM):

    def __init__(self, av):
        BattleNPCGameFSM.BattleNPCGameFSM.__init__(self, av)

    def cleanup(self):
        BattleNPCGameFSM.BattleNPCGameFSM.cleanup(self)