# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.npc.NPCManager
from direct.showbase import DirectObject

class NPCManager(DirectObject.DirectObject):

    def __int__(self):
        self.clearNpcData()

    def clearNpcData(self):
        self.npcData = {}

    def addNpcData(self, data):
        for currKey in list(data.keys()):
            self.npcData.setdefault(currKey, {}).update(data[currKey])

    def getNpcData(self, uid):
        return self.npcData.get(uid, {})