# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.npc.Ghost
from pandac.PandaModules import *
from direct.interval.IntervalGlobal import *
from pirates.piratesbase import PiratesGlobals
from pirates.pirate import Human
from pirates.pirate import AvatarTypes

class Ghost(Human.Human):

    def __init__(self, avatarType=AvatarTypes.Ghost):
        Human.Human.__init__(self)
        self.avatarType = avatarType