# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.leveleditor.WorldDataGlobals
LINK_TYPE_AI_NODE = 'Node Links'
LINK_TYPE_LOC_NODE = 'Locator Links'
LINK_TYPE_INT_OBJ = 'Interact Links'
LINK_TYPES = [LINK_TYPE_AI_NODE, LINK_TYPE_LOC_NODE, LINK_TYPE_INT_OBJ]
OCEAN_AREAS = 'Ocean Areas'
SHIP_WAYPOINT = 'Ship Movement Node'
WAYPOINT_TYPES = [SHIP_WAYPOINT]