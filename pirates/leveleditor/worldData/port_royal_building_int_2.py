# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.leveleditor.worldData.port_royal_building_int_2
from pandac.PandaModules import Point3, VBase3
objectStruct = {'Objects': {'1155772882.54fxlara0': {'Type': 'Building Interior', 'Name': 'port_royal_building_int_2', 'Instanced': True, 'Objects': {'1169151604.41mike': {'Type': 'Townsperson', 'Category': 'Commoner', 'Hpr': Point3(0.0, 0.0, 0.0), 'Pos': Point3(201.7, 26.992, 1.266), 'Respawns': True, 'Scale': VBase3(1.0, 1.0, 1.0), 'Start State': 'Walk', 'Team': 'Player'}}, 'Visual': {'Model': 'models/buildings/interior_vip_room'}}}, 'Node Links': [], 'Layers': {}, 'ObjectIds': {'1155772882.54fxlara0': '["Objects"]["1155772882.54fxlara0"]', '1169151604.41mike': '["Objects"]["1155772882.54fxlara0"]["Objects"]["1169151604.41mike"]'}}