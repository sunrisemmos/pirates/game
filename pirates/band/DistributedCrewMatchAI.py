from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedCrewMatchAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedCrewMatchAI')

    def requestCrewAdd(self, todo0, todo1, todo2):
        pass

    def responseCrewAdd(self, todo0):
        pass

    def requestCrewDelete(self):
        pass

    def responseCrewDelete(self, todo0):
        pass

    def requestInitialAvatarAdd(self, todo0):
        pass

    def responseInitialAvatarAdd(self, todo0, todo1, todo2, todo3):
        pass

    def requestInitialAvatarAddResponse(self, todo0, todo1):
        pass

    def responseInitialAvatarAddResponse(self, todo0):
        pass

    def requestPutAvatarOnLookoutList(self, todo0):
        pass

    def requestdeleteAvatarFromLookoutList(self):
        pass

    def responseCrewFound(self, todo0, todo1, todo2):
        pass

    def requestAcceptInvite(self, todo0):
        pass

    def requestCrewOfOneCreation(self):
        avId = self.air.getAvatarIdFromSender()
        av = self.air.doId2do.get(avId)
        if not av:
            return

        self.air.bandManager.createCrewOfOne(av, avId)

    def requestCrewOfOneDelete(self):
        pass

    def notifySponsorNewMember(self, todo0):
        pass