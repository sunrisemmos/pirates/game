from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedBandMemberAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedBandMemberAI')
    bandMembers = {}
    bandMap = {}

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

        self.avatarId = 0
        self.name = ''
        self.hp = 0
        self.maxHp = 0
        self.bandId = None
        self.isManager = False
        self.shipInfo = [0, '', 0, []]
        self.shipHasSpace = False

    def announceGenerate(self):
        DistributedObjectAI.announceGenerate(self)

        self.air.setOwner(self.doId, self.GetPuppetConnectionChannel(self.avatarId), True)

    def disable(self):
        if self.bandId:
            self.bandMap[self.bandId].remove(self)
            if len(self.bandMap[self.bandId]) <= 0:
                del self.bandMap[self.bandId]

        if self.avatarId != 0:
            del self.bandMembers[self.avatarId]

        DistributedObjectAI.disable(self)

    def delete(self):
        self.avatarId = None

        DistributedObjectAI.delete(self)

    def setAvatarId(self, avatarId):
        if self.avatarId != 0:
            del self.bandMembers[avatarId]

        self.avatarId = avatarId
        self.bandMembers[avatarId] = self

    def d_setAvatarId(self, avatarId):
        self.sendUpdate('setAvatarId', [avatarId])

    def b_setAvatarId(self, avatarId):
        self.setAvatarId(avatarId)
        self.d_setAvatarId(avatarId)

    def getAvatarId(self):
        return self.avatarId

    def setName(self, name):
        self.name = name

    def d_setName(self, name):
        self.sendUpdate('setName', [name])

    def b_setName(self, name):
        self.setName(name)
        self.d_setName(name)

    def getName(self):
        return self.name

    def setHp(self, hp):
        self.hp = hp

    def d_setHp(self, hp):
        self.sendUpdate('setHp', [hp])

    def b_setHp(self, hp):
        self.setHp(hp)
        self.d_setHp(hp)

    def getHp(self):
        return self.hp

    def setMaxHp(self, maxHp):
        self.maxHp = maxHp

    def d_setMaxHp(self, maxHp):
        self.sendUpdate('setMaxHp', [maxHp])

    def b_setMaxHp(self, maxHp):
        self.setMaxHp(maxHp)
        self.d_setMaxHp(maxHp)

    def getMaxHp(self):
        return self.maxHp

    def setBandId(self, manager, id_):
        if self.bandId:
            self.bandMap[self.bandId].remove(self)
            if len(self.bandMap[self.bandId]) <= 0:
                del self.bandMap[self.bandId]

        self.bandId = (manager, id_)
        if self.bandId:
            self.bandMap.setdefault(self.bandId, set()).add(self)

    def d_setBandId(self, manager, id_):
        self.sendUpdate('setBandId', [manager, id_])

    def b_setBandId(self, manager, id_):
        self.setBandId(manager, id_)
        self.d_setBandId(manager, id_)

    def getBandId(self):
        return self.bandId or (0, 0)

    def setIsManager(self, isManager):
        self.isManager = isManager

    def d_setIsManager(self, isManager):
        self.sendUpdate('setIsManager', [isManager])

    def b_setIsManager(self, isManager):
        self.setIsManager(isManager)
        self.d_setIsManager(isManager)

    def getIsManager(self):
        return self.isManager

    def setShipInfo(self, shipId, shipName, shipClass, mastInfo):
        self.shipInfo = [shipId, shipName, shipClass, mastInfo]

    def d_setShipInfo(self, shipId, shipName, shipClass, mastInfo):
        self.sendUpdate('setShipInfo', [shipId, shipName, shipClass, mastInfo])

    def b_setShipInfo(self, shipId, shipName, shipClass, mastInfo):
        self.setShipInfo(shipId, shipName, shipClass, mastInfo)
        self.d_setShipInfo(shipId, shipName, shipClass, mastInfo)

    def getShipInfo(self):
        return self.shipInfo

    def setShipHasSpace(self, hasSpace):
        self.shipHasSpace = hasSpace

    def d_setShipHasSpace(self, hasSpace):
        self.sendUpdate('setShipHasSpace', [hasSpace])

    def b_setShipHasSpace(self, hasSpace):
        self.setShipHasSpace(hasSpace)
        self.d_setShipHasSpace(hasSpace)

    def getShipHasSpace(self):
        return self.shipInfo[0] and self.shipHasSpace

    def teleportQuery(self, localAvId, localGuildId, localShardId):
        pass

    def teleportResponse(self, avId, available, shardId, instanceDoId, areaDoId):
        pass