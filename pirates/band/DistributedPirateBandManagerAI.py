from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.band import BandConstance
from pirates.band.DistributedBandMemberAI import DistributedBandMemberAI

class DistributedPirateBandManagerAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedPirateBandManagerAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

        self.crews = {}

    def addAvToCrew(self, av, avId, hostId):
        """
        This adds an avatar (including the host) to an existing crew.
        """

        if hostId not in self.crews:
            # Crew doesn't exist. Make a new one.
            self.createCrewOfOne(self.air.doId2do[hostId], hostId)

        bandId = self.crews[hostId][0]
        bandMemberDoId = self.air.allocateChannel()

        bandMember = DistributedBandMemberAI(self.air)
        bandMember.setAvatarId(avId)
        if avId == hostId:
            bandMember.setIsManager(True)
        bandMember.setName(av.getName())
        bandMember.setHp(av.getHp())
        bandMember.setMaxHp(av.getMaxHp())
        bandMember.setBandId(self.doId, bandId)
        bandMember.generateWithRequiredAndId(bandMemberDoId, self.air.districtId, bandId)

        av.b_setBandId(self.doId, bandId)

        handleSpace = self.air.interestManager.registerHandleSpace(avId, 'BandManager')

        self.air.interestManager.openInterest(self.GetPuppetConnectionChannel(avId), handleSpace[0], self.air.districtId, [bandId])

        del handleSpace[0]

        return True

    def createCrewOfOne(self, av, avId):
        """
        This sets up a new band and generates a band member object for the host.
        """

        if not self.air.wantBands:
            return

        bandId = self.air.allocateChannel()

        self.crews[avId] = (bandId, [])

        self.addAvToCrew(av, avId, avId)

    def requestInvite(self, avatarId):
        """
        An avatar is requesting an invitation to another avatar to join
        their crew. We'll check the credentials and route it through.
        """

        if not self.air.wantBands:
            return

        avId = self.air.getAvatarIdFromSender()
        av = self.air.doId2do.get(avId)
        if not av:
            return

        av = self.air.doId2do.get(avatarId)
        if not av:
            # The invitee is invalid or not on our shard.
            return

        self.sendUpdateToAvatarId(avatarId, 'invitationFrom', [avId, av.getName()])

    def requestCancel(self, todo0):
        pass

    def requestOutCome(self, todo0, todo1):
        pass

    def invitationFrom(self, todo0, todo1):
        pass

    def invitationCancel(self, todo0):
        pass

    def invitationResponce(self, avatarId, avatarName, response):
        avId = self.air.getAvatarIdFromSender()
        av = self.air.doId2do.get(avId)
        if not av:
            return

        if response != BandConstance.outcome_declined and response != BandConstance.outcome_ok:
            # Invalid response.
            return

        # Add the avatar to the crew.
        success = self.addAvToCrew(av, avId, avatarId)
        if not success:
            # Failed to add the avatar to the crew.
            return

        self.sendUpdateToAvatarId(avatarId, 'requestOutCome', [avId, avatarName, response])

    def requestRemove(self, todo0):
        pass

    def requestCrewIconUpdate(self, todo0):
        pass

    def receiveUpdatedCrewIcon(self, todo0):
        pass