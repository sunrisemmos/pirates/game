# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.map.MinimapGlobals
from pandac.PandaModules import *
MAPCOLOR_FRIEND = Vec4(0.0, 0.0, 1.0, 1.0)
MAPCOLOR_ENEMY = Vec4(1.0, 0.0, 0.0, 1.0)
MAPCOLOR_PIRATE = Vec4(1.0, 1.0, 1.0, 1.0)
MAPCOLOR_NPC = Vec4(1.0, 0.7, 0.4, 1.0)
MAPCOLOR_QUEST = Vec4(1.0, 1.0, 0.0, 1.0)