# uncompyle6 version 3.7.4
# Python bytecode 2.4 (62061)
# Decompiled from: Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)]
# Embedded file name: pirates.map.Mappable

class Mappable:

    def __init__(self):
        pass

    def getMapNode(self):
        return

class MappableArea(Mappable):

    def getMapName(self):
        return ''

    def getZoomLevels(self):
        return ((100, 200, 300), 1)

    def getFootprintNode(self):
        return

    def getShopNodes(self):
        return ()

    def getCapturePointNodes(self, holidayId):
        return ()

class MappableGrid(MappableArea):

    def getGridParamters(self):
        return ()