from direct.directnotify.DirectNotifyGlobal import directNotify

from pirates.instance.DistributedInstanceWorldAI import DistributedInstanceWorldAI
from pirates.world import WorldGlobals
from pirates.piratesbase import PiratesGlobals


class DistributedPiratesTutorialWorldAI(DistributedInstanceWorldAI):
    notify = directNotify.newCategory('DistributedPiratesTutorialWorldAI')

    def __init__(self, air):
        DistributedInstanceWorldAI.__init__(self, air)

        self.fileName = WorldGlobals.PiratesTutorialSceneFileBase
        self.instanceType = PiratesGlobals.INSTANCE_TUTORIAL
