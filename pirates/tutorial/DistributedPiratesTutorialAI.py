from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectAI import DistributedObjectAI
from pirates.tutorial import TutorialGlobals
from pirates.quest import QuestDB
from pirates.piratesbase import PiratesGlobals
from pirates.world.LocationConstants import LocationIds


class DistributedPiratesTutorialAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedPiratesTutorialAI')

    def __init__(self, air):
        DistributedObjectAI.__init__(self, air)

    def clientEnterAct0Tutorial(self):
        avatarId = self.air.getAvatarIdFromSender()
        avatar = self.air.doId2do.get(avatarId)

        # Create our initial quest.
        questDNA = QuestDB.QuestDict[TutorialGlobals.FIRST_QUEST]
        self.air.questMgr.createQuest(avatar, questDNA.getQuestId())

    def makeAPirateComplete(self):
        avatarId = self.air.getAvatarIdFromSender()
        avatar = self.air.doId2do.get(avatarId)

        quest = self.air.questMgr.getQuest(avatar, questId = TutorialGlobals.FIRST_QUEST)

        def questFinalizeCallback():
            # Create our second quest.
            questDNA = QuestDB.QuestDict[TutorialGlobals.SECOND_QUEST]
            self.air.questMgr.createQuest(avatar, questDNA.getQuestId())

            self.sendUpdate('makeAPirateCompleteResp', [])

        self.acceptOnce(f'quest-finalize-{quest.doId}', questFinalizeCallback)

        # Start the second cutscene.
        quest.d_startFinalizeScene(1, 0)

    def tutorialComplete(self):
        avatarId = self.air.getAvatarIdFromSender()
        avatar = self.air.doId2do.get(avatarId)

        # Create our skip tutorial quest.
        questDNA = QuestDB.QuestDict[TutorialGlobals.SKIP_TUTORIAL_QUEST]
        self.air.questMgr.createQuest(avatar, questDNA.getQuestId())

        # TEMP
        avatar.sendUpdate('updateClientTutorialStatus', [PiratesGlobals.TUT_FINISHED])
        avatar.b_setReturnLocation(LocationIds.PORT_ROYAL_ISLAND)

        self.air.teleportMgr.createLoginTeleport(avatarId, False, False)
