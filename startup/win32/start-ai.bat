@echo off
title Pirates of the Caribbean - AI
cd ../..

set /P PYTHON_PATH=<PYTHON_PATH

cls

:main
%PYTHON_PATH% -m pirates.ai.AIStart
pause
goto :main
