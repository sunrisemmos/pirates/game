@echo off
title Pirates of the Caribbean - UberDOG
cd ../..

set /P PYTHON_PATH=<PYTHON_PATH

%PYTHON_PATH% -m pip install -r requirements.txt

:main
%PYTHON_PATH% -m pirates.uberdog.Start
pause
goto :main
