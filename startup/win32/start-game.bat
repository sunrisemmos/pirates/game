@echo off
cd ../..

rem Server address input selection
echo Choose your server (Default: Localhost)
echo #1 - Localhost
echo #2 - Development Server
echo #3 - Custom

:selection

set INPUT=-1
set /P INPUT=Selection: 

if %INPUT%==1 (
    set GAME_SERVER=127.0.0.1:6665
) else if %INPUT%==2 (
    set GAME_SERVER=34.136.173.79:6665
) else if %INPUT%==3 (
    set /P GAME_SERVER=Gameserver: 
) else (
    goto selection
)

title Pirates of the Caribbean - Game Client (%GAME_SERVER%)

set /P DISLTOKEN="Username: "
set /P PYTHON_PATH=<PYTHON_PATH

cls

:main
%PYTHON_PATH% -m pirates.launcher.StartPiratesLauncher
pause
goto :main