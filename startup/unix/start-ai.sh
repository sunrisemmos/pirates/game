#!/bin/sh
cd ../..

# Get the user input:
read -p "District name (DEFAULT: Abassa): " DISTRICT_NAME
export DISTRICT_NAME=${DISTRICT_NAME:-Abassa}
read -p "Base channel (DEFAULT: 401000000): " BASE_CHANNEL
export BASE_CHANNEL=${BASE_CHANNEL:-401000000}

clear

python3 -m pirates.ai.AIStart config/server.prc