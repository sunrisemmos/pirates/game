#!/bin/sh
cd ../..

# Get the user input:
read -p "Username: " PLAY_TOKEN
read -p "Gameserver (DEFAULT: 127.0.0.1): " GAME_SERVER
GAME_SERVER=${GAME_SERVER:-"127.0.0.1"}

# Export the environment variables:
export PLAY_TOKEN=$PLAY_TOKEN
export GAME_SERVER=$GAME_SERVER

echo "==============================="
echo "Starting Pirates of the Caribbean..."
echo "Username: $PLAY_TOKEN"
echo "Gameserver: $GAME_SERVER"
echo "==============================="

python3 -m pirates.piratesbase.PiratesStart